/*****************************************************************************
 * Copyright (C) 2009 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**	@file   glprintf.h
 *	@author Herve Lombaert
 *	@date   July 31st, 2008
 *	@brief  glPrintf, prints in 3D a la printf()
 *              NB: this file does rely on glut to load the used font
 */

#ifndef GLPRINTF_H
#define GLPRINTF_H

#if _WIN32
# include <windows.h>
#endif

#ifdef __APPLE__
# include <OpenGL/gl.h>
# include <GLUT/glut.h>
#else
# include <GL/gl.h>
# include <GL/glut.h>
#endif

#include <cstring>
#include <cstdarg>
#include <cstdio>

#if _WIN32
# pragma warning(disable:4996)
#endif

/** prints in 3D a la printf
 *  Position the text with \c glTranslate()
 *  Currently, bitmaps are displayed, \c glScale() does not work
 *  NB: this file does rely on glut to load the used font
 *
 * \code
 *   glColor3f(1,1,1);
 *   glTranslatef(10,10,0);
 *   glPrintf("value: %f\n", value);
 * \endcode
 */
void glPrintf(const char* msg, ...)
{
	static GLuint fontBase = 0;
	if(!glIsList(fontBase))
	{
		fontBase = glGenLists(127);
		for(unsigned int i=0; i!=127; ++i)
		{
			// create a display list for each letter
			glNewList(fontBase + i, GL_COMPILE);
			glutBitmapCharacter(GLUT_BITMAP_8_BY_13, i);
			glEndList();
		}
	}

	// prepare the string
	char str[256];
	va_list arg;
	va_start(arg, msg);
	vsnprintf(str, 256, msg, arg);
	va_end(arg);

	// position and display the string
	glRasterPos3f(0,0,0);
	glListBase(fontBase);
	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
}

#if _WIN32
# pragma warning(default:4996)
#endif


#endif

