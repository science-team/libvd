/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	color.h
 * @author	Herve Lombaert
 * @date	October 2006
 */

#ifndef COLOR_H
#define COLOR_H

/**
 * Color with 3 channel, this can be used to define an image element with for
 * instance Image<color<float>,2>
 * @see Image
 */

template <typename T>
struct color {
	T b,g,r; ///< colors as they appear in the file
	color(T r=0, T g=0, T b=0) : b(b),g(g),r(r) {} ///< constructor
};

#endif
