/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/** @file slidebar.h
 *  @author Herve Lombaert
 *	@date	Feb 7th, 2007
 *
 *	Adds a slide bar
 */

#ifndef SLIDEBAR_H
#define SLIDEBAR_H


namespace Tools
{

	class SlideBar
	{
	public:
		SlideBar();
		~SlideBar();

		/** get the slide bar position (between 0..1) */
		float get() const;

		/** set the slide bar position (between 0..1) */
		void set(float val);

		/** display the bar */
		void display() const;

		/** set the slide bar size and position on the screen */
		void place(int x, int y, int width, int height, int win_width, int win_height);

		/** handle mouse press */
		bool mouse(int x, int y);

		/** handle mouse motion */
		bool motion(int x, int y);

		/** reshape the slide bar */
		void reshape(int w, int h);

	private:
		float m_val; /// value of the bar between (0..1)
		int m_posx;
		int m_posy;
		int m_width;
		int m_height;
		int m_win_width;
		int m_win_height;
		bool m_mouse_pressed;
	};

} // namespace


#endif
