/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	clip.h
 * @author	Herve Lombaert
 * @date	November 15th, 2006
 */

#ifndef CLIP_H
#define CLIP_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <vector>
#include <cmath>
#include <error.h>


namespace Tools
{
	/**
	 * @class Clip
	 * @brief Clip objects
	 *
	 * Clip a scene with a plane. For instance, when visualizing a volume, clipping
	 * will reveal what is inside the volume
	 *
	 * \image html clip.png
	 * \image latex clip.png
	 *
	 * Use and manipulate a clip plane with:
	 *
	 * \code
	 *	Clip plane;
	 *
	 *	   plane.enable();
	 *	// ... draw  ...
	 *	// plane.display(); // optionnaly draw the plane
	 *	   plane.disable();
	 *
	 *	plane.rotateWithScreen(delta_mouse_x, delta_mouse_y);
	 *	plane.slide(delta_mouse_y);
	 * \endcode
	 *
	 * Note, there is currently a maximum of 6 available clip planes
	 * If using more clip planes, an Error will be thrown
	 */


	class Clip
	{
	public:
		Clip();
		~Clip();

		/** enable clip plane
		 *  when using this method while drawing every object drawn next will be clipped */
		void enable();

		/** disable clip plane
		 *  use this method after enable() while drawing */
		void disable();

		/** slide plane */
		void slide(float d);

		/** rotate the plane from its current orientation
		 *  @param dx a rotation around the plane Y world axis
		 *  @param dy a rotation around the plane X world axis */
		void rotateWithWorld(float dx, float dy);

		/** rotate plane with mouse movement
		 *  this method requires the currnet modelview matrix, it is assumed
		 *  to be the same as when "enable()" was called
		 *  @param dx a rotation around the screen Y axis (horizontal movement)
		 *  @param dy a rotation around the screen X axis (vertical movement) */
		void rotateWithScreen(float dx, float dy);

		/** display a plane close to the origin
		 *  @param use_cross_section draw a cross section of a unit centered cube */
		void display(bool use_cross_section=true) const;

		/** Use identity matrix for the clip plane, that is a plane normal to the Z axis */
		void init();

		/** Get plane matrix
		 *  @param m (out) plane matrix (4x4 rotation and translation) */
		void getPlaneMatrix(float m[]) const;

		/** Set modelview matrix */
		void setPlaneMatrix(const float m[]);

	private:
		/** make sure the translation part of the plane matrix
		 *  is always the shortest distance from plane to origin */
		void correctMatrix();

		/** Draw a cross section of a centered unit square */
		void drawCrossSection(const float m[]) const;

		/** Draw a plane */
		void drawPlane(const float m[]) const;

		static void mult(const float a[], const float b[], float m[]); /// multiply matrices
		static void transp(float m[]); /// transpose matrix

		/** Manage the plane Id (OpenGL has a limited number of clip planes) */
		struct PlaneManager {
			PlaneManager();          ///< init the list of available plane
			GLenum allocate();       ///< remove a plane from the list of available plane
			void free(GLenum);       ///< add a plane to the list of available plane
			enum { NBPLANES = 6 };   ///< GL_CLIP_PLANE0 to GL_CLIP_PLANE5
			bool m_planes[NBPLANES]; ///< list of available planes
		};

	private:
		float m_planeMatrix[16];             ///< plane matrix
		float m_modelview[16];               ///< modelview matrix (saved in "enable")
		GLenum m_planeId;                    ///< clip plane id of current object
		static PlaneManager* m_planeManager; ///< which plane is available next (singleton, so it uplives in a library)
	};


} // namespace

#endif
