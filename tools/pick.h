/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	pick.h
 * @author	Herve Lombaert
 * @date	November 15th, 2006
 */

#ifndef PICK_H
#define PICK_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


namespace Tools
{
	/**
	 * @class Pick
	 * @brief Pick 3D coordinates
	 *
	 * Pick some objects and returns the 3D coordinates from the mouse position.
	 *
	 * \image html pick.png "Picking the tip of the left ventricle"
	 * \image latex pick.png "Picking the tip of the left ventricle"
	 *
	 * Uses the Z buffer to get the 3D coordinate. Renders the objects with alpha
	 * testing so transparent part of the object are not selected.
	 *
	 * Object to be picked are defined with "enable()" and "disable()"
	 *
	 * \code
	 *	// to define selection
	 *	glPushMatrix();
	 *	glTranslate/Rotate(...);
	 *
	 *	pick.enable();
	 *	// ... draw scene ...
	 *	pick.disable();
	 *
	 *	glPopMatrix();
	 *
	 *	// to pick
	 *	pick.get(mouse_x, mouse_y, x,y,z);
	 * \endcode
	 *
	 * This object creates a display list between \c enable() and \c disable()
	 * The picking is done by drawing the display list in the back buffer
	 * and reading the Z buffer
	 *
	 * Note that the \c GL_SELECT rendering mode is not used because it only pick
	 * primitives and can not do fragment picking. Using the Z buffer allows to
	 * detect transparent fragments.
	 */


	class Pick
	{
	public:
		Pick();
		~Pick();

		/** save modelview/projection matrices
		 * place this function right before drawing your object */
		void saveMatrices();

		/** pick objects
		 * @param mouse_x,mouse_y (in) mouse position where (0,0) is at the lower left corner
		 * @param x,y,z (out) 3D world coordinates x,y,z from mouse position
		 * @return true when an object has been hit */
		bool get(int mouse_x, int mouse_y, float &x, float &y, float &z) const;

		/** start defining objects to be picked */
		void enable() const;

		/** stop defining objects to be picked */
		void disable() const;

		/** set max transparent alpha (default 0.2)
		 *  @param max picking will not occur on element with alpha<max
		 *             if max is 0, everything will be picked */
		void setMaxAlpha(float max);

	private:
		mutable GLdouble m_modelview[16];  ///< modelview matrix
		mutable GLdouble m_projection[16]; ///< projection matrix
		float            m_maxalpha;       ///< maximum alpha allowed when rendering object
		mutable GLuint m_displayId;        ///< display of objects to be picked
		mutable bool   m_displayCreated;   ///< check if this object created the display list or not
	};


} // namespace

#endif
