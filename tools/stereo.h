/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	stereo.h
 * @author	Herve Lombaert
 * @date	March 19th 2007
 */

#ifndef STEREO_H
#define STEREO_H

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <error.h>


namespace Tools
{
	/**
	 * @class Stereo
	 * @brief Render a stereographic view
	 *
	 * Renders a stereo view for red/blue 3D glasses. All drawings rendered between
	 * \c enable() and \c disable() will be displayed twice in red and in blue. Each
	 * drawing will have a slight offset.
	 *
	 * \code
	 *   Tools::Stereo stereo;
	 *   stereo.enable();
	 *   // drawings
	 *   stereo.disable();
	 * \endcode
	 */

	class Stereo
	{
	public:
		Stereo();
		~Stereo();

		/** Start defining the stereoscopic view */
		void enable();

		/** Stop defining the stereoscopic view and draw it in stereo */
		void disable();

	private:
		GLdouble m_modelview[16]; ///< modelview matrix of the scene
		GLuint   m_id;            ///< scene display list
		bool     m_listcreated;   ///< check if display list is created by this object
		GLdouble m_eye_shift;     ///< translation for each eye
	};
};

#endif
