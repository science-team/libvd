/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	manipworld.h
 * @author	Herve Lombaert
 * @date	October 27th, 2006
 */


/**
 * @namespace Tools
 * Handy tools for the visualization library.
 * Anything extra in the visualization library ends up in this namespace.
 * Features such as mouse manipulator, clip planes, or object picking are
 * available here.
 */


#ifndef MANIPWORLD_H
#define MANIPWORLD_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


namespace Tools
{
	/**
	 * @class ManipWorld
	 * @brief Mouse handler to manipulate the world
	 *
	 * Manipulate the world, mouse translations and rotations are screen-axis aligned
	 * This object handles the modelview matrix, in your display function use:
	 *
	 * \code
	 *	void display()
	 *	{
	 *	  glClear(GL_COLOR_BUFFER_BIT | // erase last image on buffer
	 *	          GL_DEPTH_BUFFER_BIT); // erase last z buffer
	 *	  
	 *	  glPushMatrix();
	 *	  manip.loadIdentityMatrix();
	 *
	 *	  // ... draw objects ...
	 *
	 *	  glPopMatrix();
	 *	}
	 * \endcode
	 *
	 * Rotate with:
	 *
	 * \code
	 *	void mouse_motion(int x, int y)
	 *	{
	 *	   manip.rotateXAxis(y - last_y);
	 *	   manip.rotateYAxis(x - last_x);
	 *	}
	 * \endcode
	 *
	 * To pick an object (also look into the Pick class):
	 *
	 * \code
	 *	void mouse_click(int x, int y)
	 *	{
	 *	   float pos[3];
	 *	   if( ManipWorld::pick(x, window_height-y, pos[0], pos[1], pos[2]) )
	 *	     // ... object picked at position -pos- ...
	 *	}
	 * \endcode
	 * 
	 * Note that the mouse coordinate origin is on the lower left corner of the viewport
	 *
	 */


	class ManipWorld
	{
	public:
		ManipWorld();
		~ManipWorld();

		/** Use identity matrix */
		void init();

		/** translate camera (screen aligned translations)
		 * @param dx,dy translate object parallel to the screen
		 * @param dz move object in and outside the screen */
		void translateAxis(float dx, float dy, float dz);

		/** translate camera (world aligned translations)
		 * @param dx,dy,dz translate object in world distance */
		void translate(float dx, float dy, float dz);

		/** camera zoom */
		void zoom(float zoom);

		/** Rotate verticaly, around the screen X axis */
		void rotateXAxis(float delta);

		/** Rotate horizontaly, around the screen Y axis */
		void rotateYAxis(float delta);

		/** Tilt camera, around the screen Z axis */
		void rotateZAxis(float delta);

		/** Rotate around the world X axis */
		void rotateX(float delta);

		/** Rotate around the world Y axis */
		void rotateY(float delta);

		/** Rotate around the world Z axis */
		void rotateZ(float delta);

		/** Load modelview matrix
		 *  this actually rotate and translate the world */
		void loadModelViewMatrix() const;

		/** Load inverse of the modelview matrix
		 *  it can be used to undo a manipulation */
		void loadInverseModelViewMatrix() const;

		/** Save the actual modelview matrix
		 *  if the modelview if modified externally, this method will
		 *  copy it to this manipulator */
		void saveModelViewMatrix();

		/** Get modelview matrix */
		void getModelViewMatrix(float m[]) const;

		/** Set modelview matrix */
		void setModelViewMatrix(const float m[]);

		/** pick a 3D point at pixel (x,y)
		 * Assume the projection matrix is unchanged since last drawing 
		 * This is a static method, so you can call it from anywhere
		 * If assumptions are wrong, use the class Pick
		 * @see Pick */
		static bool pick(int mouse_x, int mouse_y, float &x, float &y, float &z);

	private:
		static void transp(float m[]);

	private:
		float m_modelview[16]; ///< model view matrix
	};


} // namespace


#endif
