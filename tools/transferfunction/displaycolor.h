/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	displaycolor.h
 * @author	Herve Lombaert
 * @date	November 14th, 2006
 */

#ifndef DISPLAYCOLOR_H
#define DISPLAYCOLOR_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <error.h>


namespace Tools { namespace TransferFunction
{
	/**
	 * Display a color palette, a unit box is drawn with
	 * different colors. With a mouse position (normalized
	 * with the box) it is possible to query what color has
	 * been selected.
	 *
	 * \image html displaycolor.png
	 * \image latex displaycolor.png
	 *
	 * This can be used as a color selector in a transfer
	 * function editor.
	 *
	 * To draw the colors, this model is used:
	 *
	 * \verbatim
	 *      ---------------------- white (1,1,1)
	 *      |                    |
	 *      |                    |
	 *      R      G      B      R
	 *      |------|------|------|
	 *    (100)  (010)  (001)  (100)
	 *      |                    |
	 *      |                    |
	 *      ---------------------- black (0,0,0)
	 * \endverbatim
	 *
	 * Internally, a 128x128 texture is created.
	 *
	 * @see TransferFunction
	 *
	 */


	class DisplayColor
	{
	public:
		DisplayColor();
		~DisplayColor();

		/** display the colors in a centered 1x1 square */
		void display() const;

		/** get rgb for a normalized position (0..1)
		 * @param x,y (in) mouse position normalized with the box size (0..1)
		 * @param r,g,b (out) color selected at position x,y */
		void getRGB(float x, float y, float &r, float &g, float &b) const;

		/** place a cursor at color rgb */
		void mark(float r, float g, float b);

	private:
		void initTexture() const; ///< init texture

	private:
		mutable GLuint m_textureName; ///< texture name
		float m_mark_r; ///< rgb of marked color
		float m_mark_g; ///< rgb of marked color
		float m_mark_b; ///< rgb of marked color
		float m_mark_u; ///< position of marker
		float m_mark_v; ///< position of marker
	};


}} // namespaces

#endif
