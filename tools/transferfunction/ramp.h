/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ramp.h
 * @author	Herve Lombaert
 * @date	November 8th, 2006
 */

#ifndef DISPLAYRAMP_H
#define DISPLAYRAMP_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


namespace Tools { namespace TransferFunction
{
	/**
	 * Display and manipulate a Ramp.
	 *
	 * \image html ramp.png
	 * \image latex ramp.png
	 *
	 * \verbatim
	 *                 |               /-----------|
	 * left border     |             /             | right border
	 *                 |-----------/               |
	 *            ---------------------------------------
	 *               left       low    high      right
	 * \endverbatim
	 *
	 * Here data intensities between low and high as well as data intensities
	 * beyond high will appear as bright pixels. Everything below low will
	 * not appear. Top controls the opacity.
	 *
	 * Color transition is from black to low color to high color.
	 *
	 * As with the histogram, it is possible to visualize only a portion
	 * of the ramp, and to zoom and pan it.
	 *
	 */


	class Ramp
	{
	public:
		Ramp();
		Ramp(float low, float high, float opacity=1);
		~Ramp();

		/** update ramp */
		void update(float low, float high, float opacity);

		/** update border (visualize only portion of the ramp) */
		void clamp(float left, float right);

		/** display ramp line and squares */
		void display() const;

		/** did mouse pick a ramp feature (xy normalized 0..1) */
		bool mouse(float x, float y);

		/** handle mouse motion after a mouse pick (xy normalized 0..1) */
		bool motion(float x, float y);

		/** get the ramp value (rgba 0..1) for a given intensity */
		void get(float i, float &r, float &g, float &b, float &a) const;

		/** set color of the selected feature */
		void setColor(float r, float g, float b);

		/** update aspect ratio (w/h) so square appears as square */
		void reshape(int w, int h);

	private:
		float m_low;    ///< ramp low value
		float m_high;   ///< ramp high value
		float m_opacity;///< ramp opacity
		float m_left;   ///< left border of visualization
		float m_right;  ///< right border of visualization
		float m_low_u;  ///< ramp low position (0..1)
		float m_high_u; ///< ramp high position (0..1)
		float m_aspectratio;     ///< aspect ratio so squares appear as squares
		float m_low_color[3];    ///< color or low cornder
		float m_middle_color[3]; ///< color or middle cornder
		float m_high_color[3];   ///< color or lower cornder

		/// ramp features
		/// LOW/HIGH is the low/high ramp corner
		/// MIDDLE is a dragrable square, change translate the ramp
		enum feature { NONE, LOW, MIDDLE, HIGH };
		feature m_feature; ///< selected ramp feature
	};


}} // namespaces

#endif
