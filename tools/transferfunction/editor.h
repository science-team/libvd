/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	editor.h
 * @author	Herve Lombaert
 * @date	November 11th, 2006
 */

#ifndef EDITOR_H
#define EDITOR_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


#include <tools/transferfunction/displayhistogram.h>
#include <tools/transferfunction/displaycolor.h>
#include <list>


namespace Tools { namespace TransferFunction
{
	/**
	 * @class Editor
	 * @brief Transfer function editor
	 *
	 * Display and manipulate a transfer function editor. It draws a histogram
	 * and manipulators allow you to change the transfer function. It is possible
	 * to change the color of a manipulator feature (e.g. ramp or trapezoid corner).
	 *
	 * \image html Editor.png "Histogram blended with volume"
	 * \image latex Editor.png "Histogram blended with volume"
	 *
	 * When drawing the transfer function, it can be blended with the background.
	 *
	 *
	 * Add Ramp and other manipulator:
	 *
	 * \code
	 *	Tools::TransferFunction::Editor tfe;
	 *
	 *	// initialize transfer function editor
	 *	tfe.update(image, 100*100);
	 *	tfe.place(x,y, tfe_width,tfe_height, window_with,window_height);
	 *	tfe.add(new Ramp(100,200));
	 *
	 *	// handle mouse events
	 *	tfe.mouse(x,y);
	 *	tfe.motion(x,y);
	 *	tfe.display();
	 *
	 *	// query transfer function (returns rgba(0..1))
	 *	tfe.get(intensity, r,g,b,a);
	 * \endcode
	 *
	 * To update a volume renderer, use Display::Volume::updateLut(tfe)
	 * @see Display::Volume::updateLut()
	 *
	 */


	class Editor
	{
	public:
		Editor();
		~Editor();

		/** update transfer function with new image */
		template <typename DataType>
		void update(const DataType* data, unsigned int data_size);

		/** only show a portion of the transfer function */
		void clamp(float min, float max);

		/** place transfer function at position x,y with size width,height
		 * by giving window size the aspect ratio can be known
		 * @param x,y lower left corner of the transfer function editor
		 * @param width,height size of the editor
		 * @param win_width,win_height size of the window or of the current viewport */
		void place(int x, int y, int width, int height, int win_width, int win_height);

		/** add new manipulator (ramp, trapezoid)
		 * memory is handled by editor (will be deleted with editor: do not delete manipulator outside)
		 * manipulator must have the following interface:
		 *
		 * \code
		 *  void clamp(left, right);    // only show portion of manipulator between left and right
		 *  bool mouse(x,y);            // handle mouse click (normalized coordinates 0..1)
		 *  void motion(x,y);           // handle mouse motion
		 *  void display() const;       // display manipulator
		 *  void get(i, r,g,b,a) const; // query value at given intensity
		 *  void setColor(r,g,b);       // give a color (may be used by the selected feature)
		 * \endcode
		 */
		template <typename Manip>
		void add(Manip* o);

		/** clear, remove all manipulators
		 *
		 */
		void clear();

		/** handle mouse click (mouse coordinates are pixels)
		 * @return true if event is caught in the TFE area */
		bool mouse(int x, int y);

		/** handle mouse motion (mouse coordinates are pixels)
		 * @return true if event is caught in the TFE area */
		bool motion(int x, int y);

		/** has lut value changed after a mouse event (ie. one object caught and changed its properties) */
		bool lutchanged() const { return m_lutchanged; }

		/** change contrast (scale color)
		 * @param contrast scale color, default is 1 */
		void contrast(float contrast) { m_contrast = contrast; }

		/** get contrast (scale color) between 0 and 1 */
		float contrast() const { return m_contrast; }

		/** change brightness (offset color)
		 * @param brightness offset color, default is 0 */
		void brightness(float brightness) { m_brightness = brightness; }

		/** get brightness (offset color) between 0 and 1 */
		float brightness() const { return m_brightness; }

		/** in color mode, the mouse motion will select a color
		 * entering this mode will pop a color selector
		 * if motion is outside the color selector, the editor leaves the color mode */
		void colorMode();

		/** display transfer function */
		void display() const;

		/** display hide
		 * @param status true if the TFE must be hidden, false to show it */
		void hide(bool status=true) { m_hide = status; }

		/** get display status */
		bool status() const { return m_hide; }

		/** query transfer function at given intensity returns rgba (0..1) */
		void get(float intensity, float &r, float &g, float &b, float &a) const;

	private:
		/** Transfer function object common interface */
		struct TFEobject {
			virtual ~TFEobject() {}
			virtual void clamp(float,float) = 0;
			virtual bool mouse(float,float) = 0;
			virtual bool motion(float,float) = 0;
			virtual void display() const = 0;
			virtual void reshape(int,int) = 0;
			virtual void get(float, float&, float&, float&, float&) const = 0;
			virtual void setColor(float, float, float) = 0;
			virtual void destroy() = 0;
		};

		/** Transfer function object with any type of manipilator */
		template <typename Manip>
		struct TFEobjectimpl : public TFEobject {
			TFEobjectimpl(Manip*o) : o(o) {}
			~TFEobjectimpl()                                                { if(o) delete o; }
			void clamp(float l, float r)                                    { if(o) o->clamp(l,r); }
			bool mouse(float x, float y)   /* normalized (0..1) */          { return (o) ? o->mouse(x,y):0; }
			bool motion(float x, float y)  /* normalized (0..1) */          { return (o) ? o->motion(x,y):0; }
			void display() const                                            { if(o) o->display(); }
			void reshape(int w, int h)                                      { if(o) o->reshape(w,h); }
			void get(float i, float &r, float &g, float &b, float &a) const { if(o) o->get(i,r,g,b,a); }
			void setColor(float r, float g, float b)                        { if(o) o->setColor(r,g,b); }
			void destroy()                                                  { if(o) delete o; o=0; }
			Manip* o; /// manipulator
		};

	private:
		DisplayHistogram m_histogram;
		std::list<TFEobject*> m_manips; ///< TFE objects such as ramps, trapezoids
		float m_min;          ///< minimum histogram value
		float m_max;          ///< maximum histogram value
		float m_left;         ///< left limit of visible histogram
		float m_right;        ///< right limit of visible histogram
		int m_posx;           ///< x pos of tfe on window
		int m_posy;           ///< y pos of tfe on window
		int m_width;          ///< width of tfe on window
		int m_height;         ///< height of tfe on window
		int m_win_width;      ///< width of window
		int m_win_height;     ///< height of window
		bool m_lutchanged;    ///< lut values has changed due to mouse event
		float m_contrast;     ///< contrast of lut, can be any value, default is 1 (scale color)
		float m_brightness;   ///< brightness of lut, between -1 and +1 (offset of color)
		bool m_colorMode;     ///< color mode (color selector is poped)
		DisplayColor m_color; ///< the color selector
		float m_colorSize;    ///< size of the color selector
		bool m_hide;          ///< hide transfer function
	};


	template <typename DataType>
	void Editor::update(const DataType* image, unsigned int image_size)
	{
		if(!image && image_size>0) throw LOCATE(Error("image is empty"));
		/* find minimum and maximum value of image, this will define the histogram resolution */
		float min = 1e6;
		float max = -1e6;
		for(const DataType* it = image; it!=&image[image_size]; ++it)
		{
			if((*it)<min) min = (*it);
			if((*it)>max) max = (*it);
		}
		unsigned int nbBins = (int)(max-min);
		
		/* update histogram */
		m_histogram.update(image, image_size, min, max, nbBins);
	}


	template <typename Manip>
	void Editor::add(Manip* m)
	{
		TFEobject* o = new TFEobjectimpl<Manip>(m);
		m_manips.push_back(o);

		// update shape
		o->reshape(m_width, m_height);
		o->clamp(m_histogram.getLeft(),m_histogram.getRight());
	}

}} // namespaces

#endif
