/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	displayhistogram.h
 * @author	Herve Lombaert
 * @date	November 7th, 2006
 */

#ifndef DISPLAYHISTOGRAM_H
#define DISPLAYHISTOGRAM_H


#if __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <error.h>


namespace Tools { namespace TransferFunction
{
	/**
	 * Display a histogram, take as input a single channel image, or anything
	 * with const iterators returning a single value, and draw its histogram.
	 *
	 * \image html displayhistogram.png
	 * \image latex displayhistogram.png
	 *
	 * It is possible to zoom and pan portion of the histogram. At any time, the
	 * viewed portion of the histogram is normalizd, so the highest visible bin
	 * will have a height of 1 in the histogram.
	 *
	 * This can be used in a transfer function editor with Ramps and Trapezoids to
	 * manage a lookup table of an image or of a volume.
	 *
	 * Internally a texture keeps the histogram and zooming and panning are done
	 * by changing the texture coordinates (m_u1, m_u1) of the drawn square.
	 *
	 * @see TransferFunction
	 */


	class DisplayHistogram
	{
	public:
		DisplayHistogram();
		~DisplayHistogram();

		/** create a new histogram with an image
		 * @param image compute the histogram of this image
		 * @param data_size numer of pixels in the image
		 * @param min,max the minimum and the maximum values of the histogram, this is used
		 * to display only a portion or the full intensity range of an image
		 * @param nbBins  number of bins in the histogram */
		template <typename DataType>
		void update(const DataType* data, unsigned int data_size, float min, float max, unsigned int nbBins);

		/** change histogram lower and higher limit (show only a section of the histogram)
		 * @param left,right left and right intensity values of the visible histogram */
		void clamp(float left, float right);

		/* get left and right values of the visible histogram */
		float getLeft() const { return m_left; }   ///< get left value of the visible histogram
		float getRight() const { return m_right; } ///< get right value of the visible histogram

		/** display histogram on a centered 1x1 square */
		void display() const;

		/** handle a mouse press
		 * @param x,y normalized mouse coordinate 0..1
		 * @return true if inside histogram */
		bool mouse(float x, float y);

		/** handle a mouse motion
		 * @param x,y normalized mouse coordinate 0..1
		 * @return true if inside histogram */
		bool motion(float x, float y);

	private:
		/**  update texture size (must be opengl compatible size) */
		void updateTexture(unsigned int width, unsigned int height, float r, float g, float b, float a);

	private:
		unsigned int* m_histogram; ///< bins of the histogram
		unsigned int m_nbBins;     ///< number of bins (histogram size)
		float m_min;     ///< minimum intensity value (user defined)
		float m_max;     ///< maximum intensity value (user defined)
		float m_left;    ///< left value of visible histogram
		float m_right;   ///< right value of visible histogram
		float m_height;  ///< height of visible histogram
		unsigned int m_maxheight; /// maximum bin height
		bool m_selected; ///< was histogram selected
		float m_lastx;   ///< last y mouse position
		float m_lasty;   ///< last y mouse position

		GLuint m_textureName; ///< histogram image
	};


	template <typename DataType>
	void DisplayHistogram::update(const DataType* image, unsigned int image_size, float min, float max, unsigned int nbBins)
	{
		/* destroy previous bin if not reusable */
		if(m_histogram && m_nbBins!=nbBins) delete [] m_histogram;

		/* update internal values */
		m_min = min;
		m_max = max;
		m_nbBins = nbBins;

		float binSize = (m_max - m_min) / m_nbBins;

		/* create histogram */
		m_histogram = new unsigned int [m_nbBins];
		for(unsigned int i=0; i<m_nbBins; ++i) m_histogram[i] = 0;

		/* iterate image and increment histogram bin */
		for(const DataType* it = image; it != &image[image_size]; ++it)
		{
			// which bin
			int bin = static_cast<int>((*it-m_min) / binSize);

			if(bin>=0 && bin<(int)m_nbBins)
			{
				// increment bin
				++m_histogram[bin];

				// update max bin height
				if(m_histogram[bin] > m_maxheight) m_maxheight = m_histogram[bin];
			}
		}

		/* display the whole histogram */
		m_left   = m_min;
		m_right  = m_max;
		m_height = static_cast<float>(m_maxheight);

		/* create new texture */
		updateTexture(1024,1024, 1,1,1, 0.8f);
	}

}} // namespaces


#endif
