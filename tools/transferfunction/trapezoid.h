/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	trapezoid.h
 * @author	Herve Lombaert
 * @date	November 8th, 2006
 */

#ifndef DISPLAYTRAPEZOID_H
#define DISPLAYTRAPEZOID_H


#if __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


namespace Tools { namespace TransferFunction
{
	/**
	 * Display and manipulate a Trapezoid
	 *
	 * \image html trapezoid.png
	 * \image latex trapezoid.png
	 *
	 * \verbatim
	 *                        high0   high1
	 *                 |          /---\            |
	 * left border     |         /     \           | right border
	 *                 |--------/       \----------|
	 *            ---------------------------------------
	 *                left    low0     low1      right 
	 * \endverbatim
	 *
	 * Here data intensities between low0 and low1 will appear as bright
	 * pixels. Everything else will not appear. Top control the opacity.
	 *
	 * As with the histogram, it is possible to visualize only a portion
	 * of the ramp, and to zoom and pan it.
	 */

	class Trapezoid
	{
	public:
		Trapezoid();
		Trapezoid(float low0, float high0, float high1, float low1, float opacity=1);
		~Trapezoid();

		/* update ramp */
		void update(float low0, float high0, float high1, float low1, float opacity);

		/* update border (visualize only portion of the ramp) */
		void clamp(float left, float right);

		/* display ramp line and squares */
		void display() const;

		/* did mouse pick a ramp feature (xy normalized 0..1) */
		bool mouse(float x, float y);

		/* handle mouse motion after a mouse pick (xy normalized 0..1) */
		bool motion(float x, float y);

		/* get the ramp value (rgba 0..1) for a given intensity */
		void get(float i, float &r, float &g, float &b, float &a) const;

		/* set color of the selected feature */
		void setColor(float r, float g, float b);

		/* update aspect ratio (w/h) so square appears as square */
		void reshape(int w, int h);

	private:
		float m_low0;    ///< trapezoid first ramp lowest value
		float m_low1;    ///< trapezoid second ramp lowest value
		float m_high0;   ///< trapezoid first ramp highest value
		float m_high1;   ///< trapezoid second ramp highest value
		float m_opacity; ///< max opacity
		float m_left;    ///< left border of visualization
		float m_right;   ///< right border of visualization
		float m_low0_u;  ///< trapezoid low position (0..1)
		float m_low1_u;  ///< trapezoid low position (0..1)
		float m_high0_u; ///< trapezoid high position (0..1)
		float m_high1_u; ///< trapezoid high position (0..1)
		float m_aspectratio;   ///< aspect ratio so squares appear as squares
		float m_low0_color[3]; ///< color of lowest corner of first ramp
		float m_low1_color[3]; ///< color of lowest corner of second ramp
		float m_high0_color[3]; ///< color of highest corner of first ramp
		float m_high1_color[3]; ///< color of highest corner of second ramp
		float m_middle0_color[3]; ///< color of middle square of first ramp
		float m_middle1_color[3]; ///< color of middle square of second ramp

		// trapezoid features
		// LOW0  is the first ramp lowest value
		// HIGH1 is the first ramp highest value
		// LOW1  is the second ramp lowest value
		// HIGH1 is the second ramp highest value
		// MIDDLE0 is a dragrable square of the first ramp, change translate the ramp
		// MIDDLE1 is a dragrable square of the first ramp, change translate the ramp
		enum feature { NONE, LOW0, LOW1, HIGH0, HIGH1, MIDDLE0, MIDDLE1 };
		feature m_feature; ///< selected ramp feature
	};


}} // namespaces

#endif
