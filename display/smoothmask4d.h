/** @file	smoothmask4d.h
 *	@author	Herve Lombaert
 *	@date	March 1st, 2007
 *
 *	From a orthogonal mask, create a smooth mesh, and display it
 */

#ifndef SMOOTHMASK4D_H
#define SMOOTHMASK4D_H

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <queue>
#include <cmath>
#include <error.h>

namespace libvdDisplay
{
	/**
	 * @class SmoothMask4D
	 * @brief Display an smoothed mesh for a 4D mask
	 *
	 * Display a mesh for a mask. It is an isosurface whose isovalue
	 * represents the boundary of the mask.
	 *
	 * This classes uses a binary mask of any type. Possible values
	 * of the masks are 0 and anything but 0.
	 *
	 * For better results use the following OpenGL initialization:
	 *
	 * \code
	 *   glShadeModel(GL_SMOOTH);
	 *   glEnable(GL_DEPTH_TEST);
	 *   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 * \endcode
	 *
	 * To use it, use:
	 *
	 * \code
	 *   unsigned char* volume; // e.g., [512*512*512*10] volume
	 *   unsigned char* mask;   // e.g., [512*512*512*10] fill mask
	 *   Display::SmoothMask4D displayMask;
	 *
	 *   displayMask.update(volume, mask, 512,512,512,10);
	 *
	 *   glColor3f(0.8,0,0); // color of the mask
	 *   displayMask.display();
	 * \endcode
	 *
	 */

	class SmoothMask4D
	{
	public:
		SmoothMask4D();
		~SmoothMask4D();

		/** update the visualizer with a new mask
		 *  internally creates or update the display list
		 * @param data  volume data (we will get its gradient for the surface normal
		 * @param mask  binary mask (either 0 or else)
		 */
		template <typename DataType, typename MaskType>
		void update(const DataType* data,
					const MaskType* mask,
		            unsigned int width,
		            unsigned int height,
		            unsigned int depth,
					unsigned int nb_frames);

		/** set the frame number to display */
		void setFrame(unsigned int frame);

		/** display the display list */
		void display() const;

		/** clear the 4D mask, remove any display list */
		void clear();

	private:
		template <typename DataType>
		void distanceTransform(const DataType* data,
							  unsigned char* phi,
							  std::vector<int> &band_index,
							  unsigned int width,
							  unsigned int height,
							  unsigned int depth);

		/** march all cubes */
		template <typename DataType>
		void march_cubes(const DataType* data, ///< volume data
						 const unsigned char* mask, ///< band
						 std::vector<int> &band_index, ///< voxels in band
						 unsigned int width,
						 unsigned int height,
						 unsigned int depth);

		/** Marching a single cube
		 * Code from Paul Bourke (http://local.wasp.uwa.edu.au/~pbourke/modelling/polygonise/) */
		struct XYZ { double x,y,z; };
		struct TRIANGLE { XYZ p[3]; };
		struct GRIDCELL { XYZ p[8]; double val[8]; };
		static XYZ VertexInterp(double isolevel, XYZ p1, XYZ p2, double valp1, double valp2);
		static int Polygonise(GRIDCELL grid,double isolevel,TRIANGLE *triangles);

	private:
		GLuint m_meshid;          ///< mesh display list (id of first frame display list)
		unsigned int m_nb_frames; ///< how many frames
		unsigned int m_frame;     ///< current frame
	};

} // namespace



namespace libvdDisplay
{

	template <typename DataType, typename MaskType>
	void SmoothMask4D::update(const DataType* data,
							  const MaskType* mask,
							  unsigned int width,
							  unsigned int height,
							  unsigned int depth,
							  unsigned int nb_frames)
	{
		/* Create a new display list */
		if(glIsList(m_meshid)) glDeleteLists(m_meshid,m_nb_frames);
		m_nb_frames = nb_frames;
		m_meshid = glGenLists(m_nb_frames);

		/* distance transform */
		unsigned char* phi = new unsigned char [width*height*depth];

		for(unsigned int frame = 0; frame<m_nb_frames; ++frame)
		{
			glNewList(m_meshid+frame, GL_COMPILE);

			/* Scale to volume space */
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glTranslatef(-0.5f,-0.5f,-0.5f);
			glScalef(1.0f/width, 1.0f/height, 1.0f/depth);

			/* Nice coloring for the mesh */
			glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
			glEnable(GL_COLOR_MATERIAL);

			/* Lighting helps visualization of the mesh */
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);

			/* create smoothed mask */
			std::vector<int> band_index;
			distanceTransform(&mask[frame*width*height*depth], phi, band_index, width, height, depth);

			/* Create mesh */
			if(data)
				march_cubes(&data[frame*width*height*depth], phi, band_index, width, height, depth);
			else
				march_cubes(&mask[frame*width*height*depth], phi, band_index, width, height, depth);

			/* Leaving function */
			glDisable(GL_LIGHT0);
			glDisable(GL_LIGHTING);
			glDisable(GL_COLOR_MATERIAL);

			/* End list */
			glPopMatrix();
			glEndList();
		}

		/* distance transform */
		delete [] phi;
	}


	template <typename DataType>
	void SmoothMask4D::distanceTransform(const DataType* mask,
										unsigned char* phi,
										std::vector<int> &band_index, // voxels in band (only inner layers)
										unsigned int width,
										unsigned int height,
										unsigned int depth)
	{
		int neighbors[][3] = {
			{0,0,-1}, {0,-1,0}, {-1,0,0},
			{0,0,+1}, {0,+1,0}, {+1,0,0}
		};

		/////////////////////////////////////////////////
		// Build a distance transform within a small band

		// ping-pong boundaries
		std::queue<int> inner_boundary[2];
		std::queue<int> outer_boundary[2];
		bool current_list = 0;
		unsigned char zero = 128;
		const int max_radius = 5; // narrow band radius

		// empty phi
		for(unsigned int i=0; i<width*height*depth; ++i) phi[i] = zero;

		// find boundary and place +1 inside mask
		for(unsigned int k=0; k<depth; ++k)
			for(unsigned int j=0; j<height; ++j)
				for(unsigned int i=0; i<width; ++i)
		{
			int index = (k*height+j)*width+i;
			if(mask[index] != 0)
			{
				for(unsigned int m=0; m<6; ++m)
				{
					int ii = i + neighbors[m][0];
					int jj = j + neighbors[m][1];
					int kk = k + neighbors[m][2];

					if(ii >=0 && ii < (int)width && jj >= 0 && jj < (int)height && kk>=0 && kk<(int)depth)
					{
						// if on boundary inside mask
						if(mask[(kk*height+jj)*width+ii] == 0)
						{
							phi[index] = zero+1;
							inner_boundary[current_list].push(index); // inside
							break;
						}
					}
				}
			}
		}
		
		// find boundary and place -1 outside mask
		for(unsigned int k=0; k<depth; ++k)
			for(unsigned int j=0; j<height; ++j)
				for(unsigned int i=0; i<width; ++i)
		{
			int index = (k*height+j)*width+i;
			if(mask[index] == 0)
			{
				for(unsigned int m=0; m<6; ++m)
				{
					int ii = i + neighbors[m][0];
					int jj = j + neighbors[m][1];
					int kk = k + neighbors[m][2];

					if(ii >=0 && ii < (int)width && jj >= 0 && jj < (int)height && kk>=0 && kk<(int)depth)
					{
						// if on boundary outside mask
						if(mask[(kk*height+jj)*width+ii] != 0)
						{
							phi[index] = zero-1;
							outer_boundary[current_list].push(index); // outside
							break;
						}
					}
				}
			}
		}

		// grow inner layer until it is done
		current_list = 0;
		int layer = 1;
		while(!inner_boundary[current_list].empty() && layer < max_radius)
		{
			// get neighbors
			while(!inner_boundary[current_list].empty())
			{
				// get point
				int index = inner_boundary[current_list].front();
				inner_boundary[current_list].pop();

				// mark point
				if(phi[index] == zero+layer)
				{
					// put in list of voxels in band
					if(layer < max_radius-1)
						band_index.push_back(index);

					int k = index / (width*height);
					int j = (index - k*width*height) / width;
					int i = index - (k*height+j)*width;

					// push all neighbors
					for(unsigned int m=0; m<6; ++m)
					{
						int ii = i + neighbors[m][0];
						int jj = j + neighbors[m][1];
						int kk = k + neighbors[m][2];

						if(ii >=0 && ii < (int)width && jj >= 0 && jj < (int)height && kk>=0 && kk<(int)depth)
						{
							int nindex = (kk*height+jj)*width+ii;
							if(phi[nindex] == zero)
							{
								phi[nindex] = zero+layer+1;
								inner_boundary[!current_list].push(nindex);
							}
						}
					}
				}
			}

			// switch list
			current_list = !current_list;
			++layer;
		}
		
		// grow outer layer until it is done
		current_list = 0;
		layer = 1;
		while(!outer_boundary[current_list].empty() && layer < max_radius)
		{
			// get neighbors
			while(!outer_boundary[current_list].empty())
			{
				// get point
				int index = outer_boundary[current_list].front();
				outer_boundary[current_list].pop();

				// mark point
				if(phi[index] == zero-layer)
				{
					// put in list of voxels in band
					if(layer < max_radius-1)
						band_index.push_back(index);

					int k = index / (width*height);
					int j = (index - k*width*height) / width;
					int i = index - (k*height+j)*width;

					// push all neighbors
					for(unsigned int m=0; m<6; ++m)
					{
						int ii = i + neighbors[m][0];
						int jj = j + neighbors[m][1];
						int kk = k + neighbors[m][2];

						if(ii >=0 && ii < (int)width && jj >= 0 && jj < (int)height && kk>=0 && kk<(int)depth)
						{
							int nindex = (kk*height+jj)*width+ii;
							if(phi[nindex] == zero)
							{
								phi[nindex] = zero-layer-1;
								outer_boundary[!current_list].push(nindex);
							}
						}
					}
				}
			}

			// switch list
			current_list = !current_list;
			++layer;
		}
	}

	/* march all voxel of the volume, we want all isosurfaces */
	template <typename DataType>
	void SmoothMask4D::march_cubes(const DataType* data, // volume data
								   const unsigned char* mask, // band
								   std::vector<int> &band_index, // voxels in band
								   unsigned int width,
								   unsigned int height,
								   unsigned int depth)
	{
		try
		{
			float isovalue = 128; // zero in the unsigned char distance transform

			// iterate in the band
			for(std::vector<int>::const_iterator it = band_index.begin(); it != band_index.end(); ++it)
			{
				int k = *it / (width*height);
				int j = (*it - k*width*height) / width;
				int i = *it - (k*height+j)*width;

				if(i == (int)width-1 || j == (int)height-1 || k == (int)depth-1) continue; // cannot march cube in this case

				GRIDCELL cell;
				cell.p[0].x = i+0; cell.p[0].y = j+0; cell.p[0].z = k+0; cell.val[0] = mask[((k+0)*height+(j+0))*width+(i+0)];
				cell.p[1].x = i+1; cell.p[1].y = j+0; cell.p[1].z = k+0; cell.val[1] = mask[((k+0)*height+(j+0))*width+(i+1)];
				cell.p[2].x = i+1; cell.p[2].y = j+1; cell.p[2].z = k+0; cell.val[2] = mask[((k+0)*height+(j+1))*width+(i+1)];
				cell.p[3].x = i+0; cell.p[3].y = j+1; cell.p[3].z = k+0; cell.val[3] = mask[((k+0)*height+(j+1))*width+(i+0)];
				cell.p[4].x = i+0; cell.p[4].y = j+0; cell.p[4].z = k+1; cell.val[4] = mask[((k+1)*height+(j+0))*width+(i+0)];
				cell.p[5].x = i+1; cell.p[5].y = j+0; cell.p[5].z = k+1; cell.val[5] = mask[((k+1)*height+(j+0))*width+(i+1)];
				cell.p[6].x = i+1; cell.p[6].y = j+1; cell.p[6].z = k+1; cell.val[6] = mask[((k+1)*height+(j+1))*width+(i+1)];
				cell.p[7].x = i+0; cell.p[7].y = j+1; cell.p[7].z = k+1; cell.val[7] = mask[((k+1)*height+(j+1))*width+(i+0)];
		
				TRIANGLE triangles[5];
				int nbTriangles = 0;
		
				nbTriangles = Polygonise(cell,isovalue,triangles);

				/* use triangle normal to check if the volume gradient has to be inverted or not */
				for(int m=0; m<nbTriangles; ++m)
				{
					/* triangle points */
					float x1 = triangles[m].p[0].x; float y1 = triangles[m].p[0].y; float z1 = triangles[m].p[0].z;
					float x2 = triangles[m].p[1].x; float y2 = triangles[m].p[1].y; float z2 = triangles[m].p[1].z;
					float x3 = triangles[m].p[2].x; float y3 = triangles[m].p[2].y; float z3 = triangles[m].p[2].z;

					/* normal is (x1->x2) cross (x2->x3) */
					float u[3] = {x2-x1, y2-y1, z2-z1};
					float v[3] = {x3-x2, y3-y2, z3-z2};
					float nx = + (u[1]*v[2] - v[1]*u[2]);
					float ny = - (u[0]*v[2] - v[0]*u[2]);
					float nz = + (u[0]*v[1] - v[0]*u[1]);

					/* draw opengl triangle */
					if(!data)
					{
						glBegin(GL_TRIANGLES);
							glNormal3f(nx,ny,nz);
							glVertex3f(x1,y1,z1);
							glVertex3f(x2,y2,z2);
							glVertex3f(x3,y3,z3);
						glEnd();
					}

					/* if a volume is provided, use data gradient for normals */
					else
					{
						glBegin(GL_TRIANGLES);
						for(unsigned int p=0; p<3; ++p)
						{
							/* get vertex coordinate and gradient (central difference) */
							float x = triangles[m].p[p].x; float y = triangles[m].p[p].y; float z = triangles[m].p[p].z;
							int xp = (int)((x>0) ? x-1 : x);       // previous point (x+1)
							int xn = (int)((x<width-1) ? x+1 : x); // next point     (x-1)
							int yp = (int)((y>0) ? y-1 : y);
							int yn = (int)((y<height-1) ? y+1 : y);
							int zp = (int)((z>0) ? z-1 : z);
							int zn = (int)((z<depth-1) ? z+1 : z);

							/* compute volume gradient and normalize it */
							float gx = data[(((int)z)*height+((int)y))*width+(xn)] - data[(((int)z)*height+((int)y))*width+(xp)];
							float gy = data[(((int)z)*height+(yn))*width+((int)x)] - data[(((int)z)*height+(yp))*width+((int)x)];
							float gz = data[((zn)*height+((int)y))*width+((int)x)] - data[((zp)*height+((int)y))*width+((int)x)];
							float g = sqrt(gx*gx + gy*gy + gz*gz); // norm
							gx = gx/g; gy = gy/g; gz = gz/g; // normalize

							/* check if gradient points to the same direction as the triangle normal */
							float cos_theta = nx*gx + ny*gy + nz*gz;
							if(cos_theta < 0) // revert gradient
							{
								gx = -gx,
								gy = -gy,
								gz = -gz;
							}

							/* draw triangle */
							glNormal3f(gx,gy,gz); // normal
							glVertex3f(x,y,z);    // vertex
						}
						glEnd();
					}
				} // end for
			} // end band iteration
		}
		catch(Error &e) { e.tag("IsoSurface::march_cubes"); throw e; }
	}

} // namespace

#endif
