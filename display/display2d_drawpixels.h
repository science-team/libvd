/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	display2d_drawpixels.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 */

#ifndef DISPLAY2D_DRAWPIXELS_H
#define DISPLAY2D_DRAWPIXELS_H


#include <cstdlib>


namespace libvdDisplay
{
	/**
	 * Draw an image with \c glDrawPixels() (write to frame buffer)
	 * Currently support any native image type
	 * (ie. unsigned char, unsigned short, and others)
	 * use update and display when image is changed:
	 *
	 * \code
	 *	unsigned char* image = new unsigned char [100*100]; // fill data
	 *	Display::ImageDrawPixels disp;
	 *
	 *	disp.update(image, 100, 100);
	 *	disp.display();
	 * \endcode
	 *
	 * \image html displayimage.png
	 * \image latex displayimage.png
	 *
	 * Texture are not used, there is some performance issue when
	 * the texture needs to be update very fast (eg. playing a movie)
	 */


	class ImageDrawPixels {
	public:
		ImageDrawPixels();
		~ImageDrawPixels();
		
		/** Update the viewer with a new 2D image */
		template <typename DataType>
		void update(const DataType* image, unsigned int width, unsigned int height);

		/** Draw the image with glDrawPixels
		 * if you need to position the image somewhere else
		 * use glRaster(x,y) instead of glTranslate, also glScale cannot be used */
		void display() const;

		void* m_frame;            ///< frame buffer
		unsigned int m_size[2];   ///< frame size
		GLenum m_format;          ///< glDrawPixels param
		GLenum m_type;            ///< glDrawPixels param

	private:
		/// DrawPixels Arguments
		template <typename T>
		struct DrawPixelsArgs { static GLenum format, type; };
	};


	template <typename DataType>
	void ImageDrawPixels::update(const DataType* image, unsigned int width, unsigned int height)
	{
		if(m_size[0] != width ||
		   m_size[1] != height ||
		   m_format  != DrawPixelsArgs<DataType>::format ||
		   m_type    != DrawPixelsArgs<DataType>::type)
		{
			if(m_frame) free(m_frame); // delete any allocated memory

			// image size
			m_size[0] = width;
			m_size[1] = height;

			// allocate new memory
			m_frame = malloc(m_size[0]*m_size[1]*sizeof(DataType));
			
			// glDrawPixels arguments
			m_format = DrawPixelsArgs<DataType>::format;
			m_type   = DrawPixelsArgs<DataType>::type;
		}

		// fill frame buffer
		DataType* ptr = reinterpret_cast<DataType*>(m_frame);
		for(unsigned int i=0; i<width*height; ++i)
			ptr[i] = image[i];
	}


	// specialization for image type
	template <typename T> GLenum ImageDrawPixels::DrawPixelsArgs<T>   ::format = GL_LUMINANCE;


#ifdef COLOR_H

#ifndef GL_BGR
#define GL_BGR GL_BGR_EXT
#endif

	// specialization for image type when it is template of template
	template <typename T> struct ImageDrawPixels::DrawPixelsArgs<color<T> > { static GLenum format, type; };
	template <typename T> GLenum ImageDrawPixels::DrawPixelsArgs<color<T> >::type = DrawPixelsArgs<T>::type;
	template <typename T> GLenum ImageDrawPixels::DrawPixelsArgs<color<T> >::format = GL_BGR;

#endif


} // namespace


#endif
