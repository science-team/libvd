/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 *	@file	display4d.h
 *	@author	Herve Lombaert
 *	@date	October 10th, 2006
 *
 */

#ifndef DISPLAY4D_H
#define DISPLAY4D_H

#ifdef __linux__
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>
#endif

#include <limits>
#include <error.h>


namespace libvdDisplay
{
	/**
	 * @class Volume4D
	 * @brief 4D volume renderer
	 *
	 * This class plays a 4D dataset. It uses the volume renderer from Display3D.
	 * @see Display::Volume
	 *
	 * \image html displayvolume.png "A human heart"
	 * \image latex displayvolume.png "A human heart"
	 *
	 * Multiple 3D textures are used, one for each frame volume. It is therefore
	 * important to check if the whole sequence fits in the graphic card memory.
	 * Use \c setMaxTextureSize() to avoid problems. To change the coloring of
	 * the volume, a fragment shader implements a lookup table.
	 * @see setMaxTextureSize()
	 *
	 * When drawing the volume it is centered at (0,0,0) and has the
	 * unit size (1,1,1). You can scale or modify its orientation with
	 * \c glScale() and \c glMultMatrix() when drawing it in your application.
	 *
	 * Here is an example of code:
	 *
	 * \code
	 *	unsigned short* move; // also fill the data
	 *	Display::Volume4D disp;
	 *
	 *	disp.update(volume, 100,100,100, 10); // 10 frames
	 *	disp.updateRamp(0,500);
	 *	disp.display();
	 * \endcode
	 *
	 * If you want to blend an object with the volume, enable \c GL_DEPTH_TEST
	 * and draw the objects first before the volume.
	 *
	 * Internally, it uses 3D texture, as well as a fragment shader. That
	 * means you need to load OpenGL extensions before using this code.
	 * You can do it with for instance Glew, GLee.
	 *
	 * Internally, the lookup table has typically 4096 bins (the maximum
	 * texture size). This may be a problem if you have more than 4096
	 * values in your volume, the lut resolution decreases.
	 *
	 * Internally, it creates a temporary volume with normalized intensities
	 * with the right opengl compatible size. If the volume is too large
	 * the texture data is going to be smaller. This can be set with
	 * \c setMaxTextureSize().
	 *
	 * Different display modes are available:
	 *   VRT - the regular volume renderer where intensities accumulates
	 *   MIP - maximum intensity projection where only the max intensity is
	 *         displayed
	 *   MPR - shows an arbitrary slice of the volume, it might be usefull
	 *         to use this mode with alignWithScreen() to display the
	 *         mpr in a 2D window with other geometries, and with
	 *         setMprMatrix() to set the mpr plane orientation and position
	 */

	class Volume4D
	{
	public:
		enum Mode {
			VRT,	///< Volume Rendering Technique, accumulate voxel intensities (default)
			MIP,	///< Maximum Intensity Projection, display the brightest voxels
			MPR,	///< Multi Planar Reconstruction, an arbitrary slice of the volume
		};

		Volume4D();
		~Volume4D();

	public:

		/** update visualizer with a new volume
		 *  this creates a 3D texture on the graphic memory
		 *  (the max size is set with setMaxTextureSize) */
		template <typename DataType>
		void update(const DataType* data,
		            unsigned int width,
		            unsigned int height,
		            unsigned int depth,
					unsigned int nb_frames);

		/** display the 3D volume */
		void display() const;

		/** use a given frame */
		void setFrame(unsigned int frame);

		/** get the current frame number */
		unsigned int getFrame() const;

		/** get the number of frames */
		unsigned int getNbFrames() const;

		/** go to the next frame */
		void nextFrame();

		/** set rendering mode
		 *  @param mode rendering mode can be either VRT, MIP, MPR */
		void setMode(Mode mode);

		/** get rendering mode */
		Mode getMode() const;

		/** set mpr plane matrix */
		void setMprMatrix(const float matrix[]);

	public:

		/** update the lookup table, this uses a ramp to defines redish colors
		 *  @param min lower bound of the ramp
		 *  @param max higher bound of the ramp */
		void updateRamp(float min, float max);

		/** update lookup table, this uses an arbitrary lut
		 *  @param lut RGBA values, ranges from 0..1
		 *  @param lutSize number of RGBA entries, ranges from all posible data intensities */
		void updateLut(const float* lut, unsigned int lutSize);

		/** update lookup table with an external lut object
		 *  @param lut lut object, must implement the method "get(intensity, r,g,b,a)"
		 *  which takes an intensity and returns the corresponding rgba (normalized between 0..1)
		 *  @see Tools::TransferFunction::Ramp
		 *  @see Tools::TransferFunction::Trapezoid
		 *  @see Tools::TransferFunction::TransferFunction */
		template <typename Lut>
		void updateLut(const Lut &lut);

		/** align geometry with screen
		 *  all further geometry will be aligned with the screen,
		 *  it multiplies the modelview matrix by the inverse of the mpr plane matrix
		 *  make sure to apply all transformations (e.g., scaling) before aligning with screen
		 *  usefull when displaying the mpr plane */
		void alignWithScreen() const;

		/** set the rendering quality (default is 1)
		 *  it changes the number of rendering planes, usefull when interactivity becomes important
		 *  @param quality if quality is < 1, less planes are used however the rendering is much faster
		 *                 if the quality is > 1, more planes are used, however the rendering gets slower */
		void setQuality(float quality);

		/** use interpolation to generate intervoxels
		 *  @param interpolation 0 (no interpolation, use the nearest neighbor), 1 (use linear interpolation)
		 */
		void setInterpolation(int interpolation);

		/** set the maximum texture size
		 *  this must be a opengl compatible size (2^m) */
		void setMaxTextureSize(unsigned int w, unsigned int h, unsigned int d);

		/** get the intensity range of the displayed volume (values casted to float) */
		void getIntensityRange(float &min, float &max) const;

	protected:

		/** initialize the shader */
		void initShader();

		/** get the texture size
		 *  the texture size is opengl compatible */
		static void getTextureSize(const unsigned int data_size[],
								   unsigned int texture_size[],
								   const unsigned int max_texture_size[]);

		/** create texture data
		 *  the texture size is opengl compatible
		 *  intensities are normalized for a better lut resolution
		 *  @param min,max min and max intensity value of the data */
		template <typename DataType, typename DataOutType>
		static DataOutType* createTextureData(const DataType* data,
											  const unsigned int data_size[],
											  DataType &min,
											  DataType& max,
											  const unsigned int texture_size[]);

		/** render the planes in 3D
		 *  this is the actual volume renderer */
		void render() const;

		/** render a slice in 3D 
		 *  @param m plane matrix */
		void renderSlice(const float m[]) const;

		/** compute the number of rendering planes
		 *  evaluate how many pixels are used to display the volume */
		int getNbRenderingPlanes() const;

		/** transform a point with a matrix (pp = m*pp) */
		static void transf(const float p[], const float m[], float pp[]);

		/** transpose, also invert a orthonormal matrix */
		static void transp(float m[]);

		/** normalize the rotation part of the matrix */
		static void normalize(float m[]);

	protected:
		Mode m_rendering_mode;              ///< Rendering mode (default is VRT)
		float m_quality;                    ///< Rendering quality, this affects the number of rendering planes
		GLint m_interpolation;              ///< what interpolation to use
		GLuint* m_data_texture;             ///< handle on the 3D textures (one per frame volume)
		unsigned int m_nb_frames;           ///< number of frames
		unsigned int m_frame;               ///< the current frame volume
		unsigned int m_max_texture_size[3]; ///< maximum texture size (default is 256^3)
		float m_texture_scale[3];           ///< texture scale (scale to opengl compatible size)
		float m_intensity_range[2];         ///< data intensity range (min and max value of the data)
		float m_mpr_matrix[16];             ///< mpr plane matrix

	protected:
		GLuint m_gpu_program;     ///< handle on the GPU program (0 when uninitialized)
		GLuint m_fragment_shader; ///< handle on the fragment shader
		float* m_internal_lut;    ///< normalized lookup table (0..GL_MAX_TEXTURE_SIZE)
		GLuint m_lut_texture;     ///< handle on lookup table texture

	protected:
		/// provides usefull constants depending on the data type
		template <typename DataType>
		struct OpenGLArgs {
			static GLenum type;  ///< eg. GL_UNSIGNED_BYTE, or GL_FLOAT
			static DataType min; ///< eg. 0   for uchar, or -1.0f for float
			static DataType max; ///< eg. 255 for uchar, or +1.0f for float
		};
	};

} // namespace


//////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////

namespace libvdDisplay
{
	/**
	 *	Create a 3D texture
	 *	Template tools creates the right 3D texture
	 *  Initialize shader is this is the first update
	 */
	template <typename DataType>
	void Volume4D::update(const DataType* data,
	                    unsigned int width,
						unsigned int height,
						unsigned int depth,
						unsigned int nb_frames)
	{
		/* internal texture data */
		typedef unsigned char TextureType; // save some memory here
		//typedef typename DataType TextureType;

		/* check inputs */
		if(!data || !width || !height || !depth || !nb_frames) throw LOCATE(Error("wrong inputs"));

		/* if this is the first function call, create shaders */
		if(!m_gpu_program) initShader();

		/* use the GPU program */
		glUseProgram(m_gpu_program);

		/* get texture size */
		unsigned int data_size[3] = {width, height, depth};
		unsigned int texture_size[3];
		getTextureSize(data_size, texture_size, m_max_texture_size);
		DataType min_intensity = OpenGLArgs<DataType>::max;
		DataType max_intensity = OpenGLArgs<DataType>::min;

		/* set the texture scale */
		for(unsigned int i=0; i<3; ++i)
		{
			m_texture_scale[i] = (float)data_size[i] / texture_size[i];

			// if the texture size is smaller, the volume fits the whole texture (1.0f)
			if(m_texture_scale[i]>1) m_texture_scale[i] = 1.0f;
		}

		/* create texture handles */
		if(m_data_texture)
		{
			if(m_nb_frames != nb_frames)
			{
				glDeleteTextures(m_nb_frames, m_data_texture);
				delete [] m_data_texture;

				m_nb_frames = nb_frames;
				m_data_texture = new GLuint [nb_frames];
				glGenTextures(m_nb_frames, m_data_texture);
			}
		}
		else
		{
			m_nb_frames = nb_frames;
			m_data_texture = new GLuint [nb_frames];
			glGenTextures(m_nb_frames, m_data_texture);
		}

		/* iterate each frame volume */
		for(unsigned int frame=0; frame<m_nb_frames; ++frame)
		{
			/* create texture buffer */
			DataType min, max;
			const DataType* current_frame = &data[frame*data_size[0]*data_size[1]*data_size[2]];
			TextureType* buffer = createTextureData<DataType, TextureType>(current_frame, data_size, min, max, texture_size);
			if(min < min_intensity) min_intensity = min;
			if(max > max_intensity) max_intensity = max;

			/* Create and send 3D texture */
			glBindTexture(GL_TEXTURE_3D, m_data_texture[frame]); glCheckError();
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, m_interpolation);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, m_interpolation);
			glTexImage3D(GL_TEXTURE_3D,
						0,
						GL_LUMINANCE,
						texture_size[0],
						texture_size[1],
						texture_size[2],
						0,
						GL_LUMINANCE,
						OpenGLArgs<TextureType>::type,
						buffer);
			glCheckError();

			/* Check if memory has been allocated on graphic card */
			glEnable(GL_TEXTURE_3D);
			glBindTexture(GL_TEXTURE_3D, m_data_texture[frame]); glCheckError();
			glDisable(GL_TEXTURE_3D);

			/* delete texture buffer, it is now transfered to the graphic card */
			delete [] buffer;
		}

		/* use a default lookup table */
		m_intensity_range[0] = static_cast<float>(min_intensity);
		m_intensity_range[1] = static_cast<float>(max_intensity);
		updateRamp(m_intensity_range[0], m_intensity_range[1]);

		/* stop using the GPU program until requested */
		glUseProgram(0);

		/* use the first frame */
		m_frame = 0;
	}


	template <typename Lut>
	void Volume4D::updateLut(const Lut& lut)
	{
		/* use the GPU program */
		glUseProgram(m_gpu_program);

		/* internal lut is normalized (maximum texture size)
		 * lut[0] is the lowest volume value and
		 * lut[size-1] is the highest voluem value */
		GLint size = 4096;
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &size); // query what the graphic card max texture size

		/* set lookup table */
		if(!m_internal_lut) m_internal_lut = new float [size*4];

		/* copy lut */
		float scale = (float)(m_intensity_range[1]-m_intensity_range[0])/size;
		for(int i=0; i<size; ++i)
		{
			/* find where i should be in given lut
			 * need to convert i which ranges from m_minVal to m_maxVal (normalized to 0..size)
			 * to the lut size which is 0 to lutSize */
			float intensity = i*scale + m_intensity_range[0];

			float r=0,g=0,b=0,a=0;
			lut.get(intensity, r,g,b,a);

			m_internal_lut[i*4+0] = r;
			m_internal_lut[i*4+1] = g;
			m_internal_lut[i*4+2] = b;
			m_internal_lut[i*4+3] = a;
		}

		/* Create 1D texture for lookup table */
		if(!glIsTexture(m_lut_texture)) glGenTextures(1, &m_lut_texture);
		glBindTexture(GL_TEXTURE_2D, m_lut_texture); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
		             0,
		             GL_RGBA8,
		             size, // lut size
		             1,
		             0,
		             GL_RGBA, // color lookup table
		             GL_FLOAT,
		             m_internal_lut);
		glCheckError();

		/* NB: instead of freeing this unused memory, keep it there
			   so when updating it the next time, no time will be lost in memory allocation/deallocation */
		//delete [] m_internal_lut; m_internal_lut = 0;

		/* stop using the GPU program until requested */
		glUseProgram(0);
	}


	/**
	 *  create texture data
	 *  the texture size is opengl compatible
	 *  intensities are normalized for a better lut resolution
	 */
	template <typename DataType, typename DataOutType>
	DataOutType* Volume4D::createTextureData(const DataType* data,
											 const unsigned int data_size[],
											 DataType &min,
											 DataType& max,
											 const unsigned int texture_size[])
	{
		/* find max and min value of volume */
		min = OpenGLArgs<DataType>::max;
		max = OpenGLArgs<DataType>::min;
		for(const DataType* it=data; it!=&data[data_size[0]*data_size[1]*data_size[2]]; ++it)
		{
			if(*it < min) min = *it;
			if(*it > max) max = *it;
		}

		/* create new volume */
		DataOutType* buffer = new DataOutType [texture_size[0]*texture_size[1]*texture_size[2]];

		/* texture scale */
		float scale[3] = {
			(float)data_size[0] / texture_size[0],
			(float)data_size[1] / texture_size[1],
			(float)data_size[2] / texture_size[2]
		};

		/* fill new volume voxel per voxel
		 * if the texture size is larger than the volume size (scale<=1)
		 * the volume fits a portion of the texture and a texture scaling will be used
		 * if the data size is larger than the texture size (scale>1)
		 * the volume is scaled down to fit in the texture */
		for(unsigned int k=0; k<texture_size[2]; ++k)
			for(unsigned int j=0; j<texture_size[1]; ++j)
				for(unsigned int i=0; i<texture_size[0]; ++i)
		{
			/* corresponding coordinate in the volume */
			unsigned int I = (scale[0]>1) ? (unsigned int)(i*scale[0]) : i;
			unsigned int J = (scale[1]>1) ? (unsigned int)(j*scale[1]) : j;
			unsigned int K = (scale[2]>1) ? (unsigned int)(k*scale[2]) : k;

			/* texture value (either from volume, or 0 if outside) */
			DataType val = 0;
			if(I<data_size[0] && J<data_size[1] && K<data_size[2])
				val = data[(K*data_size[1]+J)*data_size[0]+I];

			/* normalize between 0..1.0 0..255 or 0..65535 */
			if(max>min)
				val = (val-min) * OpenGLArgs<DataOutType>::max / (max-min);

			/* fill texture buffer */
			buffer[(k*texture_size[1]+j)*texture_size[0]+i] = static_cast<DataOutType>(val);
		}

		return buffer;
	}


} // namespace

#endif
