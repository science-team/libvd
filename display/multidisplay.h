/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 *	@file	multidisplay.h
 *	@author	Herve Lombaert
 *	@date	March 23rd, 2007
 *
 */

#ifndef MULTIDISPLAY_H
#define MULTIDISPLAY_H

#include <vector>
#include <algorithm>

namespace libvdDisplay
{
	/**
	 * @class MultiDisplay
	 * @brief Display multiple volume renderer, blends them for visualisation
	 *
	 * This class renders multiple volume in the same scene. Display
	 * each renderer separately is difficult since the rendering planes
	 * needs to be display with the back-to-front constraint. This class
	 * handles this constraint.
	 *
	 * Here is an example of code:
	 *
	 * \code
	 *	Display::Volume disp1;
	 *	Display::Volume disp2;
	 *
	 *  Display::MultiDisplay blend;
	 *
	 *  blend.add(&disp1);
	 *  blend.add(&disp2);
	 *
	 *  blend.display();
	 * \endcode
	 *
	 */

	template <typename Renderer>
	class Multi
	{
	public:
		Multi();
		~Multi();

		/** Add a new renderer
		 * @param ren Renderer to use
		 * @param scalingX pixel scaling in X direction (between 0.0 and 1.0)
		 * @param scalingY pixel scaling in Y direction (between 0.0 and 1.0)
		 * @param scalingZ pixel scaling in Z direction (between 0.0 and 1.0)
		 */
		void add(Renderer* ren, float scalingX, float scalingY, float scalingZ);

		/** Remove a renderer */
		void remove(Renderer* ren);

		/** Display all renderers */
		void display() const;

	private:
		/** Derive a class, so we can access
		 *  protected members of each renderer */
		class Display : public Renderer
		{
			/** setup the current renderer */
			void tearUp() const;

			/** free handles of the current renderer */
			void tearDown() const;

			/** calls the right glBind(), 3D or 4D version */
			void bind3Dtexture() const;

			friend class Multi;
		};

		/** Pixel scaling associated with a renderer */
		struct PixelScaling
		{
			PixelScaling(float x,float y,float z) : x(x),y(y),z(z) {}
			float x,y,z; ///< pixel scaling (between 0 and 1)
		};

	private:
		std::vector<Display*> m_renderers; ///< Renderers
		std::vector<PixelScaling> m_scalings; ///< Pixel scaling for each renderer
	};
};

namespace libvdDisplay
{
	template <typename Renderer>
	Multi<Renderer>::Multi()
	{
	}

	template <typename Renderer>
	Multi<Renderer>::~Multi()
	{
	}

	template <typename Renderer>
	void Multi<Renderer>::add(Renderer* ren, float scalingX, float scalingY, float scalingZ)
	{
		Display* ptr = static_cast<Display*>(ren);

		// check if renderer is already in the list
		if(std::find(m_renderers.begin(), m_renderers.end(), ptr) == m_renderers.end())
		{
			m_renderers.push_back(ptr);
			m_scalings.push_back(PixelScaling(scalingX, scalingY, scalingZ));
		}
	}

	template <typename Renderer>
	void Multi<Renderer>::remove(Renderer* ren)
	{
		Display* ptr = static_cast<Display*>(ren);

		// check if renderer is already in the list
		typename std::vector<PixelScaling>::iterator pit = m_scalings.begin();
		for(typename std::vector<Display*>::iterator it = m_renderers.begin(); it != m_renderers.end(); ++it, ++pit)
		{
			if(*it == ptr)
			{
				it = m_renderers.erase(it);
				pit = m_scalings.erase(pit);
			}
		}
	}

	template <typename Renderer>
	void Multi<Renderer>::display() const
	{
		/* our plane is parallel to the screen
		   its rotation is the inverse rotation part of the modelview matrix */
		glMatrixMode(GL_MODELVIEW);
		float plane_matrix[16];
		glGetFloatv(GL_MODELVIEW_MATRIX, plane_matrix);
		Renderer::transp(plane_matrix); // invert modelview matrix, ignore translation
		plane_matrix[12] = plane_matrix[13] = plane_matrix[14] = 0;
		Renderer::normalize(plane_matrix);

		/* compute the number of planes */
		float nbPlanes = 0;
		for(typename std::vector<Display*>::const_iterator it = m_renderers.begin(); it != m_renderers.end(); ++it)
		{
			float nb = (*it)->getNbRenderingPlanes() * (*it)->m_quality;
			if(nb > nbPlanes)
				nbPlanes = nb;
		}
		float step = 1.73f / nbPlanes;

		/* loop and use a different rendering plane
		   move plane by step at each iteration
		   sqrt(3) is the maximum size of our plane (the diagonal of the cube)
		   0.87 is half of sqrt(3) */
		float epsilon = step * 0.1;
		for(float d=-0.87f; d<0.87f; d+=step)
		{
			float offset = 0;
			for(unsigned int r=0; r<m_renderers.size(); ++r)
			{
				/* scale the texture */
				float scaling[3] = {
					m_scalings[r].x,
					m_scalings[r].y,
					m_scalings[r].z,
				};

				glMatrixMode(GL_TEXTURE);
				glPushMatrix();
				glTranslatef(+0.5f,+0.5f,+0.5f); // center it
				glScalef(1.0f/scaling[0],
				         1.0f/scaling[1],
					 1.0f/scaling[2]); // change pixel spacing
				glTranslatef(-0.5f,-0.5f,-0.5f);
				glMatrixMode(GL_MODELVIEW);


				/* set the plane translation in the Z screen axis */
				plane_matrix[12] = (d+offset)*plane_matrix[8];
				plane_matrix[13] = (d+offset)*plane_matrix[9];
				plane_matrix[14] = (d+offset)*plane_matrix[10];

				/* use the right renderer */
				m_renderers[r]->tearUp();

				/* render one slice for this renderer */
				m_renderers[r]->renderSlice(plane_matrix);

				/* stop using renderer */
				m_renderers[r]->tearDown();

				/* offset next plane */
				offset += epsilon;

				glMatrixMode(GL_TEXTURE);
				glPopMatrix();
				glMatrixMode(GL_MODELVIEW);
			}
		}
	}

	template <typename Renderer>
	void Multi<Renderer>::Display::tearUp() const
	{
		/* check if the display has been initialized */
		if(!this->m_gpu_program) return;

		/* start using GPU program */
		glUseProgram(this->m_gpu_program);

		/* Use 1D texture for the lookup table */
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, this->m_lut_texture); glCheckError();
		glDisable(GL_TEXTURE_2D); // LUT is now in graphic memory

		/* Use 3D texture */
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_3D);
		bind3Dtexture();

		/* Position texture */
		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glScalef(this->m_texture_scale[0], this->m_texture_scale[1], this->m_texture_scale[2]);
		glTranslatef(0.5,0.5,0.5);   // center volume texture at (0,0,0)

		/* Decide which rendering mode to use */
		switch(this->m_rendering_mode)
		{
		case Display::VRT:
			/* accumulate intensities from all rendered planes */
			glEnable(GL_BLEND);
			glBlendEquation(GL_FUNC_ADD);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;

		case Display::MIP:
			/* display the maximum intensity from all rendered planes */
			glEnable(GL_BLEND);
			glBlendEquation(GL_MAX); // MIP, Maximum Intensity Projection
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;
		default: break;
		};
	}

	template <typename Renderer>
	void Multi<Renderer>::Display::tearDown() const
	{
		/* check if the display has been initialized */
		if(!this->m_gpu_program) return;

		glMatrixMode(GL_TEXTURE);
		glPopMatrix(); // texture

		glDisable(GL_TEXTURE_3D);

		/* stop using the program for further drawing */
		glUseProgram(0);

		glMatrixMode(GL_MODELVIEW);
	}

	template <typename Renderer>
	void Multi<Renderer>::Display::bind3Dtexture() const
	{
		glBindTexture(GL_TEXTURE_3D, this->m_data_texture); glCheckError();
	}
}

#include <display/display4d.h>

namespace libvdDisplay
{

	template <>
	void Multi<Volume4D>::Display::bind3Dtexture() const
	{
		glBindTexture(GL_TEXTURE_3D, this->m_data_texture[m_frame]); glCheckError();
	}
};

#endif
