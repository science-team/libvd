/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/** @file mask3d.h
 *  @author Herve Lombaert
 *  @date January 17th, 2006
 *
 */

#ifndef MASK3D_H
#define MASK3D_H

#include <error.h>

namespace libvdDisplay
{
	/**
	 * @class Mask3D
	 * @brief Display an orthogonal mesh for a 3D mask
	 *
	 * Display a mesh for a mask. It is made of tiny small cubes
	 * showing where the boundary of the mask is.
	 *
	 * This classes uses a binary mask of any type. Possible values
	 * of the masks are 0 and anything but 0.
	 *
	 * For better results use the following OpenGL initialization:
	 *
	 * \code
	 *   glShadeModel(GL_SMOOTH);
	 *   glEnable(GL_DEPTH_TEST);
	 *   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 * \endcode
	 *
	 * To use it, use:
	 *
	 * \code
	 *   unsigned char* mask; // e.g., [512*512*512] fill mask
	 *   Display::Mask3D displayMask;
	 *
	 *   displayMask.update(mask, 512,512,512);
	 *
	 *   glColor3f(0.8,0,0); // color of the mask
	 *   displayMask.display();
	 * \endcode
	 *
	 */

	class Mask3D
	{
	public:
		Mask3D();
		~Mask3D();

		/** update the visualizer with a new mask
		 *  internally creates or update the display list */
		template <typename DataType>
		void update(const DataType* data,
		            unsigned int width,
		            unsigned int height,
		            unsigned int depth);

		/** display the display list */
		void display();

		/** display directly the mask without using a display list
		 *  this can be used when the mask is interactively updated */
		template <typename DataType>
		void display(const DataType* data,
		             unsigned int width,
		             unsigned int height,
		             unsigned int depth);

		/** clear the mask, remove any display list */
		void clear();

	private:
		GLuint m_id;
	};


	template <typename DataType>
	void Mask3D::update(const DataType* data,
	                    unsigned int width,
	                    unsigned int height,
	                    unsigned int depth)
	{
		/* Create a new display list */
		if(glIsList(m_id)) glDeleteLists(m_id,1);
		m_id = glGenLists(1); glCheckError();
		glNewList(m_id, GL_COMPILE); glCheckError();

		/* Create mesh */
		display(data, width, height, depth);

		/* End list */
		glEndList();
	}


	template <typename DataType>
	void Mask3D::display(const DataType* data,
	                     unsigned int width,
	                     unsigned int height,
	                     unsigned int depth)
	{
		int neighbors[6][3] = { {-1,0,0},{+1,0,0},{0,-1,0},{0,+1,0},{0,0,-1},{0,0,+1} };

		/* cube
		 *
		 *      4----------5
		 *      |\         |\
		 *      | \        | \
		 *      |  7----------6
		 *      |  |       |  |
		 *      0--|-------1  |
		 *       \ |        \ |
		 *        \|         \|
		 *         3----------2
		 *
		 */

		// list of face points and normals
		// 4 first triplets are counter clockwise face point
		// last triplet is the face normal
		float offset[6][5][3] = {
		{	{-0.5,+0.5,+0.5}, // 74033047
			{-0.5,-0.5,+0.5},
			{-0.5,-0.5,-0.5},
			{-0.5,+0.5,-0.5},
			{-1,0,0}	},
		{	{+0.5,-0.5,+0.5}, // 56211265
			{+0.5,+0.5,+0.5},
			{+0.5,+0.5,-0.5},
			{+0.5,-0.5,-0.5},
			{+1,0,0}	},
		{	{-0.5,-0.5,+0.5}, // 45100154
			{+0.5,-0.5,+0.5},
			{+0.5,-0.5,-0.5},
			{-0.5,-0.5,-0.5},
			{0,-1,0}	},
		{	{+0.5,+0.5,+0.5}, // 67322376
			{-0.5,+0.5,+0.5},
			{-0.5,+0.5,-0.5},
			{+0.5,+0.5,-0.5},
			{0,+1,0}	},
		{	{-0.5,-0.5,-0.5}, // 01233210
			{+0.5,-0.5,-0.5},
			{+0.5,+0.5,-0.5},
			{-0.5,+0.5,-0.5},
			{0,0,-1}	},
		{	{-0.5,+0.5,+0.5}, // 76544567
			{+0.5,+0.5,+0.5},
			{+0.5,-0.5,+0.5},
			{-0.5,-0.5,+0.5},
			{0,0,+1}	}	};
  
		/* Scale to volume space */
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(-0.5,-0.5,-0.5);                  // offset half the volume
		glScalef(1.0f/width, 1.0f/height, 1.0f/depth); // unit is voxel
		glTranslatef(+0.5,+0.5,+0.5);                  // offset half voxel size

		/* Lighting helps visualization of the mesh */
		/* Nice coloring for the mesh */
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		/* start lists of quads */
		glBegin(GL_QUADS);

		for(unsigned int k=0; k<depth; ++k)
			for(unsigned int j=0; j<height; ++j)
				for(unsigned int i=0; i<width; ++i)
		{
			if(data[(k*height+j)*width+i] != 0)
			{
				for(int m=0; m<6; ++m)
				{
					bool drawFace = false;

					int ni = i+neighbors[m][0];
					int nj = j+neighbors[m][1];
					int nk = k+neighbors[m][2];

					if(ni>=0 && ni<(int)width &&
					   nj>=0 && nj<(int)height &&
					   nk>=0 && nk<(int)depth)
					{
						if(data[(nk*height+nj)*width+ni] == 0)
							drawFace = true;
					}
					else
						drawFace = true;

					if(drawFace)
					{
						/* Draw one face (outward), the interface of the mask */
						glNormal3f(offset[m][4][0], offset[m][4][1], offset[m][4][2]);
						glVertex3f(i+offset[m][0][0], j+offset[m][0][1], k+offset[m][0][2]);
						glVertex3f(i+offset[m][1][0], j+offset[m][1][1], k+offset[m][1][2]);
						glVertex3f(i+offset[m][2][0], j+offset[m][2][1], k+offset[m][2][2]);
						glVertex3f(i+offset[m][3][0], j+offset[m][3][1], k+offset[m][3][2]);
					}
				}
			}
		}

		glEnd();

		glPopMatrix();

		/* Leaving function */
		glPopAttrib();
  	}

} // namespace

#endif
