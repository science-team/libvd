/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/** @file point3d.h
 *  @author Herve Lombaert
 *  @date January 17th, 2006
 *
 */

#ifndef POINT3D_H
#define POINT3D_H

#include <error.h>

namespace libvdDisplay
{
	/**
	 * @class Point3D
	 * @brief Display a list of points
	 * 
	 * Display a list of 3D points. It is made of tiny small cubes
	 * showing where the points are. Points coordinates are 3 float
	 * values (x,y,z).
	 *
	 * Points are in world coordinates. If you are using volume
	 * coordinates, scale the points to the correct volume size
	 * before. The following example shows how to display the points
	 * with a volume displayed in a centered unit box:
	 *
	 * \code
	 *   glTranslatef(-0.5f,-0.5f,-0.5f);
	 *   glScalef(1.0f/width, 1.0f/height, 1.0f/depth);
	 *
	 *   points.display();
	 * \endcode
	 *
	 * For better results use the following OpenGL initialization:
	 *
	 * \code
	 *   glShadeModel(GL_SMOOTH);
	 *   glEnable(GL_DEPTH_TEST);
	 *   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 * \endcode
	 */

	class Point3D
	{
	public:
		Point3D();
		~Point3D();

		/** update the visualizer with a new list of point
		 *  points are in world coordinates
		 *  internally creates or update the display list
		 * @param points successive triplets (x,y,z) for each point
		 * @param nbPoints number of triplets */
		void update(const float* points,
		            unsigned int nbPoints);

		/** display the display list */
		void display();

		/** display directly a point
		 * point is in world coordinate
		 * @param data a triplet (x,y,z) */
		void display(const float* data);

	private:
		GLuint m_id;
	};

} // namespace

#endif
