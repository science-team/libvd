/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	display2d.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 */


/**
 * @namespace libvdDisplay
 * Namespace containing all classes that display objects.
 * Image viewer, volume renderer and 4D volume renderer are all in
 * the Display namespace.
 */


#ifndef DISPLAY2D_H
#define DISPLAY2D_H


#include <error.h>


namespace libvdDisplay
{
	/**
	 * @class Image
	 * @brief 2D image viewer
	 *
	 * Draw an image with 2D textures
	 * Currently support any native image type
	 * (ie. unsigned char, unsigned short, and others)
	 * use update and display when image is changed:
	 *
	 * \code
	 *	unsigned char* image = new unsigned char [100*100]; // fill the data
	 *	Display::Image disp;
	 *
	 *	disp.update(image);
	 *	disp.display();
	 * \endcode
	 *
	 * \image html displayimage.png
	 * \image latex displayimage.png
	 *
	 * If an image needs to be updated very fast (eg. playing a movie)
	 * transfers to the texture memory can become a bottleneck
	 * In this case, Display::ImageDrawPixels<T> might be more suitable
	 * as it directly writes the image in the frame buffer.
	 *
	 * \todo Should be able to display any image size
	 */


	class Image
	{
	public:
		Image();
		~Image();
		
		/** Update the viewer with a new 2D image */
		template <typename DataType>
		void update(const DataType* image, unsigned int width, unsigned int height);

		/** Draw the image on a centered unit square 
		 * use glTranslate/glScale to move the image around */
		void display() const;

	private:
		GLuint m_textureName;     ///< texture handle

	private:
		/// OpenGL constants
		template <typename DataType>
		struct OpenGLArgs { static GLenum format, type; };
	};

} // namespace


//////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////

namespace libvdDisplay
{
	template <typename DataType>
	void Image::update(const DataType* image, unsigned int width, unsigned int height)
	{
		// Create new texture if required
		if(!glIsTexture(m_textureName)) glGenTextures(1, &m_textureName);

		// Send texture to graphic card
		glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D,
		             0,
		             GL_RGBA,
		             width,
		             height,
		             0,
		             OpenGLArgs<DataType>::format,
		             OpenGLArgs<DataType>::type,
		             image);
		glCheckError();
	}


	// specialization for image type
	template <typename T> GLenum Image::OpenGLArgs<T>   ::format = GL_LUMINANCE;


#ifdef COLOR_H

#ifndef GL_BGR
#define GL_BGR GL_BGR_EXT
#endif

	// specialization for image type when it is template of template
	template <typename T> struct Image::OpenGLArgs<color<T> > { static GLenum format, type; };
	template <typename T> GLenum Image::OpenGLArgs<color<T> >::type = OpenGLArgs<T>::type;
	template <typename T> GLenum Image::OpenGLArgs<color<T> >::format = GL_BGR;

#endif


} // namespace


#endif
