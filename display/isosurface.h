/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	isosurface.h
 * @author	Herve Lombaert
 * @date	Oct 28th, 2006
 */

#ifndef ISOSURFACE_H
#define ISOSURFACE_H


#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include <cmath>
#include <error.h>


namespace libvdDisplay
{
	/**
	 * @class IsoSurface
	 * @brief Mesh generator with the marching cube algorithm
	 *
	 * Display an isosurface of a 3D image. It uses the marching cube algorithm
	 * to display the isosurface mesh.
	 *
	 * \image html displayisosurface.png
	 * \image latex displayisosurface.png
	 *
	 * To compute the normal of the mesh two options are available.
	 * The first method uses the triangle normals. The resulting mesh is
	 * blocky as the normal is identical on the whole triangle.
	 * The second method uses the data gradient. The resulting mesh is
	 * smoother as the normal is different on the triangle.
	 * The default is to use the gradient method as it gives a nicer mesh.
	 *
	 * It the isosurface to be displayed is showing a darker region,
	 * the surface normals must be changed as they normally points from
	 * bright to dark regions.
	 *
	 * For better results use the following OpenGL initialization:
	 *
	 * \code
	 *   glShadeModel(GL_SMOOTH);
	 *   glEnable(GL_DEPTH_TEST);
	 *   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 * \endcode
	 *
	 * The Marching Cube code is adapted from Paul Bourke code which can be found at:
	 * http://local.wasp.uwa.edu.au/~pbourke/modelling/polygonise/
	 */


	class IsoSurface
	{
	public:
		IsoSurface();
		~IsoSurface();

		/** display the isosurface mesh */
		void display() const;

		/** create a new mesh
		 * @param data   3D image to extract the isovalue
		 * @param width  width of the data
		 * @param height height of the data
		 * @param depth  depth of the data
		 * @param isovalue isovalue to extract
		 * @param from_bright_to_dark is the surface enclosing a bright (true) or dark (false) surface
		 * @param use_gradient compute the surface normals from the image gradient (smoother) rather than using triangle normals (produces facets) */
		template <typename DataType>
		void update(const DataType* data,
					unsigned int width,
					unsigned int height,
					unsigned depth,
					float isovalue,
					bool from_bright_to_dark = true,
					bool use_gradient = true);

	private:
		/** march all cubes */
		template <typename DataType>
		void march_cubes(const DataType* data,
						 unsigned int width,
						 unsigned int height,
						 unsigned int depth,
						 float isovalue,
						 bool from_bright_to_dark,
						 bool use_gradient);

		/** Marching a single cube
		 * Code from Paul Bourke (http://local.wasp.uwa.edu.au/~pbourke/modelling/polygonise/) */
		struct XYZ { double x,y,z; };
		struct TRIANGLE { XYZ p[3]; };
		struct GRIDCELL { XYZ p[8]; double val[8]; };
		static XYZ VertexInterp(double isolevel, XYZ p1, XYZ p2, double valp1, double valp2);
		static int Polygonise(GRIDCELL grid,double isolevel,TRIANGLE *triangles);

	private:
		GLuint m_meshid; ///< mesh display list
	};


	template <typename DataType>
	void IsoSurface::update(const DataType* data,
							unsigned int width,
							unsigned int height,
							unsigned depth,
							float isovalue,
							bool from_bright_to_dark,
							bool use_gradient)
	{
		/* Create a new display list */
		if(glIsList(m_meshid)) glDeleteLists(m_meshid,1);
		m_meshid = glGenLists(1);
		glNewList(m_meshid, GL_COMPILE);

		/* Scale to volume space */
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(-0.5,-0.5,-0.5);
		glScalef(1.0f/width, 1.0f/height, 1.0f/depth);

		/* Nice coloring for the mesh */
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);

		/* Lighting helps visualization of the mesh */
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		/* Create mesh */
		march_cubes(data, width, height, depth, isovalue, from_bright_to_dark, use_gradient);

		/* Leaving function */
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);

		/* End list */
		glPopMatrix();
		glEndList();
	}


	/* march all voxel of the volume, we want all isosurfaces */
	template <typename DataType>
	void IsoSurface::march_cubes(const DataType* data,
								 unsigned int width,
								 unsigned int height,
								 unsigned int depth,
								 float isovalue,
								 bool from_bright_to_dark,
								 bool use_gradient)
	{
		try
		{
			for(unsigned int k=0; k<depth-1; ++k)
				for(unsigned int j=0; j<height-1; ++j)
					for(unsigned int i=0; i<width-1; ++i)
			{
				GRIDCELL cell;
				cell.p[0].x = i+0; cell.p[0].y = j+0; cell.p[0].z = k+0; cell.val[0] = data[((k+0)*height+(j+0))*width+(i+0)];
				cell.p[1].x = i+1; cell.p[1].y = j+0; cell.p[1].z = k+0; cell.val[1] = data[((k+0)*height+(j+0))*width+(i+1)];
				cell.p[2].x = i+1; cell.p[2].y = j+1; cell.p[2].z = k+0; cell.val[2] = data[((k+0)*height+(j+1))*width+(i+1)];
				cell.p[3].x = i+0; cell.p[3].y = j+1; cell.p[3].z = k+0; cell.val[3] = data[((k+0)*height+(j+1))*width+(i+0)];
				cell.p[4].x = i+0; cell.p[4].y = j+0; cell.p[4].z = k+1; cell.val[4] = data[((k+1)*height+(j+0))*width+(i+0)];
				cell.p[5].x = i+1; cell.p[5].y = j+0; cell.p[5].z = k+1; cell.val[5] = data[((k+1)*height+(j+0))*width+(i+1)];
				cell.p[6].x = i+1; cell.p[6].y = j+1; cell.p[6].z = k+1; cell.val[6] = data[((k+1)*height+(j+1))*width+(i+1)];
				cell.p[7].x = i+0; cell.p[7].y = j+1; cell.p[7].z = k+1; cell.val[7] = data[((k+1)*height+(j+1))*width+(i+0)];
		
				TRIANGLE triangles[5];
				int nbTriangles = 0;
		
				nbTriangles = Polygonise(cell,isovalue,triangles);
		
				/* First version: uses triangle normals, no smooth surface
				   Second version: use volume gradient, smoother surface */
				if(!use_gradient)
				{
					for(int m=0; m<nbTriangles; ++m)
					{
						/* triangle points */
						float x1 = triangles[m].p[0].x; float y1 = triangles[m].p[0].y; float z1 = triangles[m].p[0].z;
						float x2 = triangles[m].p[1].x; float y2 = triangles[m].p[1].y; float z2 = triangles[m].p[1].z;
						float x3 = triangles[m].p[2].x; float y3 = triangles[m].p[2].y; float z3 = triangles[m].p[2].z;

						/* normal is (x1->x2) cross (x2->x3) */
						float u[3] = {x2-x1, y2-y1, z2-z1};
						float v[3] = {x3-x2, y3-y2, z3-z2};
						float gx = + (u[1]*v[2] - v[1]*u[2]);
						float gy = - (u[0]*v[2] - v[0]*u[2]);
						float gz = + (u[0]*v[1] - v[0]*u[1]);

						/* draw opengl triangle */
						glBegin(GL_TRIANGLES);
							glNormal3f(gx,gy,gz);
							glVertex3f(x1,y1,z1);
							glVertex3f(x2,y2,z2);
							glVertex3f(x3,y3,z3);
						glEnd();
					}
				}
				else
				{
					for(int m=0; m<nbTriangles; ++m)
					{
						glBegin(GL_TRIANGLES);
						for(unsigned int p=0; p<3; ++p)
						{
							/* get vertex coordinate and gradient (central difference) */
							float x = triangles[m].p[p].x; float y = triangles[m].p[p].y; float z = triangles[m].p[p].z;
							int xp = (int)((x>0) ? x-1 : x);                // previous point (x+1)
							int xn = (int)((x<width-1) ? x+1 : x); // next point     (x-1)
							int yp = (int)((y>0) ? y-1 : y);
							int yn = (int)((y<height-1) ? y+1 : y);
							int zp = (int)((z>0) ? z-1 : z);
							int zn = (int)((z<depth-1) ? z+1 : z);

							/* compute volume gradient and normalize it */
							float gx = data[(((int)z)*height+((int)y))*width+(xn)] - data[(((int)z)*height+((int)y))*width+(xp)];
							float gy = data[(((int)z)*height+(yn))*width+((int)x)] - data[(((int)z)*height+(yp))*width+((int)x)];
							float gz = data[((zn)*height+((int)y))*width+((int)x)] - data[((zp)*height+((int)y))*width+((int)x)];
							float g = sqrt(gx*gx + gy*gy + gz*gz); // norm
							gx = gx/g; gy = gy/g; gz = gz/g; // normalize

							/* if the normal points from bright to dark, use the opposite of the gradient */
							if(from_bright_to_dark)
								gx = -gx,
								gy = -gy,
								gz = -gz;

							/* draw triangle */
							glNormal3f(gx,gy,gz); // normal
							glVertex3f(x,y,z);    // vertex
						}
						glEnd();
					}
				}
			}
		}
		catch(Error &e) { e.tag("IsoSurface::march_cubes"); throw e; }
	}

} // namespace

#endif
