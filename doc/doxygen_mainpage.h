/**
 * @mainpage libvd - visualization library
 *
 * \section What
 * libvd is a very simple OpenGL visualization library. It has a 3D volume renderer.
 * As each functionnality is well isolated and commented, this library is perfect for
 * quick visualization, or for learning.
 *
 * \image html libvd-demo.png "A clip plane is used to visualize the cardiac chambers (the left atrium is picked with the red marker) from a 128x128x128 volume.<br>Visualization is controled with a transfer function on the lower left corner"
 * \image latex libvd-demo.png "A clip plane is used to visualize the cardiac chambers (the left atrium is picked with the red marker) from a 128x128x128 volume.<br>Visualization is controled with a transfer function on the lower left corner"
 *
 * \section Why
 * While much larger scale visualization libraries exist out there (e.g. vtk), the learning
 * curve might be steep. If you like simple code that works in almost any situation, or if
 * if you like to h ack your own code, this library will be great for you.
 *
 * \section How
 * Here are some lines required to visualize a volume:
 *
 * \code
 *	// Create and read a volume
 *	unsigned short* volume = NULL;
 *	IO::Raw::read(volume, "volume.img", 512,512,370);
 *
 *	// Create a volume renderer
 *	Display::Volume renderer;
 *	renderer.update(volume);
 *
 *	// Display the volume in OpenGL
 *	renderer.display();
 * \endcode
 *
 * Pretty simple, eh?
 *
 * \section When
 * When I started my PhD in 2005, I needed a 3D visualization library. So I
 * decided to code my own, just because I like coding.
 *
 * \section But
 * As this project has no commercial target, and as it is still a work
 * in progress, the code and interface will change from time to time.
 * If you are willing to do some serious visualization you should probably
 * be using VTK. This project is just for fun. It might be handy if you
 * like to hack code.
 *
 */
