/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	error.h
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifndef ERROR_H
#define ERROR_H

/**
 * Error class with a syntax similar to \c printf:
 *
 * \code
 *	if(error)
 *	  throw Error("my message %d %f", 2, 2.5f);
 * \endcode
 *
 * or:
 *
 * \code
 *	catch(const Error &e) {
 *	  e.tag("my message %d %f", 2, 2.5f);
 *	  throw e;
 *	}
 * \endcode
 *
 * To embed the message with \c __FILE__,__LINE__ use the \c "LOCATE" macro
 *
 * \code
 *	throw LOCATE(Error("my error"));
 * \endcode
 *
 * When catching and printing the error message it will output a message
 * similar to:
 *
 * \verbatim
 *	error.h:27
 *		my error
 * \endverbatim
 *
 * Also check for OpenGL error with \c glCheckError();
 *
 ***************************************************************************/

#include <cstdio>
#include <cstdarg>
#include <cstring>

#if _WIN32
# pragma warning(disable:4996)
# ifndef snprintf
#  define snprintf _snprintf
# endif
#endif


/** Bare error class
 *  You could use an error message "a la" printf */
struct Error {
	/** Creates an Error with a message
	 *  Use a printf-like syntax
	 */
	Error(const char* msg, ...)
	{
		va_list arg;
		va_start(arg, msg);
		vsnprintf(this->msg, 1024, msg, arg);
		va_end(arg);
	}

	/** Sets an Error message "a la" printf()
	 *  Adds a line to the error description with a filename/code line
	 */
	void tag(const char* msg, ...)
	{
		char newline[1024];
		va_list arg;
		va_start(arg, msg);
		vsnprintf(newline, 1024, msg, arg);
		va_end(arg);
		char newmsg[1024];
		snprintf(newmsg, 1024, "%s\n%s", this->msg, newline);
		strncpy(this->msg, newmsg, 1024);
	}

	/** Print the error message on stdout */
	void print() const
	{
		printf("[error message]\n%s\n", msg);
	}

	char msg[1024];     ///< error message
};


#if _WIN32
# pragma warning(default:4996)
#endif


/** Range error, when a value is out of bound */
struct RangeError : public Error {
	RangeError(const char* msg, float val, float lowerbound, float upperbound)
		:	Error("RangeError: %s, got: %f (bound: %f:%f)", msg, val, lowerbound, upperbound) {} ///< constructor
};

/** Check a test */
#define CHECK(msg,testvalid) { if((testvalid)==0) throw LOCATE(Error(msg)); }

/** Check range, test is low <= val < up */
#define CHECKRANGE(msg,val,low,up) { if((val)<(low) || (val)>=(up)) throw LOCATE(RangeError(msg,(float)val,(float)low,(float)up)); }

/** Check minimum, test is val >= min */
#define CHECKMIN(msg,val,min) { if((val)<(min)) throw LOCATE(RangeError(msg,(float)val,(float)min,0)); }

/** Check maximum, test is val < max */
#define CHECKMAX(msg,val,max) { if((val)>=(max)) throw LOCATE(RangeError(msg,(float)val,0,(float)max)); }

/** Tag error with filename, code line */
#define LOCATE(Error) LocateError(Error, __FILE__, __LINE__)

/** Error containing information on its location */
struct LocateError {
	template <typename Error>
	LocateError(Error e, const char* file, int line) { e.tag("\t%s:%d", file, line); throw e; } ///< constructor
};


#ifdef GL_VERSION

# ifndef GLU_VERSION
#  ifdef __APPLE__
#   include <OpenGL/glu.h>
#  else
#   include <GL/glu.h>
#  endif
# endif

/** OpenGL error, use glCheckError() to generate it */
struct OpenGLError : public Error {
	OpenGLError(GLenum err)
		:	Error("OpenGL error 0x%x: %s\n", err, gluErrorString(err)) {} ///< constructor
};

/** Check for OpenGL error */
#define glCheckError() \
{\
	GLenum err = glGetError();\
	if(err != GL_NO_ERROR)\
		throw LOCATE(OpenGLError(err));\
}

#else

// gl.h were not included yet - generate a compilation warning
# pragma message(__FILE__ " : warning: OpenGL errors not handled, <GL/gl.h> is not included before <error.h>")

#endif


#endif
