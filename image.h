/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	image.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 */


#ifndef IMAGE_H
#define IMAGE_H

#include <error.h>


/**
 * This is a template image class. It can be used to create
 * Images of any type, float, int, or even color.
 *
 * This interface can be used as a model for algorithm inputs
 * and outputs. Simply copy this file and start your own
 * implementation of your own image type.
 *
 * Internally, it keeps a continuous memory of the image data
 */


template <typename T, unsigned int N=2>
class Image {
public:
	typedef T type;   ///< type of image (eg. float, uchar, color)
	enum { dim = N }; ///< dimension of image (eg. 2, 3)
	typedef unsigned int uint; /// index type

public:
	// Some constructors creating an image with a given size
	Image();                                                   ///< no image
	Image(uint width, uint height);                            ///< 2D image
	Image(uint width, uint height, uint depth);                ///< 3D image
	Image(const T* data, uint width, uint height);             ///< 2D image with data
	Image(const T* data, uint width, uint height, uint depth); ///< 3D image with data
	Image(const uint size[]); ///< N-D
	Image(const Image&);      ///< copy constructor
	~Image();                 ///< destructor

	// Set/Get image size
	void        setSize(const uint size[]);              ///< set image size
	const uint* getSize()       const { return m_size; } ///< get image size
	      uint  getSize(uint n) const;                   ///< Get size of a dimension
	const uint*    size()       const { return m_size; } ///< get image size
	      uint     size(uint n) const;                   ///< Get size of a dimension

	// Set/Get image size - 2D/3D shortcuts
	void        setSize(uint width, uint height);             ///< set size in 2D
	void        setSize(uint width, uint height, uint depth); ///< set size in 3D
	
	// Set/Get image data
	const T& get(const uint index[])         const; ///< get data
	void     set(const uint index[], const T &val); ///< set data

	// Set/Get image data - 2D/3D shortcuts
	const T& get(uint i, uint j)         const; ///< get data in 2D
	const T& get(uint i, uint j, uint k) const; ///< get data in 3D
	void     set(uint i, uint j, const T &val); ///< set data in 2D
	void     set(uint i, uint j, uint k, const T &val); ///< set data in 3D
	
public:
	typedef T* iterator; ///< iterator definition

	/** get an iterator pointing at the beginning of the image data */
	iterator begin()
	{ return reinterpret_cast<iterator>(&m_data[0]); }

	/** get an iterator pointing at the end of the image data */
	iterator end()
	{ uint size = 1; for(uint m=0; m<N; ++m) size = size * m_size[m];
	  return reinterpret_cast<iterator>(&m_data[size-1]+1); }


	typedef const T* const_iterator; ///< const iterator definition

	/** get an iterator pointing at the beginning of the image data */
	const_iterator begin() const
	{ return reinterpret_cast<const_iterator>(&m_data[0]); }

	/** get an iterator pointing at the end of the image data */
	const_iterator end() const
	{ uint size = 1; for(uint m=0; m<N; ++m) size = size * m_size[m];
	  return reinterpret_cast<const_iterator>(&m_data[size-1]+1); }

protected:
	T* m_data;      ///< image data
	uint m_size[N]; ///< image size (width, height)
};


/****************************************************************************/
/* Implementation Below                                                     */
/****************************************************************************/


template <typename T, unsigned int N>
Image<T, N>::Image()
:	m_data(0)
{
	for(unsigned int n=0; n<N; ++n) m_size[n] = 0;
}


template <typename T, unsigned int N>
Image<T, N>::Image(uint width, uint height)
:	m_data(0)
{
	// Allocate memory for image
	setSize(width, height);
}


template <typename T, unsigned int N>
Image<T, N>::Image(uint width, uint height, uint depth)
:	m_data(0)
{
	// Allocate memory for image
	setSize(width, height, depth);
}


template <typename T, unsigned int N>
Image<T, N>::Image(const T* data, uint width, uint height)
:	m_data(0)
{
	// Allocate memory for image
	setSize(width, height);

	for(unsigned int i=0; i<width*height; ++i) m_data[i] = data[i];
}


template <typename T, unsigned int N>
Image<T, N>::Image(const T* data, uint width, uint height, uint depth)
:	m_data(0)
{
	// Allocate memory for image
	setSize(width, height, depth);

	for(unsigned int i=0; i<width*height*depth; ++i) m_data[i] = data[i];
}


template <typename T, unsigned int N>
Image<T, N>::Image(const uint size[])
:	m_data(0)
{
	// Allocate memory for image
	setSize(size);
}


template <typename T, unsigned int N>
Image<T, N>::Image(const Image& image)
:	m_data(0)
{
	// Allocate memory for image
	for(uint n=0; n<N; ++n) m_size[n] = image.m_size[n];
	setSize(m_size);
	
	// Copy image
	uint totalmem = 1;
	for(uint n=0; n<N; ++n) totalmem *= m_size[n];
	for(uint i=0; i<totalmem; ++i) m_data[i] = image.m_data[i];
}


template <typename T, unsigned int N>
Image<T, N>::~Image()
{
	// Destroy data memory
	if(m_data) delete [] m_data;
}


template <typename T, unsigned int N>
void Image<T, N>::setSize(uint width, uint height)
{
	// Set the new image size
	m_size[0] = width;
	m_size[1] = height;
	
	// Delete any previous memory
	if(m_data) delete [] m_data;
	m_data = 0;
	
	// If a dimension is 0, leave data uninitialized
	if(!m_size[0] || !m_size[1]) return;
	
	// Allocate memory
	m_data = new T [width*height];
	CHECK("allocated memory", m_data); // check if memory is allocated
	
	// Initialize it with 0s
	for(uint i=0; i<width*height; ++i)
		m_data[i] = 0;
}


template <typename T, unsigned int N>
void Image<T, N>::setSize(uint width, uint height, uint depth)
{
	// Set the new image size
	m_size[0] = width;
	m_size[1] = height;
	m_size[2] = depth;
	
	// Delete any previous memory
	if(m_data) delete [] m_data;
	m_data = 0;
	
	// If a dimension is 0, leave data uninitialized
	if(!m_size[0] || !m_size[1] || !m_size[2]) return;
	
	// Allocate memory
	m_data = new T [width*height*depth];
	CHECK("allocated memory", m_data); // check if memory is allocated
	
	// Initialize it with 0s
	for(uint i=0; i<width*height*depth; ++i)
		m_data[i] = 0;
}


template <typename T, unsigned int N>
void Image<T, N>::setSize(const uint size[])
{
	// Set the new image size
	for(uint n=0; n<N; ++n) m_size[n] = size[n];
	
	// Delete any previous memory
	if(m_data) delete [] m_data;
	m_data = 0;
	
	// If a dimension is 0, leave data uninitialized
	for(uint n=0; n<N; ++n) if(!m_size[n]) return;
	
	// Allocate memory
	uint totalmem = 1;
	for(uint n=0; n<N; ++n) totalmem *= m_size[n];
	m_data = new T [totalmem];
	CHECK("allocated memory", m_data); // check if memory is allocated
	
	// Initialize it with 0s
	for(uint i=0; i<totalmem; ++i)
		m_data[i] = 0;
}


template <typename T, unsigned int N>
unsigned int Image<T, N>::getSize(uint n) const
{
	CHECKRANGE("dimension", n, 0, N);
	return m_size[n];
}


template <typename T, unsigned int N>
unsigned int Image<T, N>::size(uint n) const
{
	CHECKRANGE("dimension", n, 0, N);
	return m_size[n];
}


template <typename T, unsigned int N>
const T& Image<T, N>::get(uint i, uint j) const
{
	// Check if wrong coordinate
	CHECK("initialization", m_data);
	CHECKRANGE("size x", i, 0, m_size[0]);
	CHECKRANGE("size y", j, 0, m_size[1]);

	// Return the intensity at pixel (i,j)
	uint index = (j*m_size[0])+i;
	return m_data[index];
}


template <typename T, unsigned int N>
const T& Image<T, N>::get(uint i, uint j, uint k) const
{
	// Check if wrong coordinate
	CHECK("initialization", m_data);
	CHECKRANGE("size x", i, 0, m_size[0]);
	CHECKRANGE("size y", j, 0, m_size[1]);
	CHECKRANGE("size z", k, 0, m_size[2]);

	// Return the intensity at pixel (i,j)
	uint index = ((k*m_size[1]+j)*m_size[0])+i;
	return m_data[index];
}


template <typename T, unsigned int N>
const T& Image<T, N>::get(const uint index[]) const
{
	// Check if wrong coordinate
	CHECK("initialization", m_data);
	for(uint n=0; n<N; ++n)
		CHECKRANGE("size", index[n], 0, m_size[n]);

	// Return the intensity at pixel (i,j)
	uint offset = index[N-1];
	for(uint n=N-1; n>0; --n)
		offset = offset*m_size[n-1] + index[n-1];
	return m_data[offset];
}


template <typename T, unsigned int N>
void Image<T, N>::set(uint i, uint j, const T &val)
{
	// Check if wrong coordinate
	CHECK("initialization", m_data);
	CHECKRANGE("size x", i, 0, m_size[0]);
	CHECKRANGE("size y", j, 0, m_size[1]);

	// Set intensity at pixel (i,j)
	uint index = (j*m_size[0])+i;
	m_data[index] = val;
}


template <typename T, unsigned int N>
void Image<T, N>::set(uint i, uint j, uint k, const T &val)
{
	// Check if wrong coordinate
	CHECK("initialization", m_data);
	CHECKRANGE("size x", i, 0, m_size[0]);
	CHECKRANGE("size y", j, 0, m_size[1]);
	CHECKRANGE("size z", k, 0, m_size[2]);

	// Set intensity at pixel (i,j)
	uint index = ((k*m_size[1]+j)*m_size[0])+i;
	m_data[index] = val;
}


template <typename T, unsigned int N>
void Image<T, N>::set(const uint index[], const T &val)
{
	// Check if wrong coordinate
	if(!m_data) throw Error("no data");
	for(uint n=0; n<N; ++n)
		CHECKRANGE("size", index[n], 0, m_size[n]);

	// Return the intensity at pixel (i,j)
	uint offset = index[N-1];
	for(uint n=N-1; n>0; --n)
		offset = offset*m_size[n-1] + index[n-1];
	m_data[offset] = val;
}


#endif
