/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	display4d.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include "msvc/GLee.h"
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
//This file uses prototypes that are only avilable in opengl 2.0. 
// to use this, we must use the opengl extensions file, where
// these prototypes are DISABLED unless GL_GLEXT_PROTOTYPES is defined
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>
#endif

#include <display/display4d.h>
#include <cmath>
#include <vector>
#include <map>
#include <utility>

//////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////

namespace libvdDisplay
{
	Volume4D::Volume4D()
	:	m_rendering_mode(VRT), // default mode is volume rendering
		m_quality(1),
		m_interpolation(GL_LINEAR),
		m_data_texture(0),
		m_nb_frames(0),
		m_frame(0),
		m_gpu_program(0),
		m_fragment_shader(0),
		m_internal_lut(0),
		m_lut_texture(0)
	{
		/* default maximum texture size is 256^3 */
		m_max_texture_size[0] = m_max_texture_size[1] = m_max_texture_size[2] = 256;

		/* min and max intensities of data */
		m_intensity_range[0] = m_intensity_range[1] = 0;

		/* set a default plane */
		m_mpr_matrix[0]  = 1, m_mpr_matrix[1]  = 0, m_mpr_matrix[2]  = 0, m_mpr_matrix[3]  = 0;
		m_mpr_matrix[4]  = 0, m_mpr_matrix[5]  = 1, m_mpr_matrix[6]  = 0, m_mpr_matrix[7]  = 0;
		m_mpr_matrix[8]  = 0, m_mpr_matrix[9]  = 0, m_mpr_matrix[10] = 1, m_mpr_matrix[11] = 0;
		m_mpr_matrix[12] = 0, m_mpr_matrix[13] = 0, m_mpr_matrix[14] = 0, m_mpr_matrix[15] = 1;
	}


	Volume4D::~Volume4D()
	{
		if(m_internal_lut) delete [] m_internal_lut;

		// If this is called after the display has been destroyed, this call will be undefined
		if(m_data_texture)
		{
			glDeleteTextures(m_nb_frames, m_data_texture);
			delete [] m_data_texture;
		}
		if(glIsTexture(m_lut_texture))
			glDeleteTextures(1, &m_lut_texture);
	}


	void Volume4D::display() const
	{
		/* check if the display has been initialized */
		if(!m_gpu_program) return;

		/* start using GPU program */
		glUseProgram(m_gpu_program);

		/* Use 1D texture for the lookup table */
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, m_lut_texture); glCheckError();
		glDisable(GL_TEXTURE_2D); // LUT is now in graphic memory

		/* Use 3D texture */
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_3D);
		glBindTexture(GL_TEXTURE_3D, m_data_texture[m_frame]); glCheckError();

		/* Position texture */
		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glScalef(m_texture_scale[0], m_texture_scale[1], m_texture_scale[2]);
		glTranslatef(0.5,0.5,0.5);   // center volume texture at (0,0,0)

		/* Decide which rendering mode to use */
		switch(m_rendering_mode)
		{
		case VRT:
			/* accumulate intensities from all rendered planes */
			glEnable(GL_BLEND);
			glBlendEquation(GL_FUNC_ADD);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			render();                // render the 3D textures on the planes
			glDisable(GL_BLEND);
			break;

		case MIP:
			/* display the maximum intensity from all rendered planes */
			glEnable(GL_BLEND);
			glBlendEquation(GL_MAX); // MIP, Maximum Intensity Projection
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			render();                // render the 3D textures on the planes
			glDisable(GL_BLEND);
			break;

		case MPR:
			/* render a single plane */
			renderSlice(m_mpr_matrix);
			break;
		};

		glMatrixMode(GL_TEXTURE);
		glPopMatrix(); // texture

		glDisable(GL_TEXTURE_3D);

		/* stop using the program for further drawing */
		glUseProgram(0);

		glMatrixMode(GL_MODELVIEW);
	}


	void Volume4D::setInterpolation(int interpolation)
	{
		if(interpolation == 0)
			m_interpolation = GL_NEAREST;
		else
			m_interpolation = GL_LINEAR;
	}


	void Volume4D::setFrame(unsigned int frame)
	{
		CHECKRANGE("frame number invalid", frame, 0, m_nb_frames);
		m_frame = frame;
	}

	
	unsigned int Volume4D::getFrame() const
	{
		return m_frame;
	}

	
	unsigned int Volume4D::getNbFrames() const
	{
		return m_nb_frames;
	}


	void Volume4D::nextFrame()
	{
		++m_frame;
		if(m_frame == m_nb_frames)
			m_frame = 0;
	}


	void Volume4D::setMode(Mode mode)
	{
		if(mode > MPR) throw LOCATE(Error("Unknown rendering mode: %d", m_rendering_mode));
		m_rendering_mode = mode;
	}


	Volume4D::Mode Volume4D::getMode() const
	{
		return m_rendering_mode;
	}


	void Volume4D::setMprMatrix(const float matrix[])
	{
		for(unsigned int i=0; i<16; ++i) m_mpr_matrix[i] = matrix[i];
	}


	void Volume4D::render() const
	{
		/* our plane is parallel to the screen
		   its rotation is the inverse rotation part of the modelview matrix */
		glMatrixMode(GL_MODELVIEW);
		float plane_matrix[16];
		glGetFloatv(GL_MODELVIEW_MATRIX, plane_matrix);
		transp(plane_matrix); // invert modelview matrix, ignore translation
		plane_matrix[12] = plane_matrix[13] = plane_matrix[14] = 0;
		normalize(plane_matrix);

		/* compute the number of planes */
		float nbPlanes = getNbRenderingPlanes() * m_quality;
		float step = 1.73f / nbPlanes;

		/* loop and use a different rendering plane
		   move plane by step at each iteration
		   sqrt(3) is the maximum size of our plane (the diagonal of the cube)
		   0.87 is half of sqrt(3) */
		for(float d=-0.87f; d<0.87f; d+=step)
		{
			/* set the plane translation in the Z screen axis */
			plane_matrix[12] = d*plane_matrix[8];
			plane_matrix[13] = d*plane_matrix[9];
			plane_matrix[14] = d*plane_matrix[10];

			/* render plane */
			renderSlice(plane_matrix);
		}
	}


	void Volume4D::renderSlice(const float m[]) const
	{
/* Two versions here
 * First method displays stacked squares
 *   simple code, but as squares must be large in order to fit the cube's cross section
 *   more texture computation is required
 * Second method displays stacked cross-section
 *   uglier code in order to compute the cross-section (sorting of crossing edges)
 *   however only the cross-section is displayed, no unnecessary rendering is done outside the cube
 */
#if 0
		/* NB: sqrt(3) is the maximum size of our plane (the diagonal of the cube)
			   0.87 is half of sqrt(3) */

		/* a3 ---- a2
		    |      |
		   a0 ---- a1 */

		/* flat plane parallel to the screen in camera coordinate */
		float p0[3] = { -0.87f,-0.87f,0 };
		float p1[3] = { +0.87f,-0.87f,0 };
		float p2[3] = { +0.87f,+0.87f,0 };
		float p3[3] = { -0.87f,+0.87f,0 };

		/* convert a plane from camera to world coordinate */
		float a0[3]; transf(p0,m,a0);
		float a1[3]; transf(p1,m,a1);
		float a2[3]; transf(p2,m,a2);
		float a3[3]; transf(p3,m,a3);

		/* Display a quad in the world */
		glColor3f(1,1,1);
		glBegin(GL_QUADS);
		glTexCoord3fv(a0); glVertex3fv(a0);
		glTexCoord3fv(a1); glVertex3fv(a1);
		glTexCoord3fv(a2); glVertex3fv(a2);
		glTexCoord3fv(a3); glVertex3fv(a3);
		glEnd();
#else

		/* cube
		 *
		 *      4----------7
		 *      |\         |\
		 *      | \        | \
		 *      |  5----------6
		 *      |  |       |  |
		 *      0--|-------3  |
		 *       \ |        \ |
		 *        \|         \|
		 *         1----------2
		 *
		 * edges are: (01) (12) (23) (30)
		 *            (45) (56) (67) (74)
		 *            (04) (15) (26) (37)
		 *
		 */

		/*
		 *          ^
		 *      (n) |   \ c[e[i][0]]
		 *          |    \
		 *         +------\----------+
		 *       /  |      o       /
		 *     /    O       .    /
		 *   +-----------------+
		 *                    . c[e[i][1]]
		 *
		 * Edge intersecting the plane
		 * Vector (v) start at the plane origin and ends at a cube corner
		 */
		

		/* Where the cube corners are */
		float c[8][3] = { // cuber corners
			{-0.5f, -0.5f, -0.5f},
			{-0.5f, +0.5f, -0.5f},
			{+0.5f, +0.5f, -0.5f},
			{+0.5f, -0.5f, -0.5f},
			{-0.5f, -0.5f, +0.5f},
			{-0.5f, +0.5f, +0.5f},
			{+0.5f, +0.5f, +0.5f},
			{+0.5f, -0.5f, +0.5f},
		};

		/* Edges of the cube */
		int e[12][2] = { // edges
			{0,1}, {1,2}, {2,3}, {3,0},
			{4,5}, {5,6}, {6,7}, {7,4},
			{0,4}, {1,5}, {2,6}, {3,7}
		};

		/* Vector (v): from plane origin to cube corners */
		float v[8][3] = { // cuber corners relative to plane origin
			{-0.5f-m[12], -0.5f-m[13], -0.5f-m[14]},
			{-0.5f-m[12], +0.5f-m[13], -0.5f-m[14]},
			{+0.5f-m[12], +0.5f-m[13], -0.5f-m[14]},
			{+0.5f-m[12], -0.5f-m[13], -0.5f-m[14]},
			{-0.5f-m[12], -0.5f-m[13], +0.5f-m[14]},
			{-0.5f-m[12], +0.5f-m[13], +0.5f-m[14]},
			{+0.5f-m[12], +0.5f-m[13], +0.5f-m[14]},
			{+0.5f-m[12], -0.5f-m[13], +0.5f-m[14]},
		};

		/* Signed distance from cube corners to the plane
		 * it is the dot-product of plane up vector (n) with (v) */
		float nv[8] = {
			m[8]*v[0][0]+m[9]*v[0][1]+m[10]*v[0][2],
			m[8]*v[1][0]+m[9]*v[1][1]+m[10]*v[1][2],
			m[8]*v[2][0]+m[9]*v[2][1]+m[10]*v[2][2],
			m[8]*v[3][0]+m[9]*v[3][1]+m[10]*v[3][2],
			m[8]*v[4][0]+m[9]*v[4][1]+m[10]*v[4][2],
			m[8]*v[5][0]+m[9]*v[5][1]+m[10]*v[5][2],
			m[8]*v[6][0]+m[9]*v[6][1]+m[10]*v[6][2],
			m[8]*v[7][0]+m[9]*v[7][1]+m[10]*v[7][2],
		};

		/* If edge ends have a different sign, the edge crosses the plane */
		int cross[12]; // is edge[i] crossing plane

		for(unsigned int i=0; i<12; ++i)
		{
			if(nv[e[i][0]]*nv[e[i][1]]<0)
				cross[i] = 1;
			else
				cross[i] = 0;
		}

		/* Compute the intersection points on each crossing edge */
		std::vector<float> points;
		for(unsigned int i=0; i<12; ++i)
		{
			if(cross[i])
			{
				float u[3] = {
					c[e[i][1]][0]-c[e[i][0]][0],
					c[e[i][1]][1]-c[e[i][0]][1],
					c[e[i][1]][2]-c[e[i][0]][2]
				};

				float nu = m[8]*u[0]+m[9]*u[1]+m[10]*u[2];

				float t = - nv[e[i][0]] / nu;

				float inters[3] = {
					c[e[i][0]][0]+t*u[0],
					c[e[i][0]][1]+t*u[1],
					c[e[i][0]][2]+t*u[2]
				};

				points.push_back(inters[0]);
				points.push_back(inters[1]);
				points.push_back(inters[2]);
			}
		}

		/* If we have more than two points, we can draw a polygon */
		if(points.size()>6) // if more than two points
		{
			/* Sort these points so we can correctly draw a polygon */
			std::multimap<float,int> map;
			float u[3] = { points[3]-points[0], points[4]-points[1], points[5]-points[2] };
			float lul = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
			for(unsigned int i=2; i<points.size()/3; ++i)
			{
				float v[3] = { points[i*3+0]-points[0],points[i*3+1]-points[1],points[i*3+2]-points[2] };
				float lvl = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
	
				float cos_angle = (v[0]*u[0]+v[1]*u[1]+v[2]*u[2])/(lul*lvl);
				map.insert(std::pair<float,int>(cos_angle,i));
			}
	
			/* Draw the cube cross-section with a texture on it */
			glColor3f(1,1,1);
			glBegin(GL_TRIANGLE_FAN);
			glTexCoord3f(points[0],points[1],points[2]);
			glVertex3f  (points[0],points[1],points[2]);
			glTexCoord3f(points[3],points[4],points[5]);
			glVertex3f  (points[3],points[4],points[5]);
			for(std::multimap<float,int>::reverse_iterator it = map.rbegin(); it != map.rend(); ++it)
			{
				glTexCoord3f(points[it->second*3+0],points[it->second*3+1],points[it->second*3+2]);
				glVertex3f  (points[it->second*3+0],points[it->second*3+1],points[it->second*3+2]);
			}
			glEnd();
		} // if more than 2 points
#endif
	}


	void Volume4D::alignWithScreen() const
	{
		/* get the modelview matrix */
		float modelview[16]; glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset, first, we align the view with the screen

		/* get the current transformation */
		glPushMatrix();
		glMultMatrixf(m_mpr_matrix); // how the plane is positioned
		glMultMatrixf(modelview); // scale it
		float m[16]; glGetFloatv(GL_MODELVIEW_MATRIX, m); // how the plane is with the scaling factors
		glPopMatrix();

		/* get the scaling matrix */
		float sx = sqrt(m[0]*m[0] + m[1]*m[1] + m[2] *m[2]);
		float sy = sqrt(m[4]*m[4] + m[5]*m[5] + m[6] *m[6]);
		float sz = sqrt(m[8]*m[8] + m[9]*m[9] + m[10]*m[10]);

		/* get the rotation matrix */
		float rot[16] = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
		rot[0] = m[0]/sx; rot[1] = m[1]/sy; rot[2] = m[2] /sz;
		rot[4] = m[4]/sx; rot[5] = m[5]/sy; rot[6] = m[6] /sz;
		rot[8] = m[8]/sx; rot[9] = m[9]/sy; rot[10]= m[10]/sz;
		normalize(rot);

		/* invert plane matrix
		 * so we can make the plane appear parallel to the screen if required */
		float inv_m[16];
		for(unsigned int i=0; i<16; ++i) inv_m[i] = rot[i];
		transp(inv_m);
		inv_m[3]  = inv_m[7]  = inv_m[11] = 0; // should be zero anyway
		inv_m[12] = inv_m[13] = inv_m[14] = 0; // remove translation for now

		/* invert the model view matrix 
		 * M^-1 = (RS)^-1 = S^-1 R^-1 = R/(sx,sy,sz)^T */
		inv_m[0] = inv_m[0] / sx; inv_m[1] = inv_m[1] / sy; inv_m[2]  = inv_m[2]  / sz;
		inv_m[4] = inv_m[4] / sx; inv_m[5] = inv_m[5] / sy; inv_m[6]  = inv_m[6]  / sz;
		inv_m[8] = inv_m[8] / sx; inv_m[9] = inv_m[9] / sy; inv_m[10] = inv_m[10] / sz;

		/* revert any translation, so the plane crosses the origin
		 * convert the plane translation into the screen coordinate (use the inverse of the rotation part)
		 * multiply translation by inv_rot (rot^T) */
		inv_m[12] = -( rot[0]*m[12]*sx + rot[1]*m[13]*sy + rot[2] *m[14]*sz ) / sx;
		inv_m[13] = -( rot[4]*m[12]*sx + rot[5]*m[13]*sy + rot[6] *m[14]*sz ) / sy;
		inv_m[14] = -( rot[8]*m[12]*sx + rot[9]*m[13]*sy + rot[10]*m[14]*sz ) / sz;

		/* change modelview matrix so all further object (eg. plane) will be parallel to the screen */
		glMatrixMode(GL_MODELVIEW);
		glMultMatrixf(inv_m); // rotate screen, aligning it with the screen
		
		/* now the correction (align with the screen) has been done,
		 * use the modelview as previously expected */
		glMultMatrixf(modelview);
	}


	void Volume4D::updateRamp(float min, float max)
	{
		/* check if the display has been initialized */
		if(!m_gpu_program) throw LOCATE(Error("updating lut before setting data"));

		/* use the GPU program */
		glUseProgram(m_gpu_program);

		/* internal lut is normalized (maximum texture size)
		   lut[0] corresponds to the lowest data value and
		   lut[size-1] corresponds to the highest data value */
		GLint size; // typically 4096
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &size); // query what the graphic card max texture size

		/* set lookup table */
		if(!m_internal_lut) m_internal_lut = new float [size*4];

		/* find where the min and max value should be in the internal lut */
		if(m_intensity_range[1] == m_intensity_range[0])
			min = m_intensity_range[0],
			max = m_intensity_range[1];
		else
			min = (min-m_intensity_range[0]) * (size-1) / (m_intensity_range[1]-m_intensity_range[0]),
			max = (max-m_intensity_range[0]) * (size-1) / (m_intensity_range[1]-m_intensity_range[0]);

		int low  = (min<0)    ?  0   : ((min>size) ? size : (int)min);
		int high = (max>size) ? size : ((max<0)    ? 0    : (int)max);

		/* fill in lut flat parts */
		for(int i=0; i<low; ++i)
		{
			m_internal_lut[i*4+0] = 0,
			m_internal_lut[i*4+1] = 0,
			m_internal_lut[i*4+2] = 0,
			m_internal_lut[i*4+3] = 0;
		}
		for(int i=high; i<size; ++i)
		{
			m_internal_lut[i*4+0] = 1,
			m_internal_lut[i*4+1] = 1,
			m_internal_lut[i*4+2] = 1,
			m_internal_lut[i*4+3] = 1;
		}

		/* fill ramp */
		for(int i=low; i<high; ++i)
		{
			m_internal_lut[i*4+0] = (i-min)/(max-min),
			//m_internal_lut[i*4+1] = (i-min)/(max-min)*(i-min)/(max-min)*(i-min)/(max-min), // G and
			//m_internal_lut[i*4+2] = (i-min)/(max-min)*(i-min)/(max-min)*(i-min)/(max-min), // B increases slower than R -> gives a redish color to the volume
			m_internal_lut[i*4+1] = (i-min)/(max-min),
			m_internal_lut[i*4+2] = (i-min)/(max-min),
			m_internal_lut[i*4+3] = (i-min)/(max-min); // from transparent to opage
		}

		/* Create 1D texture for lookup table */
		if(!glIsTexture(m_lut_texture)) glGenTextures(1, &m_lut_texture);
		glBindTexture(GL_TEXTURE_2D, m_lut_texture); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
					 0,
					 GL_RGBA8,
					 size, // lut size
					 1,
					 0,
					 GL_RGBA, // color lookup table
					 GL_FLOAT,
					 m_internal_lut);
		glCheckError();

		/* NB: we do not delete the lut, we keep it there
			   so when updating it the next time, no time will be lost in memory allocation/deallocation */
		//delete [] m_internal_lut; m_internal_lut = 0;

		/* stop using the GPU program until requested */
		glUseProgram(0);
	}


	void Volume4D::updateLut(const float* lut, unsigned int lutSize)
	{
		/* check inputs */
		if(!lut || !lutSize) throw LOCATE(Error("wrong inputs"));

		/* check if the display has been initialized */
		if(!m_gpu_program) throw LOCATE(Error("updating lut before setting data"));

		/* use the GPU program */
		glUseProgram(m_gpu_program);

		/* internal lut is normalized (maximum texture size)
		   lut[0] is the lowest volume value and
		   lut[size-1] is the highest voluem value */
		GLint size; // typically 4096
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &size); // query what the graphic card max texture size

		/* set lookup table */
		if(!m_internal_lut) m_internal_lut = new float [size*4];

		/* copy lut */
		float scale = (float)(m_intensity_range[1]-m_intensity_range[0])/size;
		for(int i=0; i<size; ++i)
		{
			/* find where i should be in the given lut
			   need to convert i which ranges from 0..size, but corresponds
			   to values ranging in m_intensity_range[2] */
			unsigned int intensity = (unsigned int)(i*scale + m_intensity_range[0]);

			if(intensity < lutSize)
			{
				m_internal_lut[i*4+0] = lut[intensity*4+0];
				m_internal_lut[i*4+1] = lut[intensity*4+1];
				m_internal_lut[i*4+2] = lut[intensity*4+2];
				m_internal_lut[i*4+3] = lut[intensity*4+3];
			}
			else
			{
				/* assume no intensity if lut is not defined */
				m_internal_lut[i*4+0] = 1;
				m_internal_lut[i*4+1] = 1;
				m_internal_lut[i*4+2] = 1;
				m_internal_lut[i*4+3] = 1;
			}
		}

		/* Create 1D texture for lookup table */
		if(!glIsTexture(m_lut_texture)) glGenTextures(1, &m_lut_texture);
		glBindTexture(GL_TEXTURE_2D, m_lut_texture); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
					 0,
					 GL_RGBA8,
					 size, // lut size
					 1,
					 0,
					 GL_RGBA, // color lookup table
					 GL_FLOAT,
					 m_internal_lut);
		glCheckError();

		/* NB: instead of freeing this unused memory, keep it there
			   so when updating it the next time, no time will be lost in memory allocation/deallocation */
		//delete [] m_internal_lut; m_internal_lut = 0;

		/* stop using the GPU program until requested */
		glUseProgram(0);
	}


	void Volume4D::setQuality(float quality)
	{
		if(quality<=0) throw LOCATE(Error("using an invalid quality: %f", quality));

		m_quality = quality;
	}


	void Volume4D::setMaxTextureSize(unsigned int w, unsigned int h, unsigned int d)
	{
		m_max_texture_size[0] = w;
		m_max_texture_size[1] = h;
		m_max_texture_size[2] = d;
	}


	void Volume4D::getIntensityRange(float &min, float &max) const
	{
		min = m_intensity_range[0];
		max = m_intensity_range[1];
	}


	void Volume4D::initShader()
	{
		/* Fragment shader source code
		   It is a simple lookup table that returns an RGBA value
		   for a given voxel intensity */
		char source[] = "uniform sampler3D tex;								\n\
						 uniform sampler2D lut;								\n\
						 void main()										\n\
						 {													\n\
							 vec4 c = texture3D(tex, vec3(gl_TexCoord[0]));	\n\
							 vec4 i = texture2D(lut, c.xx);					\n\
							 gl_FragColor = i; 								\n\
						 }\n\0";

		/* Create and compile shader */
		const char* source_pointer = source;
		m_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(m_fragment_shader, 1, &source_pointer, NULL);
		glCompileShader(m_fragment_shader);

		/* display any error if required */
		char infolog[1024];
		GLsizei infologlength = 0;
		glGetShaderInfoLog(m_fragment_shader, 1024, &infologlength, infolog);
		CHECK(infolog,infologlength==0);
		
		/* Attach shader to program */
		m_gpu_program = glCreateProgram();
		glAttachShader(m_gpu_program, m_fragment_shader);
		glLinkProgram(m_gpu_program);

		/* Use the program */
		glUseProgram(m_gpu_program);

		/* Bind shader texture variable */
		int texLoc = glGetUniformLocation(m_gpu_program, "tex");
		CHECK("Error in 3D texture location",texLoc!=-1);
		glUniform1i(texLoc, 0); /* bind volume texture to GL_TEXTURE0 */

		/* Bind shader lut variable */
		int lutLoc = glGetUniformLocation(m_gpu_program, "lut");
		CHECK("Error in lookup table location",lutLoc!=-1);
		glUniform1i(lutLoc, 1); /* bind lookup table texture to GL_TEXTURE1 */

		/* Stop using the program until next use */
		glUseProgram(0);
	}


	void Volume4D::getTextureSize(const unsigned int data_size[],
						        unsigned int texture_size[],
						        const unsigned int max_texture_size[])
	{
		/* what is the texture size (compatible with opengl) */
		for(unsigned int m=0; m<3; ++m)
		{
			/* shift until it is greater than the volume size */
			texture_size[m] = 1;
			while(texture_size[m] < data_size[m] && texture_size[m] < max_texture_size[m])
				texture_size[m] = texture_size[m]<<1;
		}
	}


	int Volume4D::getNbRenderingPlanes() const
	{
		/* NB: need to call it in Display (or when the modelview/projection matrices are set) */

		/* Get all visualization matrices */
		GLdouble modelview[16];
		GLdouble projection[16];
		GLint    viewport[4];

		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);

		/* project some point to the screen
		 *
		 *    u2(001)  +-------+
		 *             |\      |\
		 *             | \     | \
		 *             |  +-------+ u1(1111)
		 *    u0(000)  +--|----+  |
		 *              \ |     \ |
		 *               \|      \|
		 *                +-------+ u3(110)
		 *
		 * measure the max distance between u0-u1 and u2-u3
		 * the max will evaluate the number of pixel of the volume
		 */
		double u0_x, u0_y, u0_z;
		double u1_x, u1_y, u1_z;
		double u2_x, u2_y, u2_z;
		double u3_x, u3_y, u3_z;

		gluProject(-0.5,-0.5,-0.5, modelview, projection, viewport, &u0_x, &u0_y, &u0_z);
		gluProject(+0.5,+0.5,+0.5, modelview, projection, viewport, &u1_x, &u1_y, &u1_z);
		gluProject(-0.5,-0.5,+0.5, modelview, projection, viewport, &u2_x, &u2_y, &u2_z);
		gluProject(+0.5,+0.5,-0.5, modelview, projection, viewport, &u3_x, &u3_y, &u3_z);

#if 0
		/* what is the farthest rendering plane */
		double min = u0_z;
		if(u1_z < min ) min = u1_z;
		if(u2_z < min ) min = u2_z;
		if(u3_z < min ) min = u3_z;

		/* what is the closest rendering plane */
		double max = u0_z;
		if(u1_z > max ) max = u1_z;
		if(u2_z > max ) max = u2_z;
		if(u3_z > max ) max = u3_z;
		//if(max > 0) max = 0;

		/* how many rendering planes are required */
		double n = max - min;
		printf("%f %f\n", min, max);
		return static_cast<unsigned int>(n);

#else

		/* what is the max distance (rough evaluation, avoid more computation) */
		float u01_x = (float)fabs(u1_x-u0_x);
		float u01_y = (float)fabs(u1_y-u0_y);
		float u23_x = (float)fabs(u3_x-u2_x);
		float u23_y = (float)fabs(u3_y-u2_y);

		float max = u01_x;
		if(u01_y > max ) max = u01_y;
		if(u23_x > max ) max = u23_x;
		if(u23_y > max ) max = u23_y;

		/* do not use more than a certain number of planes */
		if(max > viewport[2]) max = (float)viewport[2];
		if(max > viewport[3]) max = (float)viewport[3];

		/* use maximum 200 planes */
		if(max > 200) max = 200;

		return static_cast<unsigned int>(max);
#endif
	}


	void Volume4D::transf(const float p[], const float m[], float pp[])
	{
		/* pp is the transformed point with matrix m */
		pp[0] = p[0]*m[0] + p[1]*m[4] + p[2]*m[8] + m[12];
		pp[1] = p[0]*m[1] + p[1]*m[5] + p[2]*m[9] + m[13];
		pp[2] = p[0]*m[2] + p[1]*m[6] + p[2]*m[10] + m[14];
	}


	void Volume4D::transp(float m[])
	{
		/* transpose matrix */
		for(unsigned int j=0; j<4; ++j)
			for(unsigned int i=0; i<j; ++i)
		{
			float a = m[j*4+i];
			m[j*4+i] = m[i*4+j];
			m[i*4+j] = a;
		}

		/* keep translation where it should be */
		m[12] = -m[3];
		m[13] = -m[7];
		m[14] = -m[11];
		m[3] = m[7] = m[11] = 0;
	}


	void Volume4D::normalize(float m[])
	{
		/* normalize each row of the rotation matrix */
		for(unsigned int j=0; j<3; ++j)
		{
			float d = sqrt(m[j*4+0]*m[j*4+0] + m[j*4+1]*m[j*4+1] + m[j*4+2]*m[j*4+2]);
			m[j*4+0] = m[j*4+0] / d;
			m[j*4+1] = m[j*4+1] / d;
			m[j*4+2] = m[j*4+2] / d;
		}
	}


	// Define OpenGL types
	template <> GLenum Volume4D::OpenGLArgs<float> ::type = GL_FLOAT;
	template <> GLenum Volume4D::OpenGLArgs<double>::type = GL_FLOAT;
	template <> GLenum Volume4D::OpenGLArgs<char>  ::type = GL_BYTE;
	template <> GLenum Volume4D::OpenGLArgs<short> ::type = GL_SHORT;
	template <> GLenum Volume4D::OpenGLArgs<unsigned char> ::type = GL_UNSIGNED_BYTE;
	template <> GLenum Volume4D::OpenGLArgs<unsigned short>::type = GL_UNSIGNED_SHORT;


	// Define min and max intensity values for standard types (floating type are assumed to range from 0.0f to 1.0f)
	template <>          float  Volume4D::OpenGLArgs<float> ::min = 0;
	template <>          float  Volume4D::OpenGLArgs<float> ::max = 1;
	template <>          double Volume4D::OpenGLArgs<double>::min = 0;
	template <>          double Volume4D::OpenGLArgs<double>::max = 1;
	template <>          char   Volume4D::OpenGLArgs<char>  ::min = -128;
	template <>          char   Volume4D::OpenGLArgs<char>  ::max = 127;
	template <>          short  Volume4D::OpenGLArgs<short> ::min = -32768;
	template <>          short  Volume4D::OpenGLArgs<short> ::max = 32767;
	template <> unsigned char   Volume4D::OpenGLArgs<unsigned char> ::min = 0;
	template <> unsigned char   Volume4D::OpenGLArgs<unsigned char> ::max = 255;
	template <> unsigned short  Volume4D::OpenGLArgs<unsigned short>::min = 0;
	template <> unsigned short  Volume4D::OpenGLArgs<unsigned short>::max = 65535;


} // namespace
