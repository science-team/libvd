/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	iobmp.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <io/iobmp.h>

#include <cstdio>
#include <cstring>
#include <error.h>


/// swap bytes if cpu is big endian (eg. apple ppc), do nothing if little endian (eg. intel pc)
#if APPLE
#define INTCPU(i) i = ((i&0xff000000)>>24) | (((i&0x00ff0000)>>16)<<8) | (((i&0x0000ff00)>>8)<<16) | ((i&0x000000ff)<<24);
#else
#define INTCPU(i) ;
#endif


namespace IO
{	
	void readBMP(const char* filename, color<unsigned char>* &image, unsigned int &width, unsigned int &height)
	{
		BMP::read(filename, image, width, height);
	}
		
	
	void writeBMP(const char* filename, const color<unsigned char>* image, unsigned int width, unsigned int height)
	{
		BMP::write(filename, image, width, height);
	}
	

	// 2D Color read
	void BMP::read(const char* filename, color<unsigned char>* &image, unsigned int &width, unsigned int &height)
	{
		FILE *fd;
		unsigned int w, h;

		/* open BMP file */
		fd = fopen(filename, "rb");
		if(!fd) throw LOCATE(Error("open"));

		/* read header */
		unsigned short signature = 0;
		fread(&signature, 2, 1, fd);
	#if APPLE
		if(signature != 0x424d) throw LOCATE(Error("format"));
	#else
		if(signature != 0x4d42) throw LOCATE(Error("format"));
	#endif
		fseek(fd, 18, SEEK_SET);
		fread(&w, 4, 1, fd);		INTCPU(w);
		fread(&h, 4, 1, fd);		INTCPU(h);
		fseek(fd, 54, SEEK_SET);

		image  = new color<unsigned char> [w*h];
		width  = w;
		height = h;

		// padding
		unsigned bytesPerRow = width*3;
		bytesPerRow = (bytesPerRow & 0x3) ? ((bytesPerRow >> 2) + 1) << 2 : bytesPerRow;
		unsigned padding = bytesPerRow - width*3;

		/* read bitmap */
		for(unsigned int j=0; j<h; ++j)
		{
			unsigned int n = (unsigned int)fread((char*)&image[j*width], 3, width, fd);
			if(n != width) throw LOCATE(Error("EOF reached while reading"));

			if(padding != 0)
			{
				int zero;
				fread((char*)&zero, 1, padding, fd);
			}
		}

		/* close BMP file */
		fclose(fd);
	}


	// 2D color write
	void BMP::write(const char* filename, const color<unsigned char>* image, unsigned int width, unsigned int height)
	{
		FILE *fd;
		int header = 0;
		int zero = 0;

		/* open/create BMP file */
		fd = fopen(filename, "wb");
		if(!fd) throw LOCATE(Error("open"));

		/* Write BMP header */
		header = 0x84364d42;     INTCPU(header);
		fwrite(&header, 4, 1, fd);

		header = 0x3;            INTCPU(header);
		fwrite(&header, 4, 1, fd);
		fwrite(&zero, 2, 1, fd);
		
		header = 0x36;           INTCPU(header);
		fwrite(&header, 4, 1, fd);

		header = 0x28;           INTCPU(header);
		fwrite(&header, 4, 1, fd);

		header = width;          INTCPU(header);
		fwrite(&header, 4, 1, fd);	/* width */

		header = height;         INTCPU(header);
		fwrite(&header, 4, 1, fd);	/* height */

		header = 0x180001;       INTCPU(header);
		fwrite(&header, 4, 1, fd);
		fwrite(&zero, 4, 1, fd);

		header = width*height*3; INTCPU(header);
		fwrite(&header, 4, 1, fd);	/* file size */

		header = 0x0b000012;     INTCPU(header);
		fwrite(&header, 4, 1, fd);

		header = 0x0b;           INTCPU(header);
		fwrite(&header, 4, 1, fd);
		fwrite(&zero, 4, 1, fd);
		fwrite(&zero, 4, 1, fd);
		
		// padding
		unsigned bytesPerRow = width*3;
		bytesPerRow = (bytesPerRow & 0x3) ? ((bytesPerRow >> 2) + 1) << 2 : bytesPerRow;
		unsigned padding = bytesPerRow - width*3;

		/* Write DATA */
		for(unsigned int j=0; j<height; ++j)
		{
			fwrite((char*)&image[j*width], 3, width, fd);
			
			if(padding != 0)
			{
				int zero = 0;
				fwrite((char*)&zero, 1, padding, fd);
			}
		}

		/* close BMP file */
		fclose(fd);
	}


}; // namespace


