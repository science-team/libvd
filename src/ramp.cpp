/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ramp.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/transferfunction/ramp.h>
#include <cmath>

namespace Tools { namespace TransferFunction
{
	Ramp::Ramp()
	:	m_low(0),
		m_high(0),
		m_opacity(1),
		m_left(0),
		m_right(0),
		m_low_u(0),
		m_high_u(0),
		m_aspectratio(1),
		m_feature(NONE)
	{
		m_low_color[0] = m_low_color[1] = m_low_color[2] = 0;
		m_middle_color[0] = m_middle_color[1] = m_middle_color[2] = 0.5;
		m_high_color[0] = m_high_color[1] = m_high_color[2] = 1;
	}


	Ramp::Ramp(float low, float high, float opacity)
	:	m_low(low),
		m_high(high),
		m_opacity(opacity),
		m_left(0),
		m_right(0),
		m_low_u(0),
		m_high_u(0),
		m_aspectratio(1),
		m_feature(NONE)
	{
		m_low_color[0] = m_low_color[1] = m_low_color[2] = 0;
		m_middle_color[0] = m_middle_color[1] = m_middle_color[2] = 0.5;
		m_high_color[0] = m_high_color[1] = m_high_color[2] = 1;
	}


	Ramp::~Ramp()
	{}


	void Ramp::update(float low, float high, float opacity)
	{
		/* update ramp */
		m_low = low;
		m_high = high;
		m_opacity = opacity;

		/* update ramp position for its visualization */
		m_low_u  = (m_low-m_left)  / (m_right-m_left);
		m_high_u = (m_high-m_left) / (m_right-m_left);
	}


	void Ramp::clamp(float left, float right)
	{
		/* update visualization limits */
		m_left  = left;
		m_right = right;

		/* update ramp position for its visualization */
		m_low_u  = (m_low-m_left)  / (m_right-m_left);
		m_high_u = (m_high-m_left) / (m_right-m_left);
	}


	void Ramp::display() const
	{
		/* draw ramp */
		glColor3f(1,0,0);

		glBegin(GL_LINE_STRIP);

		/* first half of the ramp */
		if(m_high_u>=0)
		{
			if(m_low_u>=0)
			{
				/* low horizontal line */
				glVertex3f(-0.5f,-0.5f,0);
				if(m_low_u<=1.0f) glVertex3f(m_low_u-0.5f,-0.5f,0);
				else           glVertex3f(0.5f,-0.5f,0);
			}
			else
			{
				/* find intersection of ramp with left border */
				float intersec = - m_low_u / (m_high_u-m_low_u);
				if(intersec<1.0f) glVertex3f(-0.5f,intersec*m_opacity-0.5f,0);
			}
		}

		/* second half of the ramp */
		if(m_low_u<=1)
		{
			if(m_high_u<=1)
			{
				/* high horizontal line */
				if(m_high_u>=0.0f) glVertex3f(m_high_u-0.5f,m_opacity-0.5f,0);
				else            glVertex3f(-0.5f,m_opacity-0.5f,0);
				glVertex3f(0.5f,m_opacity-0.5f,0);
			}
			else
			{
				/* find intersection of ramp with right border */
				float intersec = (1-m_low_u) / (m_high_u-m_low_u);
				if(intersec<1.0f) glVertex3f(0.5f,intersec*m_opacity-0.5f,0);
			}
		}
		glEnd();

		float scale = 0.05f; // square size

		/* lower ramp square */
		if(m_low_u>=0.0f && m_low_u<=1.0f)
		{
			glColor3f(m_low_color[0], m_low_color[1], m_low_color[2]);

			glPushMatrix();
			glTranslatef(m_low_u-0.5f,-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* higher ramp square */
		if(m_high_u>=0 && m_high_u<=1)
		{
			glColor3f(m_high_color[0], m_high_color[1], m_high_color[2]);

			glPushMatrix();
			glTranslatef(m_high_u-0.5f,m_opacity-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* center ramp square */
		if((m_low_u+m_high_u)/2>=0.0f && (m_low_u+m_high_u)/2<=1.0f)
		{
			glColor3f(m_middle_color[0], m_middle_color[1], m_middle_color[2]);

			glPushMatrix();
			glTranslatef((m_low_u+m_high_u)/2-0.5f,m_opacity/2-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}
	}


	void Ramp::reshape(int w, int h)
	{
		m_aspectratio = (float)w/h;
	}


	bool Ramp::mouse(float x, float y)
	{
		float tolerance = 0.1f;

		// NB: mouse coordinates are normalized (0..1)

		/* assume no feature is selected */
		m_feature = NONE;

		if(x>=0 && x<=1 && y>=0 && y<=1)
		{
			/* lower ramp corner */
			if(fabs(x-m_low_u) < 0.1 && fabs(y-0) < tolerance)
				m_feature = LOW;

			/* middle of the ramp */
			else if(fabs(x-(m_low_u+m_high_u)/2) < 0.1 && fabs(y-m_opacity/2) < tolerance)
				m_feature = MIDDLE;

			/* higher ramp corner */
			else if(fabs(x-m_high_u) < 0.1 && fabs(y-m_opacity) < tolerance)
				m_feature = HIGH;
		}

		/* if a feature is selected, return true */
		if(m_feature != NONE) return true;
		else                  return false;
	}


	bool Ramp::motion(float x, float y)
	{
		// NB: mouse coordinates are normalized (0..1)

		bool eventCaught = false;

		/* what intensity is x */
		float i = x*(m_right-m_left) + m_left;

		/* change feature with new position */
		switch(m_feature)
		{
		case LOW: // move lower ramp corner
			{
				if(i>=m_high) i = m_high-1; // avoid inverse ramp (not handled in this code)
				update(i,m_high,m_opacity);
				eventCaught = true;
				break;
			}
		case HIGH: // move higher ramp corner
			{
				if(i<=m_low) i = m_low+1; // avoid inverse ramp (not handled in this code)
				if(y>=0 && y<=1) m_opacity = y; // opacity is between 0 and 1
				update(m_low,i,m_opacity);
				eventCaught = true;
				break;
			}
		case MIDDLE: // translate both corners
			{
				// i is the new ramp center
				float hwindow = (m_high-m_low)/2;
				update(i-hwindow, i+hwindow, m_opacity);
				eventCaught = true;
				break;
			}
		default: break;
		}

		return eventCaught;
	}


	void Ramp::get(float i, float &r, float &g, float &b, float &a) const
	{
		if(i < m_low)
		{
			r = m_low_color[0],
			g = m_low_color[1],
			b = m_low_color[2],
			a = 0;
		}
		else if(i > m_high)
		{
			r = m_high_color[0],
			g = m_high_color[1],
			b = m_high_color[2],
			a = m_opacity;
		}
		else if(i>=m_low && i<=(m_low+m_high)/2)
		{
			/* transition between 2 colors */
			float v = 2 * (i-m_low) / (m_high-m_low);

			/* change is slower for the dominent color */
			r = (1- v)*m_low_color[0] + ( v)*m_middle_color[0];
			g = (1- v)*m_low_color[1] + ( v)*m_middle_color[1];
			b = (1- v)*m_low_color[2] + ( v)*m_middle_color[2];

			/* opacity is linear */
			a = v/2 * m_opacity;
		}
		else if(i>(m_low+m_high)/2 && i<=m_high)
		{
			/* transition between 2 colors */
			float v = 2 * ((i-m_low) / (m_high-m_low) - 0.5f);

			/* change is slower for the dominent color */
			r = (1- v)*m_middle_color[0] + ( v)*m_high_color[0];
			g = (1- v)*m_middle_color[1] + ( v)*m_high_color[1];
			b = (1- v)*m_middle_color[2] + ( v)*m_high_color[2];

			/* opacity is linear */
			a = (v/2 + 0.5f) * m_opacity;
		}
	}


	void Ramp::setColor(float r, float g, float b)
	{
		switch(m_feature)
		{
		case LOW:
			m_low_color[0] = r,
			m_low_color[1] = g,
			m_low_color[2] = b;
			break;
		case MIDDLE:
			m_middle_color[0] = r,
			m_middle_color[1] = g,
			m_middle_color[2] = b;
			break;
		case HIGH:
			m_high_color[0] = r,
			m_high_color[1] = g,
			m_high_color[2] = b;
			break;
		default: break;
		}
	}

}} // namespace
