/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	editor.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/transferfunction/editor.h>

namespace Tools { namespace TransferFunction
{
	Editor::Editor()
	:	m_min(0),
		m_max(0),
		m_left(0),
		m_right(0),
		m_posx(0),
		m_posy(0),
		m_width(0),
		m_height(0),
		m_win_width(0),
		m_win_height(0),
		m_lutchanged(false),
		m_contrast(1.0),
		m_brightness(0.0),
		m_colorMode(false),
		m_colorSize(0),
		m_hide(false)
	{}


	Editor::~Editor()
	{
		for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
		{
			(*it)->destroy();
			delete (*it);
		}
	}


	void Editor::clamp(float left, float right)
	{
		m_histogram.clamp(left, right);
		for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
			(*it)->clamp(left, right);
	}


	void Editor::place(int x, int y, int w, int h, int ww, int wh) 
	{
		m_posx = x,
		m_posy = y,
		m_width = w,
		m_height = h,
		m_win_width = ww,
		m_win_height = wh;
		m_colorSize = m_height*0.8f; // color selector is one heighth of the histogram height

		// update object shapes
		for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
			(*it)->reshape(w, h);
	}


	void Editor::clear()
	{
		m_manips.clear();
	}


	bool Editor::mouse(int x, int y)
	{
		// if hidden, do nothing with the mouse
		if(m_hide) return false;

		// check if we are in the color mode, leave it if necessary
		if(m_colorMode)
		{
			// assume lut does not change
			m_lutchanged = false;

			// is mouse inside the color selector
			float l0 = m_posx + (m_height-m_colorSize)/2; // low corner x
			float l1 = m_posy + (m_height-m_colorSize)/2; // low corner y
			float h0 = m_posx + (m_height-m_colorSize)/2 + m_colorSize; // high corner x
			float h1 = m_posy + (m_height-m_colorSize)/2 + m_colorSize; // high corner y

			if(x>=l0 && x<=h1 && y>=l1 && y<=h1) // inside selector
			{
				// what is the selected color
				float xx = (x-l0)/(h0-l0);
				float yy = (y-l1)/(h1-l1);
				float r=0,g=0,b=0;
				m_color.getRGB(xx,yy, r,g,b);
				m_color.mark(r,g,b);
		
				// propagate color to transfer function manipulators
				for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
					(*it)->setColor(r,g,b);

				// lut has changed
				m_lutchanged = true;
		
				// stop mouse handling here
				return true;
			}
			else
			{
				// mouse is outside selector, leave color mode
				m_colorMode = false;
			}
		}

		// normalize mouse position with transfer function size
		float xx = (float)(x-m_posx) / m_width;
		float yy = (float)(y-m_posy) / m_height;

		// give mouse event to all objects until one catch it
		for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
			if((*it)->mouse(xx,yy)) return true; // if an object caught event stop event propagation

		// if none has caught it, play with histogram
		return m_histogram.mouse(xx,yy);
	}


	bool Editor::motion(int x, int y)
	{
		// if hidden, do nothing with the mouse
		if(m_hide) return false;

		// check if we are in the color mode, leave it if necessary
		if(m_colorMode)
		{
			// assume lut does not change
			m_lutchanged = false;

			// is mouse inside the color selector
			float l0 = m_posx + (m_height-m_colorSize)/2; // low corner x
			float l1 = m_posy + (m_height-m_colorSize)/2; // low corner y
			float h0 = l0 + m_colorSize; // high corner x
			float h1 = l1 + m_colorSize; // high corner y

			if(x>=l0 && x<=h0 && y>=l1 && y<=h1) // inside selector
			{
				// what is the selected color
				float xx = (x-l0)/(h0-l0);
				float yy = (y-l1)/(h1-l1);
				float r=0,g=0,b=0;
				m_color.getRGB(xx,yy, r,g,b);
				m_color.mark(r,g,b);
		
				// propagate color to transfer function manipulators
				for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
					(*it)->setColor(r,g,b);

				// lut has changed
				m_lutchanged = true;
			}
			
			// stop mouse handling here
			return true;
		}

		// normalize mouse position with transfer function size
		float xx = (float)(x-m_posx) / m_width;
		float yy = (float)(y-m_posy) / m_height;

		// assume lut has not changed
		m_lutchanged = false;

		// propagate mouse event to all TFE object
		for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
		{
			// if an object caught event stop event propagation
			if((*it)->motion(xx,yy))
			{
				m_lutchanged = true; // object modified lut
				return true;
			}
		}

		// if none has caught it, play with histogram
		if(m_histogram.motion(xx,yy))
		{
			// update other objects visible area
			for(std::list<TFEobject*>::iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
				(*it)->clamp(m_histogram.getLeft(),m_histogram.getRight());

			// histogram setup has changed
			return true;
		}
		else
			return false;
	}


	void Editor::colorMode()
	{
		m_colorMode = true;
	}


	void Editor::display() const
	{
		if(!m_hide)
		{
			/* overlay on screen */
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			glOrtho(-0.5,+0.5,-0.5,+0.5,-1,+1);
		
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadIdentity();
		
			/* place TFE on screen with (0,0) beeing the lowest left corner */
			glScalef(1.0f/m_win_width, 1.0f/m_win_height, 1);
			glTranslatef((float)(m_posx-m_win_width/2+m_width/2), (float)(m_posy-m_win_height/2+m_height/2), 0);
			glScalef((float)m_width, (float)m_height, 1);
		
			/* draw a border */
			glColor3f(1,1,1);
			glBegin(GL_LINE_LOOP);
				glVertex3f(-0.5,-0.5,0);
				glVertex3f(+0.5,-0.5,0);
				glVertex3f(+0.5,+0.5,0);
				glVertex3f(-0.5,+0.5,0);
			glEnd();

			/* draw histogram */
			m_histogram.display();
		
			/* draw TFE objects (ramp trapezoids) */
			for(std::list<TFEobject*>::const_iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
				(*it)->display();

			/* draw the color selector is in color mode */
			if(m_colorMode)
			{
				float colorSize = m_height*0.8f;
				float colorOffset = m_height*0.1f;
				glLoadIdentity();
				glScalef(1.0f/m_win_width, 1.0f/m_win_height, 1);
				glTranslatef((float)(m_posx+colorOffset-m_win_width/2+colorSize/2), (float)(m_posy+colorOffset-m_win_height/2+colorSize/2), 0);
				glScalef(colorSize, colorSize, 1);
				
				m_color.display();
			}

			/* pop some matrices */
			glMatrixMode(GL_PROJECTION);
			glPopMatrix();
		
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
		}
	}


	void Editor::get(float i, float &r, float &g, float &b, float &a) const
	{
		/* final rgba is the rgba returned by the object with the highest opacity (a) */
		float max_a = 0;

		for(std::list<TFEobject*>::const_iterator it=m_manips.begin(); it!=m_manips.end(); ++it)
		{
			/* get rgba of current manip */
			float rr,gg,bb,aa;
			(*it)->get(i, rr,gg,bb,aa);

			/* check if this object will update final rgba */
			if(aa >= max_a)
				r = rr,
				g = gg,
				b = bb,
				a = aa,
				max_a = aa;
		}

		/* change contrast, scale color */
		r = r * m_contrast; if(r<0) r=0; else if(r>1) r=1;
		g = g * m_contrast; if(g<0) g=0; else if(g>1) g=1;
		b = b * m_contrast; if(b<0) b=0; else if(b>1) b=1;

		/* change brithness, offset color */
		r = r + m_brightness; if(r<0) r=0; else if(r>1) r=1;
		g = g + m_brightness; if(g<0) g=0; else if(g>1) g=1;
		b = b + m_brightness; if(b<0) b=0; else if(b>1) b=1;
	}

}} // namespace
