/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	pick.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <tools/pick.h>
#include <error.h>

namespace Tools
{
	Pick::Pick()
	:	m_maxalpha(0.2f),
		m_displayId(0),
		m_displayCreated(false)
	{
		m_modelview[0]  = 1; m_modelview[1]  = 0; m_modelview[2]  = 0; m_modelview[3]  = 0;
		m_modelview[4]  = 0; m_modelview[5]  = 1; m_modelview[6]  = 0; m_modelview[7]  = 0;
		m_modelview[8]  = 0; m_modelview[9]  = 0; m_modelview[10] = 1; m_modelview[11] = 0;
		m_modelview[12] = 0; m_modelview[13] = 0; m_modelview[14] = 0; m_modelview[15] = 1;

		m_projection[0]  = 1; m_projection[1]  = 0; m_projection[2]  = 0; m_projection[3]  = 0;
		m_projection[4]  = 0; m_projection[5]  = 1; m_projection[6]  = 0; m_projection[7]  = 0;
		m_projection[8]  = 0; m_projection[9]  = 0; m_projection[10] = 1; m_projection[11] = 0;
		m_projection[12] = 0; m_projection[13] = 0; m_projection[14] = 0; m_projection[15] = 1;
	}


	Pick::~Pick()
	{
		if(m_displayCreated && glIsList(m_displayId)) glDeleteLists(m_displayId,1);
	}


	void Pick::saveMatrices()
	{
		glGetDoublev(GL_MODELVIEW_MATRIX, m_modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, m_projection);
	}


	void Pick::enable() const
	{
		/* save the visualiaztion matrices */
		glGetDoublev(GL_MODELVIEW_MATRIX, m_modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, m_projection);

		/* check wether a display list is currently being created */
		GLint mode;
		GLint id;
		glGetIntegerv(GL_LIST_MODE, &mode);
		glGetIntegerv(GL_LIST_INDEX, &id);
		if(glIsList(id) && (mode == GL_COMPILE || mode == GL_COMPILE_AND_EXECUTE))
		{
			m_displayId = id;
			m_displayCreated = false;
		}
		else
		{
			/* check if a display list has already been created */
			if(!glIsList(m_displayId))
			{
				m_displayId = glGenLists(1);
			}

			/* define the list, all further commands will be
			 * executed and put in this display list */
			glNewList(m_displayId, GL_COMPILE_AND_EXECUTE);
			m_displayCreated = true;
			glCheckError();
		}
	}


	void Pick::disable() const
	{
		/* end of display list */
		if(m_displayCreated)
			glEndList();
		glCheckError();
	}


	bool Pick::get(int mouse_x, int mouse_y, float &x, float &y, float &z) const
	{
		/* saving opengl matrices */
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);

		// set the viewport at the origin (to match the mouse coordinate that has (0,0) as origin)
		viewport[0] = 0;
		viewport[1] = 0;
		glViewport(viewport[0],viewport[1],viewport[2],viewport[3]);

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadMatrixd(m_projection); // using saved projection

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixd(m_modelview); // using saved modelview

		/* render object */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear buffers
		glAlphaFunc(GL_GREATER, m_maxalpha); // avoid transparent fragment to be rendered
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_DEPTH_TEST);

		/* draw objects to be picked */
		glCallList(m_displayId);

		glDisable(GL_ALPHA_TEST);
		glDisable(GL_DEPTH_TEST);
		glFlush();

		/* restoring opengl matrices */
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();

		/* done with rendering, now do some conversion */

		/* depth of pixel (x,y) */
		GLfloat mouse_z = 0;
		glReadPixels(mouse_x, mouse_y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &mouse_z);

		/* what is the 3D position of pixel (x,y) */
		GLdouble xx,yy,zz;
		gluUnProject(mouse_x, mouse_y, mouse_z, m_modelview, m_projection, viewport, &xx, &yy, &zz);
		x = (float)xx; y = (float)yy; z = (float)zz;

		/* if the Z buffer is not the far plane, an object has been hit */
		if(mouse_z < 1) return true; // object is hit
		else            return false;
	}


	void Pick::setMaxAlpha(float max)
	{
		m_maxalpha = max;
	}

} // namespace
