/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	display2d_drawpixels.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <display/display2d_drawpixels.h>
#include <cstdlib>

namespace libvdDisplay
{

	ImageDrawPixels::ImageDrawPixels()
	:	m_frame(0)
	{
	}


	ImageDrawPixels::~ImageDrawPixels()
	{
		if(m_frame) free(m_frame);
	}


	void ImageDrawPixels::display() const
	{
		if(m_frame)
		{
			// Draw pixels on frame buffer
			glDrawPixels(m_size[0],
					 m_size[1],
					 m_format,
					 m_type,
					 m_frame);
		}
	}


	// specialization for image type
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<unsigned char> ::type = GL_UNSIGNED_BYTE;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<unsigned short>::type = GL_UNSIGNED_SHORT;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<unsigned int>  ::type = GL_UNSIGNED_INT;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<char>          ::type = GL_BYTE;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<short>         ::type = GL_SHORT;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<int>           ::type = GL_INT;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<float>         ::type = GL_FLOAT;
	template <> GLenum ImageDrawPixels::DrawPixelsArgs<double>        ::type = GL_FLOAT;


} // namespace
