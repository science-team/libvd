/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ioavi.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <io/ioavi.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <error.h>


/// swap bytes if cpu is big endian (eg. apple ppc), do nothing if little endian (eg. intel pc)
#if APPLE
#define INTCPU(i) i = ((i&0xff000000)>>24) | (((i&0x00ff0000)>>16)<<8) | (((i&0x0000ff00)>>8)<<16) | ((i&0x000000ff)<<24);
#else
#define INTCPU(i) ;
#endif


namespace IO
{
	void readAVIFrame(const char*            filename,
	             color<unsigned char>* &frame,
	             unsigned int          &frame_width,
	             unsigned int          &frame_height,
	             unsigned int           frame_nb)
	{
		AVI reader(filename);
		reader.read(frame, frame_nb);
		reader.getFrameSize(frame_width, frame_height);
	}
		
		
	void readAVI(const char*            filename,
	             color<unsigned char>* &image,
	             unsigned int          &frame_width,
	             unsigned int          &frame_height,
	             unsigned int          &nb_frames)
	{
		AVI reader(filename);
		reader.read(image);
		reader.getFrameSize(frame_width, frame_height);
		nb_frames = reader.getNbFrames();
	}


	AVI::~AVI()
	{
		if(m_fd) fclose(m_fd);
	}


	AVI::AVI(const char* filename)
	{
		if(filename) open(filename);
	}


	void AVI::getFrameSize(unsigned int &width, unsigned int &height)
	{
		// Check if file is opened
		if(!m_fd) throw LOCATE(Error("file not opened"));

		width  = m_header.width;
		height = m_header.height;
	}


	void AVI::open(const char* filename)
	{
		if(!filename) throw LOCATE(Error("no filename"));

		// Open file
		m_fd = fopen(filename, "rb");
		if(!m_fd) throw LOCATE(Error("open"));

		// Read header
		readHeader();
	}


	void AVI::close()
	{
		if(!m_fd) throw LOCATE(Error("nothing to close"));

		// Close file
		fclose(m_fd);
	}


	void AVI::readHeader()
	{
		size_t bread; /* read bytes */
		char ident[4];
		unsigned int size;
		char *data;
		bool loop = true;

		// Check if a file is opened
		if(!m_fd) throw LOCATE(Error("nothing to read"));

		/*******************
		 * Read file header
		 *******************/

		/* (RIFF) (filesize) (AVI ) */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"RIFF",4)) throw LOCATE(Error("not a RIFF file (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);
		
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"AVI ",4)) throw LOCATE(Error("not an AVI file (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		/* (LIST) (header size) (hdrl) : header list */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"LIST",4)) throw LOCATE(Error("LIST expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);

		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"hdrl",4)) throw LOCATE(Error("hdrl expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		/* (avih) (avi header size) (data) : avi header */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"avih",4)) throw LOCATE(Error("avih expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);
		if(size != 56) throw LOCATE(Error("wrong header file"));

		/* avi header */
		bread = fread(&m_header, size, 1, m_fd);

		/* (LIST) (stream headers size) (strl) : list of stream headers */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"LIST",4)) throw LOCATE(Error("LIST expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"strl",4)) throw LOCATE(Error("strl expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		/* (strh) (stream header size) (data) : stream header */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"strh",4)) throw LOCATE(Error("strh expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

		/* stream header */
		data = (char*)malloc(size);
		bread = fread(data, 1, size, m_fd);
		free(data);

		/* (strf) (stream format size) (data) : stream format */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"strf",4)) throw LOCATE(Error("strf expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

		/* stream format */
		data = (char*)malloc(size);
		bread = fread(data, 1, size, m_fd);
		free(data);

		// stream format read

		while(loop)
		{
			/* optional (strn|strd) (size) (data)
			 * strn - stream name
			 * strd - stream header */
			bread = fread(ident, 1, 4, m_fd);

			if( !strncmp(ident,"strn",4) ||
				!strncmp(ident,"strd",4) )
			{
				bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

				/* stream name or stream header */
				data = (char*)malloc(size);
				bread = fread(data, 1, size+1, m_fd); /* read name +'\0' */
				free(data);
			}
			else if(!strncmp(ident,"indx",4) ||
					!strncmp(ident,"idxl",4))
				return; // need to read chunk index, but it is not necessary
			else if(!strncmp(ident,"JUNK",4))
				loop = false; // stop loop, read JUNK padding
			else
				throw LOCATE(Error("strn or strd expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));
		}

		// optional stream name or stream header read

		/* (JUNK) (size) (padding) : 2k limit */
		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

		bread = fseek(m_fd, size, SEEK_CUR); /* jump padding */
		if(bread) throw LOCATE(Error("jump padding"));

		/* (LIST) (data size) (movie) : list of chunks */
		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"LIST",4)) throw LOCATE(Error("LIST expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));

		bread = fread(&size, 4, 1, m_fd);	INTCPU(size);

		bread = fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"movi",4)) throw LOCATE(Error("movi expected while reading (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));
	}


	unsigned int AVI::getNbFrames()
	{
		if(!m_fd) throw LOCATE(Error("file not opened"));

		return m_header.number_frames;
	}


	void AVI::read(color<unsigned char>* &image, unsigned int frame)
	{
		char ident[4];
		unsigned int size;
		
		/*******************
		 * Read frame
		 *******************/
		
		// Check if file is opened
		if(!m_fd) throw LOCATE(Error("file not opened"));
		
		// Check if the frame requested exist in file
		if(frame<0 || frame>=m_header.number_frames) throw LOCATE(Error("frame out of bound")); // out of movie
		
		// Go to the actual frame
		unsigned long offset = 2048 + frame*(m_header.width*m_header.height*3+8);
		fseek(m_fd, offset, SEEK_SET);
		
		/* (00db) (frame size) */
		fread(ident, 1, 4, m_fd);
		if(strncmp(ident,"00db",4)) throw LOCATE(Error("wrong format"));
		
		/* read frame size */
		fread(&size, 4, 1, m_fd);	INTCPU(size);
		
		/* check if frame size is the same as image size */
		if(size != m_header.width*m_header.height*3) throw LOCATE(Error("wrong frame size"));
		
		/* allocate memory for the image */
		image = new color<unsigned char> [m_header.width * m_header.height];
		
		/* read this frame */
		unsigned int n = (unsigned int)fread(image, 3, m_header.width*m_header.height, m_fd);
		if(n != m_header.width*m_header.height) throw LOCATE(Error("EOF reached while reading"));
	}

	void AVI::read(color<unsigned char>* &image) // read whole movie
	{
		read(image, 0, m_header.number_frames-1);
	}

	void AVI::read(color<unsigned char>* &image, unsigned int start, unsigned int end) // read part of the movie
	{
		char ident[4];
		unsigned int size;
		
		// Check if file is opened
		if(!m_fd) throw LOCATE(Error("file not opened"));
		
		// Check if the frame requested exist in file
		if(start<0 || start>=m_header.number_frames) throw LOCATE(Error("start position out of bound")); // out of movie
		if(end<0   || end>=m_header.number_frames)   throw LOCATE(Error("end position out of bound")); // out of movie
		if(start>end) throw LOCATE(Error("start position is after end position")); // this may become a reverse reading
		
		// Go to the actual frame
		unsigned long offset = 2048 + start*(m_header.width*m_header.height*3+8);
		fseek(m_fd, offset, SEEK_SET);
		
		/*******************
		 * Create Image (2D+t)
		 *******************/
		
		image = new color<unsigned char> [m_header.width * m_header.height * (end-start+1)];
		
		/*******************
		* Read file header
		*******************/
		
		for(unsigned int t=0; t<end-start+1; ++t)
		{
			/* (00db) (frame size) */
			fread(ident, 1, 4, m_fd);
			if(strncmp(ident,"00db",4)) throw LOCATE(Error("wrong format (%c%c%c%c)",ident[0],ident[1],ident[2],ident[3]));
			
			/* read frame size */
			fread(&size, 4, 1, m_fd);	INTCPU(size);
			
			/* check if frame size is the same as image size */
			if(size != m_header.width*m_header.height*3) throw LOCATE(Error("wrong frame size"));
			
			/* read this frame */
			unsigned int n = (unsigned int)fread(&image[t*m_header.width*m_header.height], 3, m_header.width*m_header.height, m_fd);
			if(n != m_header.width*m_header.height) throw LOCATE(Error("EOF reached while reading"));
		}
	}


}; // namespace


