/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	clip.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/clip.h>
#include <map>
#include <utility>


namespace Tools
{
	Clip::PlaneManager* Clip::m_planeManager = NULL; // which plane is available next


	Clip::PlaneManager::PlaneManager()
	{
		for(int i=0; i!=NBPLANES; ++i) m_planes[i] = true; // all planes available
	}


	GLenum Clip::PlaneManager::allocate()
	{
		// return the first plane
		for(int i=0; i!=NBPLANES; ++i)
		{
			if(m_planes[i])
			{
				m_planes[i] = false; // this plane is no longer available
				return (GL_CLIP_PLANE0+i);
			}
		}
		throw LOCATE(Error("cannot find a new clip plane"));
	}


	void Clip::PlaneManager::free(GLenum plane)
	{
		int index = plane-GL_CLIP_PLANE0;
		if(index>=0 && index<NBPLANES)
			m_planes[index] = true;
		else
			throw LOCATE(Error("Plane %d is not defined by OpenGL", plane));
	}


	Clip::Clip()
	:	m_planeId(0)
	{
		if(!m_planeManager) m_planeManager = new PlaneManager();
		m_planeId = m_planeManager->allocate();
		init();
	}


	Clip::~Clip()
	{
		m_planeManager->free(m_planeId);
	}


	void Clip::init()
	{
		m_planeMatrix[0]  = 1.0f; m_planeMatrix[1]  = 0.0f; m_planeMatrix[2]  = 0.0f; m_planeMatrix[3]  = 0.0f; 
		m_planeMatrix[4]  = 0.0f; m_planeMatrix[5]  = 1.0f; m_planeMatrix[6]  = 0.0f; m_planeMatrix[7]  = 0.0f; 
		m_planeMatrix[8]  = 0.0f; m_planeMatrix[9]  = 0.0f; m_planeMatrix[10] = 1.0f; m_planeMatrix[11] = 0.0f; 
		m_planeMatrix[12] = 0.0f; m_planeMatrix[13] = 0.0f; m_planeMatrix[14] = 0.0f; m_planeMatrix[15] = 1.0f;

		m_modelview[0]  = 1.0f; m_modelview[1]  = 0.0f; m_modelview[2]  = 0.0f; m_modelview[3]  = 0.0f; 
		m_modelview[4]  = 0.0f; m_modelview[5]  = 1.0f; m_modelview[6]  = 0.0f; m_modelview[7]  = 0.0f; 
		m_modelview[8]  = 0.0f; m_modelview[9]  = 0.0f; m_modelview[10] = 1.0f; m_modelview[11] = 0.0f; 
		m_modelview[12] = 0.0f; m_modelview[13] = 0.0f; m_modelview[14] = 0.0f; m_modelview[15] = 1.0f;
	}


	void Clip::slide(float d)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_planeMatrix);
		glTranslatef(0,0,d);          // do some translation (screen aligned)
		glGetFloatv(GL_MODELVIEW_MATRIX, m_planeMatrix); // plane matrix is updated
		glPopMatrix();
	}


	void Clip::rotateWithScreen(float dx, float dy)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_planeMatrix);

		/* we invert the modelview (with the plane matrix) */
		float matrix[16]; mult(m_modelview, m_planeMatrix, matrix);
		float inv_matrix[16]; for(unsigned int m=0; m<16; ++m) inv_matrix[m] = matrix[m];
		transp(inv_matrix); // invert modelview matrix, ignore translation

		glRotatef(dx, inv_matrix[4], inv_matrix[5], inv_matrix[6]); // around plane Y axis
		glRotatef(-dy, inv_matrix[0], inv_matrix[1], inv_matrix[2]); // around plane X axis
		glGetFloatv(GL_MODELVIEW_MATRIX, m_planeMatrix); // plane matrix is updated
		glPopMatrix();

		correctMatrix(); // correct translation part
	}


	void Clip::rotateWithWorld(float dx, float dy)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_planeMatrix);

		glRotatef(dx, 0,1,0); // around plane Y axis
		glRotatef(dy, 1,0,0); // around plane X axis

		glGetFloatv(GL_MODELVIEW_MATRIX, m_planeMatrix); // plane matrix is updated
		glPopMatrix();

		correctMatrix(); // correct translation part
	}


	void Clip::getPlaneMatrix(float m[]) const
	{
		for(unsigned int i=0; i<16; ++i) m[i] = m_planeMatrix[i];
	}


	void Clip::setPlaneMatrix(const float m[])
	{
		for(unsigned int i=0; i<16; ++i) m_planeMatrix[i] = m[i];
	}


	void Clip::enable()
	{
		/* distance to the plane from origin */
		float d = m_planeMatrix[8]*m_planeMatrix[12] + // dot product of up and translation vector
				  m_planeMatrix[9]*m_planeMatrix[13] +
				  m_planeMatrix[10]*m_planeMatrix[14];

		/* clip the plane */
		GLdouble equation[4] = {m_planeMatrix[8], m_planeMatrix[9], m_planeMatrix[10], -d};
		glClipPlane(m_planeId, equation);

		glEnable(m_planeId);

		/* save modelview matrix, so we can later do screen-aligned rotations */
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview);
	}


	void Clip::disable()
	{
		glDisable(m_planeId);
	}


	void Clip::display(bool use_cross_section) const
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		// if we are currently clipping
		// temporaly disable the clipping plane
		GLboolean enable = glIsEnabled(m_planeId);

		if(enable) glDisable(m_planeId);

		// Draw a cross section, or a plane
		if(use_cross_section)
			drawCrossSection(m_planeMatrix);
		else
			drawPlane(m_planeMatrix);

		// reenable clip plane
		if(enable) glEnable(m_planeId);

		glPopMatrix();
	}


	void Clip::correctMatrix()
	{
		/* distance to the plane from origin */
		float d = m_planeMatrix[8]*m_planeMatrix[12] + // dot product of up and translation vector
				  m_planeMatrix[9]*m_planeMatrix[13] +
				  m_planeMatrix[10]*m_planeMatrix[14];

		/* update translation part so it is parralel to the plane normal vector */
		m_planeMatrix[12] = m_planeMatrix[8]*d;
		m_planeMatrix[13] = m_planeMatrix[9]*d;
		m_planeMatrix[14] = m_planeMatrix[10]*d;
	}


	void Clip::mult(const float a[], const float b[], float m[])
	{
		for(unsigned int i=0; i<16; ++i) m[i] = 0;

		for(unsigned int i=0; i<4; ++i)
			for(unsigned int j=0; j<4; ++j)
				for(unsigned int k=0; k<4; ++k)
			m[j*4+i] = m[j*4+i] + a[k*4+i] * b[j*4+k];
	}


	void Clip::transp(float m[])
	{
		for(unsigned int j=0; j<4; ++j)
			for(unsigned int i=0; i<j; ++i)
		{
			float a = m[j*4+i];
			m[j*4+i] = m[i*4+j];
			m[i*4+j] = a;
		}
	}


	void Clip::drawPlane(const float m[]) const
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		glMultMatrixf(m);

		// Draw a plane
		glBegin(GL_LINE_LOOP);
		glVertex3f(-0.5,-0.5,0);
		glVertex3f(+0.5,-0.5,0);
		glVertex3f(+0.5,+0.5,0);
		glVertex3f(-0.5,+0.5,0);
		glEnd();

		glPopMatrix();
	}


	void Clip::drawCrossSection(const float m[]) const
	{
		/* This method displays stacked cross-section
		 *   uglier code in order to compute the cross-section (sorting of crossing edges)
		 *   however only the cross-section is displayed, no unnecessary rendering is done outside the cube
		 */

		/* cube
		 *
		 *      4----------7
		 *      |\         |\
		 *      | \        | \
		 *      |  5----------6
		 *      |  |       |  |
		 *      0--|-------3  |
		 *       \ |        \ |
		 *        \|         \|
		 *         1----------2
		 *
		 * edges are: (01) (12) (23) (30)
		 *            (45) (56) (67) (74)
		 *            (04) (15) (26) (37)
		 *
		 */

		/*
		 *          ^
		 *      (n) |   \ c[e[i][0]]
		 *          |    \
		 *         +------\----------+
		 *       /  |      o       /
		 *     /    O       .    /
		 *   +-----------------+
		 *                    . c[e[i][1]]
		 *
		 * Edge intersecting the plane
		 * Vector (v) start at the plane origin and ends at a cube corner
		 */
		

		/* Where the cube corners are */
		float c[8][3] = { // cuber corners
			{-0.5f, -0.5f, -0.5f},
			{-0.5f, +0.5f, -0.5f},
			{+0.5f, +0.5f, -0.5f},
			{+0.5f, -0.5f, -0.5f},
			{-0.5f, -0.5f, +0.5f},
			{-0.5f, +0.5f, +0.5f},
			{+0.5f, +0.5f, +0.5f},
			{+0.5f, -0.5f, +0.5f},
		};

		/* Edges of the cube */
		int e[12][2] = { // edges
			{0,1}, {1,2}, {2,3}, {3,0},
			{4,5}, {5,6}, {6,7}, {7,4},
			{0,4}, {1,5}, {2,6}, {3,7}
		};

		/* Vector (v): from plane origin to cube corners */
		float v[8][3] = { // cuber corners relative to plane origin
			{-0.5f-m[12], -0.5f-m[13], -0.5f-m[14]},
			{-0.5f-m[12], +0.5f-m[13], -0.5f-m[14]},
			{+0.5f-m[12], +0.5f-m[13], -0.5f-m[14]},
			{+0.5f-m[12], -0.5f-m[13], -0.5f-m[14]},
			{-0.5f-m[12], -0.5f-m[13], +0.5f-m[14]},
			{-0.5f-m[12], +0.5f-m[13], +0.5f-m[14]},
			{+0.5f-m[12], +0.5f-m[13], +0.5f-m[14]},
			{+0.5f-m[12], -0.5f-m[13], +0.5f-m[14]},
		};

		/* Signed distance from cube corners to the plane
		 * it is the dot-product of plane up vector (n) with (v) */
		float nv[8] = {
			m[8]*v[0][0]+m[9]*v[0][1]+m[10]*v[0][2],
			m[8]*v[1][0]+m[9]*v[1][1]+m[10]*v[1][2],
			m[8]*v[2][0]+m[9]*v[2][1]+m[10]*v[2][2],
			m[8]*v[3][0]+m[9]*v[3][1]+m[10]*v[3][2],
			m[8]*v[4][0]+m[9]*v[4][1]+m[10]*v[4][2],
			m[8]*v[5][0]+m[9]*v[5][1]+m[10]*v[5][2],
			m[8]*v[6][0]+m[9]*v[6][1]+m[10]*v[6][2],
			m[8]*v[7][0]+m[9]*v[7][1]+m[10]*v[7][2],
		};

		/* If edge ends have a different sign, the edge crosses the plane */
		int cross[12]; // is edge[i] crossing plane

		for(unsigned int i=0; i<12; ++i)
		{
			if(nv[e[i][0]]*nv[e[i][1]]<0)
				cross[i] = 1;
			else
				cross[i] = 0;
		}

		/* Compute the intersection points on each crossing edge */
		std::vector<float> points;
		for(unsigned int i=0; i<12; ++i)
		{
			if(cross[i])
			{
				float u[3] = {
					c[e[i][1]][0]-c[e[i][0]][0],
					c[e[i][1]][1]-c[e[i][0]][1],
					c[e[i][1]][2]-c[e[i][0]][2]
				};

				float nu = m[8]*u[0]+m[9]*u[1]+m[10]*u[2];

				float t = - nv[e[i][0]] / nu;

				float inters[3] = {
					c[e[i][0]][0]+t*u[0],
					c[e[i][0]][1]+t*u[1],
					c[e[i][0]][2]+t*u[2]
				};

				points.push_back(inters[0]);
				points.push_back(inters[1]);
				points.push_back(inters[2]);
			}
		}

		/* If we have more than two points, we can draw a polygon */
		if(points.size()>6) // if more than two points
		{
			/* Sort these points so we can correctly draw a polygon */
			std::multimap<float,int> map;
			float u[3] = { points[3]-points[0], points[4]-points[1], points[5]-points[2] };
			float lul = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
			for(unsigned int i=2; i<points.size()/3; ++i)
			{
				float v[3] = { points[i*3+0]-points[0],points[i*3+1]-points[1],points[i*3+2]-points[2] };
				float lvl = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
	
				float cos_angle = (v[0]*u[0]+v[1]*u[1]+v[2]*u[2])/(lul*lvl);
				map.insert(std::pair<float,int>(cos_angle,i));
			}
	
			/* Draw the cube cross-section with a texture on it */
			glBegin(GL_LINE_LOOP);
			glTexCoord3f(points[0],points[1],points[2]);
			glVertex3f  (points[0],points[1],points[2]);
			glTexCoord3f(points[3],points[4],points[5]);
			glVertex3f  (points[3],points[4],points[5]);
			for(std::multimap<float,int>::reverse_iterator it = map.rbegin(); it != map.rend(); ++it)
			{
				glTexCoord3f(points[it->second*3+0],points[it->second*3+1],points[it->second*3+2]);
				glVertex3f  (points[it->second*3+0],points[it->second*3+1],points[it->second*3+2]);
			}
			glEnd();

		} // if more than 2 points
	}

} // namespace
