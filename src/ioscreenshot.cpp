/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ioscreenshot.cpp
 * @author	Herve Lombaert
 * @date	Tue Feb 5 2007
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <io/ioscreenshot.h>

namespace IO
{	
	/* Read OpenGL buffer and create an raw 24bit image */
	void glScreenshot(color<unsigned char>* &image, unsigned int &width, unsigned int &height)
	{
		/* NB: This takes whatever is in the default OpenGL pixel buffer,
		 * we flush it to make sure it ready */
		glFlush();

		/* get viewport size */
		GLint    viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		width  = viewport[2];
		height = viewport[3];

		/* create an image buffer */
		unsigned char* buffer = new unsigned char [width * height * 4];

		/* read opengl buffer in rgba (this should be supported with most drivers) */
		glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		/* transfer buffer to image */
		image = new color<unsigned char> [width * height];
		for(unsigned int i=0; i<width*height; ++i)
			image[i].r = buffer[i*4+0],
			image[i].g = buffer[i*4+1],
			image[i].b = buffer[i*4+2];

		delete [] buffer;
	}
		
}
