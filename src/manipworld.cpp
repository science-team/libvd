/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	manipworld.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <tools/manipworld.h>

namespace Tools
{
	ManipWorld::ManipWorld()
	{
		init();
	}


	ManipWorld::~ManipWorld()
	{
	}


	void ManipWorld::init()
	{
		m_modelview[0]  = 1.0f;
		m_modelview[1]  = 0.0f;
		m_modelview[2]  = 0.0f;
		m_modelview[3]  = 0.0f;

		m_modelview[4]  = 0.0f;
		m_modelview[5]  = 1.0f;
		m_modelview[6]  = 0.0f;
		m_modelview[7]  = 0.0f;

		m_modelview[8]  = 0.0f;
		m_modelview[9]  = 0.0f;
		m_modelview[10] = 1.0f;
		m_modelview[11] = 0.0f;

		m_modelview[12] = 0.0f;
		m_modelview[13] = 0.0f;
		m_modelview[14] = 0.0f;
		m_modelview[15] = 1.0f;
	}


	void ManipWorld::translateAxis(float dx, float dy, float dz)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview); // modelview is controled by this object
		float inv_modelview[16]; for(unsigned int m=0; m<16; ++m) inv_modelview[m] = m_modelview[m];
		transp(inv_modelview); // invert modelview matrix, ignore translation
		glTranslatef(dx*inv_modelview[0],dx*inv_modelview[1],dx*inv_modelview[2]);     // do some translation in the X screen axis
		glTranslatef(dy*inv_modelview[4],dy*inv_modelview[5],dy*inv_modelview[6]);     // do some translation in the Y screen axis
		glTranslatef(dz*inv_modelview[8],dz*inv_modelview[9],dz*inv_modelview[10]);    // do some translation in the Z screen axis
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::translate(float dx, float dy, float dz)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview); // modelview is controled by this object
		glTranslatef(dx,dy,dz);     // do some translation in the world
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::zoom(float zoom)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview);
		glScalef(zoom,zoom,zoom);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateXAxis(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview);
		float inv_modelview[16]; for(unsigned int m=0; m<16; ++m) inv_modelview[m] = m_modelview[m];
		transp(inv_modelview); // invert modelview matrix, ignore translation
		glRotatef(-delta, inv_modelview[0], inv_modelview[1], inv_modelview[2]);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateYAxis(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview);
		float inv_modelview[16]; for(unsigned int m=0; m<16; ++m) inv_modelview[m] = m_modelview[m];
		transp(inv_modelview); // invert modelview matrix, ignore translation
		glRotatef(delta, inv_modelview[4], inv_modelview[5], inv_modelview[6]);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateZAxis(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview);
		float inv_modelview[16]; for(unsigned int m=0; m<16; ++m) inv_modelview[m] = m_modelview[m];
		transp(inv_modelview); // invert modelview matrix, ignore translation
		glRotatef(delta, inv_modelview[8], inv_modelview[9], inv_modelview[10]); // tilt
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateX(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview); // modelview is controled by this object
		glRotatef(-delta, 1,0,0);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateY(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview); // modelview is controled by this object
		glRotatef(delta, 0,1,0);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::rotateZ(float delta)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(m_modelview); // modelview is controled by this object
		glRotatef(delta, 0,0,1);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview); // modelview matrix is updated
		glPopMatrix();
	}


	void ManipWorld::loadModelViewMatrix() const
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(m_modelview);
	}


	void ManipWorld::loadInverseModelViewMatrix() const
	{
		// invert rotation
		float inv_modelview[16]; for(unsigned int m=0; m<16; ++m) inv_modelview[m] = m_modelview[m];
		transp(inv_modelview); // invert modelview matrix, ignore translation

		// invert translation
		inv_modelview[12] = -( inv_modelview[0]*m_modelview[12] + inv_modelview[4]*m_modelview[13] + inv_modelview[8 ]*m_modelview[14] );
		inv_modelview[13] = -( inv_modelview[1]*m_modelview[12] + inv_modelview[5]*m_modelview[13] + inv_modelview[9 ]*m_modelview[14] );
		inv_modelview[14] = -( inv_modelview[2]*m_modelview[12] + inv_modelview[6]*m_modelview[13] + inv_modelview[10]*m_modelview[14] );

		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(inv_modelview);
	}


	void ManipWorld::saveModelViewMatrix()
	{
		glMatrixMode(GL_MODELVIEW);
		glGetFloatv(GL_MODELVIEW_MATRIX, m_modelview);
	}


	void ManipWorld::getModelViewMatrix(float m[]) const
	{
		for(unsigned int i=0; i<16; ++i) m[i] = m_modelview[i];
	}


	void ManipWorld::setModelViewMatrix(const float m[])
	{
		for(unsigned int i=0; i<16; ++i) m_modelview[i] = m[i];
	}


	bool ManipWorld::pick(int mouse_x, int mouse_y, float &x, float &y, float &z)
	{
		/* NB: assume this object controls the modelview matrix
		 * and the current projection matrix is untouched since the last
		 * scene display */

		/* get some matrices */
		GLint    viewport[4];    glGetIntegerv(GL_VIEWPORT, viewport);
		GLdouble projection[16]; glGetDoublev(GL_PROJECTION_MATRIX, projection);
		GLdouble modelview[16];  glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

		/* depth of pixel (x,y) */
		GLfloat mouse_z = 0;
		glReadPixels(mouse_x, mouse_y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &mouse_z);

		/* what is the 3D position of pixel (x,y) */
		GLdouble xx,yy,zz;
		gluUnProject(mouse_x, mouse_y, mouse_z, modelview, projection, viewport, &xx, &yy, &zz);
		x = (float)xx, y = (float)yy, z = (float)zz;

		/* if the Z buffer is not the far plane, an object has been hit */
		if(mouse_z < 1) return true; // object is hit
		else            return false;
	}


	void ManipWorld::transp(float m[])
	{
		for(unsigned int j=0; j<4; ++j)
			for(unsigned int i=0; i<j; ++i)
		{
			float a = m[j*4+i];
			m[j*4+i] = m[i*4+j];
			m[i*4+j] = a;
		}
	}

} // namespace
