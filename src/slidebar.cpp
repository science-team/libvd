/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/** @file slidebar.cpp
 *  @author Herve Lombaert
 *	@date	Feb 7th, 2007
 *
 *	Adds a slide bar
 */

#if WIN32
#include <windows.h>
#endif

#if __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/slidebar.h>

namespace Tools
{

	SlideBar::SlideBar()
	:	m_val(0),
		m_posx(0),
		m_posy(0),
		m_width(0),
		m_height(0),
		m_win_width(0),
		m_win_height(0),
		m_mouse_pressed(0)
	{}

	SlideBar::~SlideBar()
	{
	}

	float SlideBar::get() const
	{
		return m_val;
	}

	void SlideBar::set(float val)
	{
		m_val = val;
	}

	void SlideBar::display() const
	{
			/* overlay on screen */
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			glOrtho(-0.5,+0.5,-0.5,+0.5,-1,+1);
		
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadIdentity();
		
			/* place slide bar on screen with (0,0) beeing the lowest left corner */
			glScalef(1.0f/m_win_width, 1.0f/m_win_height, 1);
			glTranslatef((float)(m_posx-m_win_width/2+m_width/2), (float)(m_posy-m_win_height/2+m_height/2), 0);
			glScalef((float)m_width, (float)m_height, 1);
		
			/* draw a border */
			glColor3f(1,1,1);
			glBegin(GL_LINE_LOOP);
				glVertex3f(-0.5f,-0.5f,0);
				glVertex3f(+0.5f,-0.5f,0);
				glVertex3f(+0.5f,+0.5f,0);
				glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			/* draw the bar */
			glColor3f(0,1,1);
			glPushMatrix();
			glTranslatef(m_val-0.5f,0,0);
			glScalef(10.0f/m_win_width,1,1);
			glBegin(GL_QUADS);
				glVertex3f(-0.5f,-0.5f,0);
				glVertex3f(+0.5f,-0.5f,0);
				glVertex3f(+0.5f,+0.5f,0);
				glVertex3f(-0.5f,+0.5f,0);
			glEnd();
			glPopMatrix();

			/* pop some matrices */
			glMatrixMode(GL_PROJECTION);
			glPopMatrix();
		
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
	}

	void SlideBar::place(int x, int y, int width, int height, int win_width, int win_height)
	{
		m_posx = x;
		m_posy = y;
		m_width = width;
		m_height = height;
		m_win_width = win_width;
		m_win_height = win_height;
	}

	bool SlideBar::mouse(int x, int y)
	{
		// normalize mouse position (x,y) with slidebar size
		float xx = (float)(x-m_posx) / m_width;
		float yy = (float)(y-m_posy) / m_height;

		// is the mouse on the bar
		if(xx >= m_val-0.1f && xx <= m_val+0.1f && yy >= 0 && yy <= 1)
		{
			m_mouse_pressed = true;
			return true;
		}
		else
		{
			m_mouse_pressed = false;
			return false;
		}
	}

	bool SlideBar::motion(int x, int y)
	{
		if(m_mouse_pressed)
		{
			// normalize mouse position (x,y) with lut size
			float xx = (float)(x-m_posx) / m_width;

			// change bar position
			m_val = xx;
			return true;
		}

		// mouse was not in the slide bar
		return false;
	}

	void SlideBar::reshape(int w, int h)
	{
		m_win_width  = w;
		m_win_height = h;
	}

} // namespace
