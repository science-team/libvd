/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	Mask4D.cpp
 * @author	Herve Lombaert
 * @date	March 6th 2007
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <display/mask4d.h>

namespace libvdDisplay
{
	Mask4D::Mask4D()
	:	m_id(0),
		m_nb_frames(0),
		m_frame(0)
	{
	}

	Mask4D::~Mask4D()
	{
		if(glIsList(m_id)) glDeleteLists(m_id,m_nb_frames);
	}

	void Mask4D::setFrame(unsigned int frame)
	{
		CHECKRANGE("frame out of range", frame, 0, m_nb_frames);
		m_frame = frame;
	}


	unsigned int Mask4D::getFrame() const
	{
		return m_frame;
	}

	
	unsigned int Mask4D::getNbFrames() const
	{
		return m_nb_frames;
	}


	void Mask4D::display()
	{
		if(!glIsList(m_id+m_frame)) return;

		/* use regular glColor */
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_COLOR_MATERIAL);

		/* draw list */
		glCallList(m_id+m_frame);

		glPopAttrib();
	}

	void Mask4D::clear()
	{
		if(glIsList(m_id)) glDeleteLists(m_id,m_nb_frames);
		m_id = 0;
		m_nb_frames = 0;
		m_frame     = 0;
	}


} // namespace
