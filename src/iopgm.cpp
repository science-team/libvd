/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	iopgm.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <io/iopgm.h>

#include <fstream>
#include <cstdio>
#include <string>
#include <error.h>
#include <stdlib.h>


namespace IO
{
	void readPGM(const char* filename, unsigned char* &image, unsigned int &width, unsigned int &height)
	{
		PGM::read(filename, image, width, height);
	}
		
	
	void writePGM(const char* filename, const unsigned char* image, unsigned int width, unsigned int height)
	{
		PGM::write(filename, image, width, height);
	}
	

	void PGM::read(const char* filename, unsigned char* &image, unsigned int &width, unsigned int &height)
	{
		using namespace std;

		string buf;
		char c_buf[1024];
		ifstream file;
		bool ascii = true;

		file.open(filename, ios::binary);
		if(file.fail()) throw LOCATE(Error("opening"));

		file >> buf;
		if(buf != "P2" && buf != "P5") throw LOCATE(Error("format"));
		if(buf == "P5") ascii = false; // binary file

		file.ignore(1);
		file.getline(&c_buf[0], 1024); // ignore comment, assume there is a comment

		file >> width >> height;
		file >> buf;
		if(buf != "255") throw LOCATE(Error("format"));
		
		image = new unsigned char [width * height];

		if(!ascii) // read '\n'
		{
			unsigned char val;
			file.read((char*)&val,1);
			if(val != 0x0a) throw LOCATE(Error("wrong format"));
		}

		for(int h=height-1; h>=0; --h)
			for(unsigned int w=0; w<width; ++w)
		{
			unsigned char val = 0;
			if(ascii)
			{
				file >> buf;
				val = atoi(buf.c_str());
			}
			else
			{
				file.read((char*)(&val),1);
			}
			image[h*width+w] = val;
		}
	}


	void PGM::write(const char* filename, const unsigned char* image, unsigned int width, unsigned int height)
	{
		using namespace std;

		ofstream file;
		file.open(filename, ios::binary);
		if(file.fail()) throw LOCATE(Error("open"));

		file << "P5" << endl; // writing binary format, easier
		file << "# No Comment" << endl;
		file << width << " " << height << endl;
		file << 255;
		unsigned char val = 0x0a; // '\n'
		file.write((char*)&val,1);

		for(int h=height-1; h>=0; --h)
			file.write((char*)&image[h*width], width);

		file.close();
	}


}; // namespace


