/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	stereo.cpp
 * @author	Herve Lombaert
 * @date	March 19th 2007
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <tools/stereo.h>

namespace Tools
{
	Stereo::Stereo()
	:	m_id(0),
		m_listcreated(false),
		m_eye_shift(0.025f)
	{
		m_modelview[0]  = 1; m_modelview[1]  = 0; m_modelview[2]  = 0; m_modelview[3]  = 0;
		m_modelview[4]  = 0; m_modelview[5]  = 1; m_modelview[6]  = 0; m_modelview[7]  = 0;
		m_modelview[8]  = 0; m_modelview[9]  = 0; m_modelview[10] = 1; m_modelview[11] = 0;
		m_modelview[12] = 0; m_modelview[13] = 0; m_modelview[14] = 0; m_modelview[15] = 1;
	}

	Stereo::~Stereo()
	{
		if(m_listcreated && glIsList(m_id)) glDeleteLists(m_id,1);
	}

	void Stereo::enable()
	{
		/* get the modelview matrix, used to move the
		 * camera for the stereo view */
		glGetDoublev(GL_MODELVIEW_MATRIX, m_modelview);

		/* remove any drawing, and put a green (neutral) background */
		glClearColor(0, 0.5, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* render on the blue canal */
		glColorMask(0,0,1,1);
		
		/* check if display list has already been created */
		GLint mode;
		GLint id;
		glGetIntegerv(GL_LIST_MODE, &mode);
		glGetIntegerv(GL_LIST_INDEX, &id);
		if(glIsList(id) && (mode == GL_COMPILE || mode == GL_COMPILE_AND_EXECUTE))
		{
			m_id = id;
			m_listcreated = false;
		}
		else
		{
			if(!glIsList(m_id))
			{
				m_id = glGenLists(1);
			}

			/* define the list, all further commands will be
			 * executed, later the recorded scene can be used
			 * to generate the stereo view */
			glNewList(m_id, GL_COMPILE_AND_EXECUTE);
			m_listcreated = true;
			glCheckError();
		}

		/* render right eye */
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslated(-m_eye_shift*m_modelview[0], -+m_eye_shift*m_modelview[4], -m_eye_shift*m_modelview[8]);
	}

	void Stereo::disable()
	{
		/* stop rendering the right eye */
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		if(m_listcreated)
			glEndList();
		glCheckError();

		/* render left eye on the red canal */
		glColorMask(1,0,0,1);
		glClear(GL_DEPTH_BUFFER_BIT); // remove any previous blending
		glPushMatrix();
		glLoadMatrixd(m_modelview);
		glTranslated(+2*m_eye_shift*m_modelview[0], +2*m_eye_shift*m_modelview[4], +2*m_eye_shift*m_modelview[8]);
		glCallList(m_id);
		glPopMatrix();

		glColorMask(1,1,1,1); // reset masks
	}
};
