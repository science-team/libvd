/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	point3d.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <display/point3d.h>
#include <error.h>

namespace libvdDisplay
{
	Point3D::Point3D()
	:	m_id(0)
	{
	}


	Point3D::~Point3D()
	{
		if(glIsList(m_id)) glDeleteLists(m_id,1);
	}


	void Point3D::display()
	{
		if(!glIsList(m_id)) return;

		/* draw list */
		glCallList(m_id);
	}


	void Point3D::update(const float* points,
	                     unsigned int nbPoints)
	{
		if(nbPoints && !points) throw LOCATE(Error("invalid list of points"));
		/* Create a new display list */
		if(glIsList(m_id)) glDeleteLists(m_id,1);
		m_id = glGenLists(1); glCheckError();
		glNewList(m_id, GL_COMPILE); glCheckError();

		/* Display list of points */
		for(unsigned int i=0; i<nbPoints; ++i)
			display(&points[i*3]);

		/* End list */
		glEndList();
	}


	void Point3D::display(const float* point)
	{
		// list of face points and normals
		// 4 first triplets are clockwise face point
		// last triplet is the face normal
		float offset[6][5][3] = {
		{	{-0.5,+0.5,+0.5}, // 74033047
			{-0.5,-0.5,+0.5},
			{-0.5,-0.5,-0.5},
			{-0.5,+0.5,-0.5},
			{-1,0,0}	},
		{	{+0.5,-0.5,+0.5}, // 56211265
			{+0.5,+0.5,+0.5},
			{+0.5,+0.5,-0.5},
			{+0.5,-0.5,-0.5},
			{+1,0,0}	},
		{	{-0.5,-0.5,+0.5}, // 45100154
			{+0.5,-0.5,+0.5},
			{+0.5,-0.5,-0.5},
			{-0.5,-0.5,-0.5},
			{0,-1,0}	},
		{	{+0.5,+0.5,+0.5}, // 67322376
			{-0.5,+0.5,+0.5},
			{-0.5,+0.5,-0.5},
			{+0.5,+0.5,-0.5},
			{0,+1,0}	},
		{	{-0.5,-0.5,-0.5}, // 01233210
			{+0.5,-0.5,-0.5},
			{+0.5,+0.5,-0.5},
			{-0.5,+0.5,-0.5},
			{0,0,-1}	},
		{	{-0.5,+0.5,+0.5}, // 76544567
			{+0.5,+0.5,+0.5},
			{+0.5,-0.5,+0.5},
			{-0.5,-0.5,+0.5},
			{0,0,+1}	}	};
  
		/* Translate in world space */
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(point[0], point[1], point[2]);

		/* Lighting helps visualization of the mesh */
		/* Nice coloring for the mesh */
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);

		//glEnable(GL_LIGHTING); // lignthing should be enabled outside
		//glEnable(GL_LIGHT0);

		/* start lists of quads */
		glBegin(GL_QUADS);

		for(unsigned int m=0; m<6; ++m)
		{
			glNormal3f(offset[m][4][0], offset[m][4][1], offset[m][4][2]);
			glVertex3f(offset[m][0][0], offset[m][0][1], offset[m][0][2]);
			glVertex3f(offset[m][1][0], offset[m][1][1], offset[m][1][2]);
			glVertex3f(offset[m][2][0], offset[m][2][1], offset[m][2][2]);
			glVertex3f(offset[m][3][0], offset[m][3][1], offset[m][3][2]);
		}

		glEnd();

		glPopMatrix();

		/* Leaving function */
		//glDisable(GL_LIGHT0);
		//glDisable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);
  	}

} // namespace
