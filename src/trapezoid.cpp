/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	trapezoid.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/transferfunction/trapezoid.h>
#include <cmath>

namespace Tools { namespace TransferFunction
{
	Trapezoid::Trapezoid()
	:	m_low0(0),
		m_low1(0),
		m_high0(0),
		m_high1(0),
		m_opacity(1),
		m_left(0),
		m_right(0),
		m_low0_u(0),
		m_low1_u(0),
		m_high0_u(0),
		m_high1_u(0),
		m_aspectratio(1),
		m_feature(NONE)
	{
		// black/white trapezoid by default
		m_low0_color[0] = m_low0_color[1] = m_low0_color[2] = 0;
		m_low1_color[0] = m_low1_color[1] = m_low1_color[2] = 0;
		m_high0_color[0] = m_high0_color[1] = m_high0_color[2] = 1;
		m_high1_color[0] = m_high1_color[1] = m_high1_color[2] = 1;
		m_middle0_color[0] = m_middle0_color[1] = m_middle0_color[2] = 0.5;
		m_middle1_color[0] = m_middle1_color[1] = m_middle1_color[2] = 0.5;
	}


	Trapezoid::Trapezoid(float low0, float high0, float high1, float low1, float opacity)
	:	m_low0(low0),
		m_low1(low1),
		m_high0(high0),
		m_high1(high1),
		m_opacity(opacity),
		m_left(0),
		m_right(0),
		m_low0_u(0),
		m_low1_u(0),
		m_high0_u(0),
		m_high1_u(0),
		m_aspectratio(1),
		m_feature(NONE)
	{
		// black/white trapezoid by default
		m_low0_color[0] = m_low0_color[1] = m_low0_color[2] = 0;
		m_low1_color[0] = m_low1_color[1] = m_low1_color[2] = 0;
		m_high0_color[0] = m_high0_color[1] = m_high0_color[2] = 1;
		m_high1_color[0] = m_high1_color[1] = m_high1_color[2] = 1;
		m_middle0_color[0] = m_middle0_color[1] = m_middle0_color[2] = 0.5;
		m_middle1_color[0] = m_middle1_color[1] = m_middle1_color[2] = 0.5;
	}


	Trapezoid::~Trapezoid()
	{}


	void Trapezoid::update(float low0, float high0, float high1, float low1, float opacity)
	{
		/* update ramp */
		m_low0 = low0;
		m_low1 = low1;
		m_high0 = high0;
		m_high1 = high1;
		m_opacity = opacity;

		/* update ramp position for its visualization */
		m_low0_u  = (m_low0-m_left)  / (m_right-m_left);
		m_low1_u  = (m_low1-m_left)  / (m_right-m_left);
		m_high0_u = (m_high0-m_left) / (m_right-m_left);
		m_high1_u = (m_high1-m_left) / (m_right-m_left);
	}


	void Trapezoid::clamp(float left, float right)
	{
		/* update visualization limits */
		m_left  = left;
		m_right = right;

		/* update ramp position for its visualization */
		m_low0_u  = (m_low0-m_left)  / (m_right-m_left);
		m_low1_u  = (m_low1-m_left)  / (m_right-m_left);
		m_high0_u = (m_high0-m_left) / (m_right-m_left);
		m_high1_u = (m_high1-m_left) / (m_right-m_left);
	}


	void Trapezoid::display() const
	{
		/* draw ramp */
		glColor3f(1,0,0);

		glBegin(GL_LINE_STRIP);

		/* display low0 */
		if(m_low0_u>=0 && m_low0_u<=1)
		{
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(m_low0_u-0.5f,-0.5f,0);
		}

		else if(m_low0_u<0 && m_high0_u>=0)
		{
			float intersec = - m_low0_u / (m_high0_u-m_low0_u);
			glVertex3f(-0.5f,intersec*m_opacity-0.5f,0);
		}
		else if(m_high1_u<0 && m_low1_u>0)
		{
			float intersec = m_low1_u / (m_low1_u-m_high1_u);
			glVertex3f(-0.5f,intersec*m_opacity-0.5f,0);
		}

		/* display high0 */
		if(m_high0_u>=0 && m_high0_u<=1)
			glVertex3f(m_high0_u-0.5f,m_opacity-0.5f,0);

		else if(m_high0_u<0 && m_high1_u>=0)
			glVertex3f(-0.5f,m_opacity-0.5f,0);

		/* display high1 */
		if(m_high1_u>=0 && m_high1_u<=1)
			glVertex3f(m_high1_u-0.5f,m_opacity-0.5f,0);

		else if(m_high1_u>1 && m_high0_u<=1)
			glVertex3f(+0.5f,m_opacity-0.5f,0);

		/* display low1 */
		if(m_low1_u>=0 && m_low1_u<=1)
		{
			glVertex3f(m_low1_u-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
		}

		else if(m_high1_u<=1 && m_low1_u>1)
		{
			float intersec = (m_low1_u-1)/ (m_low1_u-m_high1_u);
			glVertex3f(+0.5f,intersec*m_opacity-0.5f,0);
		}
		else if(m_low0_u<1 && m_high0_u>1)
		{
			float intersec = (1-m_low0_u) / (m_high0_u-m_low0_u);
			glVertex3f(+0.5f,intersec*m_opacity-0.5f,0);
		}

		/* flat ramp if trapezoid is not seen */
		if((m_low0_u<0 && m_low1_u<0) || (m_low0_u>1 && m_low1_u>1))
		{
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
		}

		glEnd();

		float scale = 0.05f; // square size

		/* lower square of first ramp */
		if(m_low0_u>=0 && m_low0_u<=1)
		{
			glColor3f(m_low0_color[0], m_low0_color[1], m_low0_color[2]);

			glPushMatrix();
			glTranslatef(m_low0_u-0.5f,-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* higher square of first ramp */
		if(m_high0_u>=0 && m_high0_u<=1)
		{
			glColor3f(m_high0_color[0], m_high0_color[1], m_high0_color[2]);

			glPushMatrix();
			glTranslatef(m_high0_u-0.5f,m_opacity-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* center square of first ramp */
		if((m_low0_u+m_high0_u)/2>=0 && (m_low0_u+m_high0_u)/2<=1)
		{
			glColor3f(m_middle0_color[0], m_middle0_color[1], m_middle0_color[2]);

			glPushMatrix();
			glTranslatef((m_low0_u+m_high0_u)/2-0.5f,m_opacity/2-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* lower square of second ramp */
		if(m_low1_u>=0 && m_low1_u<=1)
		{
			glColor3f(m_low1_color[0], m_low1_color[1], m_low1_color[2]);

			glPushMatrix();
			glTranslatef(m_low1_u-0.5f,-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* higher square of second ramp */
		if(m_high1_u>=0 && m_high1_u<=1)
		{
			glColor3f(m_high1_color[0], m_high1_color[1], m_high1_color[2]);

			glPushMatrix();
			glTranslatef(m_high1_u-0.5f,m_opacity-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}

		/* center square of second ramp */
		if((m_low1_u+m_high1_u)/2>=0 && (m_low1_u+m_high1_u)/2<=1)
		{
			glColor3f(m_middle1_color[0], m_middle1_color[1], m_middle1_color[2]);

			glPushMatrix();
			glTranslatef((m_low1_u+m_high1_u)/2-0.5f,m_opacity/2-0.5f,0);
			glScalef(scale/m_aspectratio, scale, scale);

			glBegin(GL_QUADS);
			glVertex3f(-0.5f,-0.5f,0);
			glVertex3f(+0.5f,-0.5f,0);
			glVertex3f(+0.5f,+0.5f,0);
			glVertex3f(-0.5f,+0.5f,0);
			glEnd();

			glPopMatrix();
		}
	}


	void Trapezoid::reshape(int w, int h)
	{
		m_aspectratio = (float)w/h;
	}


	bool Trapezoid::mouse(float x, float y)
	{
		float tolerance = 0.1f;

		// NB: mouse coordinates are normalized (0..1)

		/* assume no feature is selected */
		m_feature = NONE;

		if(x>=0 && x<=1 && y>=0 && y<=1)
		{
			/* lower corner of first ramp */
			if(fabs(x-m_low0_u) < 0.1 && fabs(y-0) < tolerance)
				m_feature = LOW0;

			/* middle of first ramp */
			else if(fabs(x-(m_low0_u+m_high0_u)/2) < 0.1 && fabs(y-m_opacity/2) < tolerance)
				m_feature = MIDDLE0;

			/* higher corner of first ramp */
			else if(fabs(x-m_high0_u) < 0.1 && fabs(y-m_opacity) < tolerance)
				m_feature = HIGH0;

			/* lower corner of second ramp */
			else if(fabs(x-m_low1_u) < 0.1 && fabs(y-0) < tolerance)
				m_feature = LOW1;

			/* middle of second ramp */
			else if(fabs(x-(m_low1_u+m_high1_u)/2) < 0.1 && fabs(y-m_opacity/2) < tolerance)
				m_feature = MIDDLE1;

			/* higher corner of second ramp */
			else if(fabs(x-m_high1_u) < 0.1 && fabs(y-m_opacity) < tolerance)
				m_feature = HIGH1;
		}

		/* if a feature is selected, return true */
		if(m_feature != NONE) return true;
		else                  return false;
	}


	bool Trapezoid::motion(float x, float y)
	{
		// NB: mouse coordinates are normalized (0..1)

		bool eventCaught = false;

		/* what intensity is x */
		float i = x*(m_right-m_left) + m_left;

		/* change feature with new position */
		switch(m_feature)
		{
		case LOW0: // move lower corner of first ramp
			{
				if(i>=m_high0) i = m_high0-1; // avoid inverse ramp (not handled in this code)
				update(i,m_high0,m_high1,m_low1,m_opacity);
				eventCaught = true;
				break;
			}
		case HIGH0: // move higher corner of the first ramp
			{
				if(i<=m_low0) i = m_low0+1; // avoid inverse ramp (not handled in this code)
				if(i>=m_high1) i = m_high1-1; // avoid inverse ramp (not handled in this code)
				if(y>=0 && y<=1) m_opacity = y; // opacity is between 0 and 1
				update(m_low0,i,m_high1,m_low1,m_opacity);
				eventCaught = true;
				break;
			}
		case HIGH1: // move higher corner of the second ramp
			{
				if(i>=m_low1) i = m_low1-1; // avoid inverse ramp (not handled in this code)
				if(i<=m_high0) i = m_high0+1; // avoid inverse ramp (not handled in this code)
				if(y>=0 && y<=1) m_opacity = y; // opacity is between 0 and 1
				update(m_low0,m_high0,i,m_low1,m_opacity);
				eventCaught = true;
				break;
			}
		case LOW1: // move higher corner of the second ramp
			{
				if(i<=m_high1) i = m_high1+1; // avoid inverse ramp (not handled in this code)
				update(m_low0,m_high0,m_high1,i,m_opacity);
				eventCaught = true;
				break;
			}
		case MIDDLE0: // translate first ramp
			{
				// i is the new ramp center
				float hwindow = (m_high0-m_low0)/2;
				if(i+hwindow>=m_high1)
					update(m_high1-2*hwindow-1, m_high1-1, m_high1, m_low1,m_opacity);
				else
					update(i-hwindow, i+hwindow, m_high1, m_low1,m_opacity);
				eventCaught = true;
				break;
			}
		case MIDDLE1: // translate second ramp
			{
				// i is the new ramp center
				float hwindow = (m_low1-m_high1)/2;
				if(i-hwindow<=m_high0)
					update(m_low0, m_high0, m_high0+1, m_high0+2*hwindow+1,m_opacity);
				else
					update(m_low0, m_high0, i-hwindow, i+hwindow,m_opacity);
				eventCaught = true;
				break;
			}
		default: break;
		}

		return eventCaught;
	}


	void Trapezoid::get(float i, float &r, float &g, float &b, float &a) const
	{
		/* lower flat portion of trapezoid */
		if(i <= m_low0 || i >= m_low1)
		{
			r=0, g=0, b=0, a=0;
		}
		/* higher flat portion of trapezoid */
		else if(i >= m_high0 && i <= m_high1)
		{
			float v = (i-m_high0) / (m_high1-m_high0);
			r = (1-v)*m_high0_color[0] + (v)*m_high1_color[0];
			g = (1-v)*m_high0_color[1] + (v)*m_high1_color[1];
			b = (1-v)*m_high0_color[2] + (v)*m_high1_color[2];
			a = m_opacity;
		}
		/* on the first ramp */
		else if(i > m_low0 && i < (m_low0+m_high0)/2)
		{
			float v = 2 * (i-m_low0) / (m_high0-m_low0);
			r = (1-v)*m_low0_color[0] + (v)*m_middle0_color[0];
			g = (1-v)*m_low0_color[1] + (v)*m_middle0_color[1];
			b = (1-v)*m_low0_color[2] + (v)*m_middle0_color[2];
			a = v/2 * m_opacity;
		}
		else if(i >= (m_low0+m_high0)/2 && i < m_high1)
		{
			float v = 2 * ((i-m_low0) / (m_high0-m_low0) - 0.5f);
			r = (1-v)*m_middle0_color[0] + (v)*m_high0_color[0];
			g = (1-v)*m_middle0_color[1] + (v)*m_high0_color[1];
			b = (1-v)*m_middle0_color[2] + (v)*m_high0_color[2];
			a = (v/2+0.5f) * m_opacity;
		}
		/* on the second ramp */
		else if(i > m_high1 && i <= (m_low1+m_high1)/2)
		{
			float v = 2 * (i-m_high1) / (m_low1-m_high1);
			r = (v)*m_high1_color[0] + (1-v)*m_middle1_color[0];
			g = (v)*m_high1_color[1] + (1-v)*m_middle1_color[1];
			b = (v)*m_high1_color[2] + (1-v)*m_middle1_color[2];
			a = v/2 * m_opacity;
		}
		else if(i > m_high1 && i < m_low1)
		{
			float v = 2 * ((i-m_high1) / (m_low1-m_high1) - 0.5f);
			r = (v)*m_middle1_color[0] + (1-v)*m_low1_color[0];
			g = (v)*m_middle1_color[1] + (1-v)*m_low1_color[1];
			b = (v)*m_middle1_color[2] + (1-v)*m_low1_color[2];
			a = (v/2+0.5f) * m_opacity;
		}
	}


	void Trapezoid::setColor(float r, float g, float b)
	{
		switch(m_feature)
		{
		case LOW0:
			m_low0_color[0] = r,
			m_low0_color[1] = g,
			m_low0_color[2] = b;
			break;
		case LOW1:
			m_low1_color[0] = r,
			m_low1_color[1] = g,
			m_low1_color[2] = b;
			break;
		case HIGH0:
			m_high0_color[0] = r,
			m_high0_color[1] = g,
			m_high0_color[2] = b;
			break;
		case HIGH1:
			m_high1_color[0] = r,
			m_high1_color[1] = g,
			m_high1_color[2] = b;
			break;
		case MIDDLE0:
			m_middle0_color[0] = r,
			m_middle0_color[1] = g,
			m_middle0_color[2] = b;
			break;
		case MIDDLE1:
			m_middle1_color[0] = r,
			m_middle1_color[1] = g,
			m_middle1_color[2] = b;
			break;
		default: break;
		}
	}

}} // namespace
