/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	displayhistogram.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include "msvc/GLee.h"
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/transferfunction/displayhistogram.h>

namespace Tools { namespace TransferFunction
{
	DisplayHistogram::DisplayHistogram()
	:	m_histogram(NULL),
		m_nbBins(0),
		m_min(0),
		m_max(0),
		m_left(0),
		m_right(0),
		m_height(0),
		m_maxheight(0),
		m_selected(0),
		m_lasty(0),
		m_textureName(0)
	{}


	DisplayHistogram::~DisplayHistogram()
	{
		/* free some memory */
		if(m_nbBins>0) delete [] m_histogram;
		if(glIsTexture(m_textureName))
			glDeleteTextures(1, &m_textureName);
	}


	void DisplayHistogram::clamp(float left, float right)
	{
		/* update left and right value of visible histogram */
		m_left  = left;
		m_right = right;

		/* what is the maximum height in this section */
		float binSize = (m_max-m_min) / m_nbBins;
		int leftbin  = static_cast<int>((left-m_min) / binSize);
		int rightbin = static_cast<int>((right-m_min) / binSize);
		if(leftbin<0) leftbin = 0;
		if(rightbin>=(int)m_nbBins) rightbin = m_nbBins-1;
		m_height = 0;

		/* scan the histogram and find the highest bin */
		for(int i=leftbin; i<rightbin; ++i)
		{
			if(m_histogram[i] > m_height)
				m_height = (float)m_histogram[i]; // update highest visible bin
		}
	}


	void DisplayHistogram::updateTexture(unsigned int width, unsigned int height, float r, float g, float b, float a)
	{
		/* create new texture */
		if(!glIsTexture(m_textureName)) glGenTextures(1, &m_textureName);
		float* texture = new float [width*height*4]; // RGBA
		for(unsigned int i=0; i<width*height; ++i)
			texture[i*4+0] = 0,
			texture[i*4+1] = 0,
			texture[i*4+2] = 0,
			texture[i*4+3] = 0;

		/* fill texture */
		for(unsigned int i=0; i<width; ++i)
		{
			/* what bin */
			unsigned int bin = static_cast<int>((m_nbBins-1)*i/width);
			CHECKRANGE("bin index", bin, 0, m_nbBins);

			/* whats the bin height */
			unsigned int max = static_cast<int>((height-1)*m_histogram[bin]/m_maxheight);
			CHECKRANGE("bin height", max, 0, height);

			/* fill bin */
			for(unsigned int j=0; j<max; ++j)
				texture[(j*width+i)*4+0] = r,
				texture[(j*width+i)*4+1] = g,
				texture[(j*width+i)*4+2] = b,
				texture[(j*width+i)*4+3] = a;
		}

		/* send texture to graphic card */
		glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D,
				 0,
				 GL_RGBA,
				 width,
				 height,
				 0,
				 GL_RGBA,
				 GL_FLOAT,
				 texture);
		glCheckError();

		/* free texture now (it is now on graphic card) */
		delete [] texture;
	}


	void DisplayHistogram::display() const
	{
		if(m_histogram)
		{
			// texture coordinate of left, right, top limits
			float u0 = (m_left-m_min)/(m_max-m_min);
			float u1 = (m_right-m_min)/(m_max-m_min);
			float v1 = m_height / m_maxheight;
	
			glEnable(GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();
	
			glColor3f(1,1,1);
	
			glBegin(GL_QUADS);
			glTexCoord2f(u0,0);  glVertex3f(-0.5,-0.5,0);
			glTexCoord2f(u1,0);  glVertex3f(+0.5,-0.5,0);
			glTexCoord2f(u1,v1); glVertex3f(+0.5,+0.5,0);
			glTexCoord2f(u0,v1); glVertex3f(-0.5,+0.5,0);
			glEnd();
	
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		}
	}


	bool DisplayHistogram::mouse(float x, float y)
	{
		// NB: mouse coordinates are normalized (0..1)

		m_lastx = x;
		m_lasty = y;

		if(x>=0 && x<=1 && y>=0 && y<=1)
			m_selected = true;
		else
			m_selected = false;

		return m_selected;
	}


	bool DisplayHistogram::motion(float x, float y)
	{
		// NB: mouse coordinates are normalized (0..1)

		if(m_selected)
		{
				// what is the intensity at mouse position x
			float lasti = m_lastx*(m_right-m_left) + m_left;
			float i     =       x*(m_right-m_left) + m_left;
		
			// change visible span of histogram with vertical movement
			float dspan = (m_lasty-y)*(m_max-m_min);
		
			// i is the new histogram center
			float di = lasti - i;
			if(m_left+di-dspan/2 < m_right+di+dspan/2)
				clamp(m_left+di-dspan/2, m_right+di+dspan/2);
		
			// update last mouse coordinate
			m_lastx = x;
			m_lasty = y;
		}

		return m_selected;
	}


}} // namespace
