/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	display2d.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <display/display2d.h>

//////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////

namespace libvdDisplay
{

	Image::Image()
	:	m_textureName(0)
	{
	}


	Image::~Image()
	{
		// If this is called after the display has been destroyed, this call will be undefined
		if(glIsTexture(m_textureName))
			glDeleteTextures(1, &m_textureName);
	}


	void Image::display() const
	{
		if(glIsTexture(m_textureName))
		{
			// Draw texture
			glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();
			glColor3f(1,1,1);

			glEnable(GL_TEXTURE_2D);

			glBegin(GL_QUADS);
			glTexCoord2f(0,0); glVertex3f(-0.5,-0.5,0);
			glTexCoord2f(1,0); glVertex3f(+0.5,-0.5,0);
			glTexCoord2f(1,1); glVertex3f(+0.5,+0.5,0);
			glTexCoord2f(0,1); glVertex3f(-0.5,+0.5,0);
			glEnd();

			glDisable(GL_TEXTURE_2D);
		}
	}


	// specialization for image type
	template <> GLenum Image::OpenGLArgs<unsigned char> ::type = GL_UNSIGNED_BYTE;
	template <> GLenum Image::OpenGLArgs<unsigned short>::type = GL_UNSIGNED_SHORT;
	template <> GLenum Image::OpenGLArgs<unsigned int>  ::type = GL_UNSIGNED_INT;
	template <> GLenum Image::OpenGLArgs<char>          ::type = GL_BYTE;
	template <> GLenum Image::OpenGLArgs<short>         ::type = GL_SHORT;
	template <> GLenum Image::OpenGLArgs<int>           ::type = GL_INT;
	template <> GLenum Image::OpenGLArgs<float>         ::type = GL_FLOAT;
	template <> GLenum Image::OpenGLArgs<double>        ::type = GL_FLOAT;


} // namespace

