/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	displaycolor.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <tools/transferfunction/displaycolor.h>

namespace Tools { namespace TransferFunction
{
	DisplayColor::DisplayColor()
	:	m_textureName(0),
		m_mark_r(0),
		m_mark_g(0),
		m_mark_b(0),
		m_mark_u(0),
		m_mark_v(0)
	{
	}


	DisplayColor::~DisplayColor()
	{
		if(glIsTexture(m_textureName))
			glDeleteTextures(1, &m_textureName);
	}


	void DisplayColor::display() const
	{
		if(!glIsTexture(m_textureName))
			initTexture();

		/* display colors */
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();

		glColor3f(1,1,1);
		glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-0.5,-0.5,0);
		glTexCoord2f(1,0); glVertex3f(+0.5,-0.5,0);
		glTexCoord2f(1,1); glVertex3f(+0.5,+0.5,0);
		glTexCoord2f(0,1); glVertex3f(-0.5,+0.5,0);
		glEnd();

		glDisable(GL_TEXTURE_2D);

		/* display border */
		glColor3f(1,1,1);
		glBegin(GL_LINE_LOOP);
		glVertex3f(-0.5,-0.5,0);
		glVertex3f(+0.5,-0.5,0);
		glVertex3f(+0.5,+0.5,0);
		glVertex3f(-0.5,+0.5,0);
		glEnd();

		/* display mark */
		float crosssize = 0.1f;
		glColor3f(1,0,0);
		glBegin(GL_LINES);
		glVertex3f(m_mark_u-crosssize-0.5f,m_mark_v-0.5f,0);
		glVertex3f(m_mark_u+crosssize-0.5f,m_mark_v-0.5f,0);
		glVertex3f(m_mark_u-0.5f,m_mark_v-crosssize-0.5f,0);
		glVertex3f(m_mark_u-0.5f,m_mark_v+crosssize-0.5f,0);
		glEnd();
	}


	void DisplayColor::getRGB(float x, float y, float &r, float &g, float &b) const
	{
		r=g=b=0;
		/* if in transition from R to G */
		if(x>=0 && x<=1.0f/6)
			r = 1,
			g = (x)*6,
			b = 0;
		if(x>1.0f/6 && x<=2.0f/6)
			r = 1-(x-1.0f/6)*6,
			g = 1,
			b = 0;

		/* if in transition from G to B */
		if(x>2.0f/6 && x<=3.0f/6)
			r = 0,
			g = 1,
			b = (x-2.0f/6)*6;
		if(x>3.0f/6 && x<=4.0f/6)
			r = 0,
			g = 1-(x-3.0f/6)*6,
			b = 1;

		/* if in transition from B to R */
		if(x>4.0f/6 && x<=5.0f/6)
			r = (x-4.0f/6)*6,
			g = 0,
			b = 1;
		if(x>5.0f/6 && x<=1)
			r = 1,
			g = 0,
			b = 1-(x-5.0f/6)*6;

		/* saturate to white */
		if(y>0.5)
		{
			float i = (1-y)/0.5f;
			r = i*r + (1-i)*1;
			g = i*g + (1-i)*1;
			b = i*b + (1-i)*1;
		}
		
		/* saturate to black */
		if(y<=0.5)
		{
			float i = y/0.5f;
			r = i*r + (1-i)*0;
			g = i*g + (1-i)*0;
			b = i*b + (1-i)*0;
		}
	}


	void DisplayColor::mark(float r, float g, float b)
	{
		m_mark_r = r;
		m_mark_g = g;
		m_mark_b = b;

		m_mark_u = 0; // is Hue (H)
		m_mark_v = 0; // is intensity (V)

		float max = r; if(g>max) max = g; if(b>max) max = b;
		float min = r; if(g<min) min = g; if(b<min) min = b;

		// Hue and Intensity
		float v = (max<1) ? max/2 : 0.5f+min/2;
		float h = 0;

		if(max-min != 0) // it is possible Hue is undefined, just set it to 0
		{
			if(max == r)
			{
				h = 1.0f/6 * (g-b)/(max-min);
			}
			else if(max == g)
			{
				h = 1.0f/3 + 1.0f/6 * (b-r)/(max-min);
			}
			else if(max == b)
			{
				h = 2.0f/3 + 1.0f/6 * (r-g)/(max-min);
			}
		
			if(h<0) h = h + 1; // Hue is circular, if negative, just add a full circle (+1)
		}

		m_mark_u = h;
		m_mark_v = v;
	}


	void DisplayColor::initTexture() const
	{
		/* texture size */
		unsigned int width = 128;
		unsigned int height = 128;

		/* delete and create new texture */
		if(!glIsTexture(m_textureName)) { glGenTextures(1, &m_textureName); glCheckError(); }
		float* texture = new float [width*height*4]; // RGBA
		for(unsigned int i=0; i<width*height; ++i)
			texture[i*4+0] = 0,
			texture[i*4+1] = 0,
			texture[i*4+2] = 0,
			texture[i*4+3] = 1;

		/* fill color map */
		for(unsigned int j=0; j<128; ++j)
			for(unsigned int i=0; i<128; ++i)
		{
			float r,g,b;
			getRGB(i/128.0f, j/128.0f, r,g,b);

			texture[(j*128+i)*4+0] = r;
			texture[(j*128+i)*4+1] = g;
			texture[(j*128+i)*4+2] = b;
			texture[(j*128+i)*4+3] = 1;
		}

		/* send texture to graphic card */
		glBindTexture(GL_TEXTURE_2D, m_textureName); glCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D,
				 0,
				 GL_RGBA,
				 width,
				 height,
				 0,
				 GL_RGBA,
				 GL_FLOAT,
				 texture);
		glCheckError();

		/* free texture now (it is now on graphic card) */
		delete [] texture;
	}

}} // namespace
