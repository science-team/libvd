/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	mask3d.cpp
 * @author	Herve Lombaert
 * @date	January 17th 2006
 */

#ifdef WIN32
#include <windows.h>
#include <GL/gl.h>
#elif __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <display/mask3d.h>

namespace libvdDisplay
{
	Mask3D::Mask3D()
	:	m_id(0)
	{
	}

	Mask3D::~Mask3D()
	{
		if(glIsList(m_id)) glDeleteLists(m_id,1);
	}

	void Mask3D::display()
	{
		if(!glIsList(m_id)) return;

		/* use regular glColor */
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_COLOR_MATERIAL);

		/* draw list */
		glCallList(m_id);

		glPopAttrib();
	}

	void Mask3D::clear()
	{
		if(glIsList(m_id)) glDeleteLists(m_id,1);
		m_id = 0;
	}

} // namespace
