/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ioraw.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 *
 * Read and write raw data in file
 * Currently implements only 8 bit black and white 2D and 3D images
 */


/**
 * @namespace IO
 * Contains all class and functions related to file reading and writing
 */


#ifndef IO_RAW_H
#define IO_RAW_H


namespace IO
{
	/** Read a N-D image */
	template <typename DataType>
	void readRaw(const char* filename, DataType* &image, unsigned int size);
		

	/** Write a N-D image */
	template <typename DataType>
	void writeRaw(const char* filename, const DataType* image, unsigned int size);
	

	/** reader for 2D black and white images */
	class Raw
	{
	public:
		/// read a N-D raw image
		template <typename DataType>
		static void read(const char* filename, DataType* &image,  unsigned int size);

		/// write a N-D raw image
		template <typename DataType>
		static void write(const char* filename, const DataType* image, unsigned int size);
	};

} // namespace	


#include <fstream>
#include <cstdio>
#include <string>
#include <error.h>


#ifdef __APPLE__
	template <typename T> inline void endian_swap(T& x) { x = x; } // default behavior
	template <> inline void endian_swap<short>(short& x) { x = (x>>8) | (x<<8); }
	template <> inline void endian_swap<unsigned short>(unsigned short& x) { x = (x>>8) | (x<<8); }
	template <> inline void endian_swap<int>(int& i) { i = ((i&0xff000000)>>24) | (((i&0x00ff0000)>>16)<<8) | (((i&0x0000ff00)>>8)<<16) | ((i&0x000000ff)<<24); }
	template <> inline void endian_swap<unsigned int>(unsigned int& i) { i = ((i&0xff000000)>>24) | (((i&0x00ff0000)>>16)<<8) | (((i&0x0000ff00)>>8)<<16) | ((i&0x000000ff)<<24); }
#endif


namespace IO
{
	template <typename DataType>
	static void readRaw(const char* filename, DataType* &image, unsigned int size)
	{
		Raw::read(filename, image, size);
	}
		
	
	template <typename DataType>
	static void writeRaw(const char* filename, const DataType* image, unsigned int size)
	{
		Raw::write(filename, image, size);
	}


	template <typename DataType>
	void Raw::read(const char* filename, DataType* &image, unsigned int size)
	{
		using namespace std;

		// Read data content
		ifstream file;

		file.open(filename, ios::binary);
		if(file.fail()) throw LOCATE(Error("open: %s", filename));

		image = new DataType [size];

		file.read((char*)image, sizeof(DataType)*size);

		file.close();

#ifdef __APPLE__
		for(unsigned int i=0; i<size; ++i)
			endian_swap(image[i]);
#endif
	}


	template <typename DataType>
	void Raw::write(const char* filename, const DataType* image, unsigned int size)
	{
		using namespace std;

		// Write data content
		ofstream file;
		file.open(filename, ios::binary);
		if(file.fail()) throw LOCATE(Error("open: %s", filename));

#if __APPLE__
		for(unsigned int i=0; i<size; ++i)
		{
			DataType p = image[i];
			endian_swap(p);
			file.write((char*)p, sizeof(DataType));
		}
#else
		file.write((char*)image, sizeof(DataType)*size);
#endif

		file.close();
	}


}; // namespace


#endif
