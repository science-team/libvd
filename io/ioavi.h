/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ioavi.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 *
 * This namespace provides some functions to read an AVI file
 *
 * The AVI needs to be uncompressed 24 bit BMP images
 *
 * It reads either a single frame in a 2D image, or the whole movie
 * in a 3D image
 */


#ifndef IO_AVI_H
#define IO_AVI_H



#include <color.h>
#include <cstdio>


namespace IO
{
	/** read a AVI frame of the movie in a 2D image */
	void readAVIFrame(const char* filename,
	                  color<unsigned char>* &frame,
	                  unsigned int &frame_width,
	                  unsigned int &frame_height,
	                  unsigned int frame_nb);
			
	/** read the whole AVI movie in a 3D image */
	void readAVI(const char* filename,
	             color<unsigned char>* &image,
	             unsigned int &frame_width,
	             unsigned int &frame_height,
	             unsigned int &nb_frames);

	/** AVI reader */
	class AVI
	{
	public:
		/** constructor */
		AVI(const char* filename=0);
		~AVI();

		/** open an avi file */
		void open(const char* filename);

		/** close file */
		void close();
	
		/** returns the number of frames in the movie */
		unsigned int getNbFrames();

		/** returns the image size */
		void getFrameSize(unsigned int &width, unsigned int &height);

		/** read a single frame in a 2D image */
		void read(color<unsigned char>* &image, unsigned int frame);

		/** read the whole movie in a 3D image */
		void read(color<unsigned char>* &image);

		/** read part of the movie in a 3D image (including start & end frames) */
		void read(color<unsigned char>* &image, unsigned int start, unsigned int end);
	
	private:
		void readHeader();
	
		struct header { ///< AVI header
			unsigned int time_delay;            // time delay between frames in microseconds
			unsigned int avi_data_rate;         // data rate of AVI data
			unsigned int padding;               // padding multiple size, typically 2048
			unsigned int parameter_flags;       // parameter flags
			unsigned int number_frames;         // number of video frames
			unsigned int number_preview_frames; // number of preview frames
			unsigned int number_streams;        // number of data streams (1 or 2)
			unsigned int buffer_size;           // suggested playback buffer size in bytes
			unsigned int width;                 // width of video image in pixels
			unsigned int height;                // height of video image in pixels
			unsigned int time_scale;            // time scale, typically 30
			unsigned int data_rate;             // data rate (frame rate = data rate / time scale)
			unsigned int starting_time;         // starting time, typically 0
			unsigned int avi_size;              // size of AVI data chunk in time scale units
		} m_header;
		
		FILE*  m_fd;
	};	
};


#endif
