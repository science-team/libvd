/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	ioscreenshot.h
 * @author	Herve Lombaert
 * @date	Tue Feb 5 2007
 *
 * Read the OpenGL RGB buffer and store its content into a RGB image
 * This can be saved into a BMP file
 * @see IO::BMP::write
 */


#ifndef IO_SCREENSHOT_H
#define IO_SCREENSHOT_H


#include <color.h>


namespace IO
{
	/** Read OpenGL buffer and create an raw 24bit image */
	void glScreenshot(color<unsigned char>* &image, unsigned int &width, unsigned int &height);
		
} // namespace


#endif
