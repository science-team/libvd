/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	iobmp.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 *
 * Read and write a BMP file in a 2D image. BMP need to be raw 24 bit data
 */


#ifndef IO_BMP_H
#define IO_BMP_H


#include <color.h>


namespace IO
{
	/** Read a 2D image from a BMP file */
	void readBMP(const char* filename, color<unsigned char>* &image, unsigned int &width, unsigned int &height);
		
	
	/** Write a 2D image in a BMP file */
	void writeBMP(const char* filename, const color<unsigned char>* image, unsigned int width, unsigned int height);
	
	
	/** BMP reader */
	class BMP { // implements IO for 2D color image
	public:
		static void read(const char* filename, color<unsigned char>* &image, unsigned int &width, unsigned int &height); ///< read a 2D BMP image
		static void write(const char* filename, const color<unsigned char>* image, unsigned int width, unsigned int height); ///< write a 2D BMP image
	};


} // namespace


#endif
