/*****************************************************************************
 * Copyright (C) 2007 Herve Lombaert <herve.lombaert@polymtl.ca>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 ****************************************************************************/


/**
 * @file	iopgm.h
 * @author	Herve Lombaert
 * @date	Tue Jun 6 2006
 *
 * Read and write 2D image from ascii or binary PGM files
 *
 * Images must be of type Image<unsigned char,2>
 */


#ifndef IO_PGM_H
#define IO_PGM_H


namespace IO
{
	/** Read a 2D image from a PGM file */
	void readPGM(const char* filename, unsigned char* &image, unsigned int &width, unsigned int &height);
		
	
	/** Write a 2D image from a PGM file */
	void writePGM(const char* filename, const unsigned char* image, unsigned int width, unsigned int height);
	

	/** Black and white reader for PGM files */
	class PGM
	{
	public:
		static void read(const char* filename, unsigned char* &image, unsigned int &width, unsigned int &height); ///< read a 2D PGM image
		static void write(const char* filename, const unsigned char* image, unsigned int width, unsigned int height); ///< write a 2D PGM binary image
	};

} // namespace


#endif
