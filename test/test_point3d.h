/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#include <iostream>
#include <cmath>
#include <vector>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <tools/manipworld.h>
#include <display/display3d.h>
#include <display/point3d.h>
#include <tools/pick.h>
#include <io/ioraw.h>

/**
 * @test Testing point list display
 */

namespace TestPoint3D
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static MouseButton gButton = NONE;
	static int gModifiers = 0; // ctrl,alt,shift
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later
	unsigned int winw = 0;
	unsigned int winh = 0;

	Tools::Pick pick;
	Tools::ManipWorld manip;
	Display::Volume displayVolume;
	unsigned short* volume;
	unsigned int volumeSize[3];
	Display::Point3D displayPoints;
	std::vector<float> points; // triplets of (x,y,z) for point coordinates


	void convertVolumeToVoxel(float x, float y, float z,
	                          unsigned int &xx, unsigned int &yy, unsigned int &zz)
	{
		xx = static_cast<unsigned int>((x+0.5f)*volumeSize[0]+0.5f);
		yy = static_cast<unsigned int>((y+0.5f)*volumeSize[1]+0.5f);
		zz = static_cast<unsigned int>((z+0.5f)*volumeSize[2]+0.5f);
	}


	void init()
	{
		glClearColor (0.0, 0.0, 0.5, 1.0);

		manip.init();
		manip.translate(0,0,-3.6);

		displayVolume.update(volume,volumeSize[0],volumeSize[1],volumeSize[2]);
		displayVolume.setMode(Display::Volume::MPR);
		pick.setMaxAlpha(0);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);

		glMatrixMode(GL_MODELVIEW);

		winw = w;
		winh = h;
	}

	void display(void)
	{
		try
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			
			/* Rotate volume with mouse movement */
			manip.loadModelViewMatrix();
	
			/* zoom a little bit */
			glScalef(2,2,2);
	
			/* display the volume */
			glEnable(GL_DEPTH_TEST);

			glPushMatrix();
			glTranslatef(-0.5f,-0.5f,-0.5f);
			glScalef(1.0f/volumeSize[0],1.0f/volumeSize[1],1.0f/volumeSize[2]);
			displayPoints.display();
			glPopMatrix();
	
			pick.enable(); // every object between enable and disable can be picked
			displayVolume.display();
			pick.disable(); // stop defining the selection
	
			glDisable(GL_DEPTH_TEST);
	
			glPopMatrix();
	
			glutSwapBuffers();
		}
		catch(const Error& e) { e.print(); }
	}


	void mouse(int button, int state, int x, int y)
	{
		gModifiers = glutGetModifiers();

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
			{
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				
				break;
			}
			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
				if(gModifiers != GLUT_ACTIVE_SHIFT)
				{
					manip.rotateYAxis(x-gPrevMouse[0]);
					manip.rotateXAxis(gPrevMouse[1]-y);

					glutPostRedisplay();
				}
				else
				{
					float xx,yy,zz; // volume coordinate (-0.5..0.5)
					if(pick.get(x,winh-y, xx,yy,zz))
					{
						unsigned int vx,vy,vz; // voxel coordinate (0..volsize)
						convertVolumeToVoxel(xx,yy,zz, vx,vy,vz);
			
						/* add point to list of point */
						points.push_back((float)vx);
						points.push_back((float)vy);
						points.push_back((float)vz);

						float* p = &(*points.begin());
						displayPoints.update(p, (int)points.size()/3);

						glutPostRedisplay();
					}
				}

				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				break;
			default:
				break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		switch(key)
		{
			case 'q':
				glutDestroyWindow(winid); // close window, terminate glut loop
				exit(0); // on OSX for some reason the program does not terminate
			default:
				break;
		}
	}

	int main_init()
	{
		try
		{
			volumeSize[0] = 128,
			volumeSize[1] = 128,
			volumeSize[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, volumeSize[0]*volumeSize[1]*volumeSize[2]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("point 3D");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
		
			//glutMainLoop();
			
			return 0;
		}
		catch(const Error &e) {
			e.print();
		}

		return 1;
	}
} // namespace
