/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#include <iostream>
#include <cmath>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <tools/manipworld.h>
#include <display/display3d.h>
#include <tools/pick.h>


/**
 * @test Testing 3D picking
 */

namespace Test3DPick
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static MouseButton gButton = NONE;
	static int gPrevMouse[2] = { 0,0 };
	static float gPos[3] = {0,0,0}; // position of picked point
	int winid = 0; // window id, so we can destroy it later
	unsigned int winw = 0;
	unsigned int winh = 0;

	Tools::Pick pick;
	Tools::ManipWorld manip;
	Display::Volume displayVolume;
	unsigned short* volume;
	unsigned int volumeSize[3];



	void convertVolumeToVoxel(float x, float y, float z, int &xx, int &yy, int &zz)
	{
		xx = static_cast<int>((x+0.5)*volumeSize[0]+0.5);
		yy = static_cast<int>((y+0.5)*volumeSize[1]+0.5);
		zz = static_cast<int>((z+0.5)*volumeSize[2]+0.5);
	}


	void displayMarker()
	{
		glPushMatrix();
		glTranslatef(gPos[0], gPos[1], gPos[2]);

		glScalef(0.3,0.3,0.3);

		glColor3f(1,0,0);

		glBegin(GL_LINES);
		glVertex3f(-0.5,0,0);
		glVertex3f(+0.5,0,0);
		glVertex3f(0,-0.5,0);
		glVertex3f(0,+0.5,0);
		glVertex3f(0,0,+0.5);
		glVertex3f(0,0,-0.5);
		glEnd();

		glPopMatrix();
	}

	void init()
	{
		glClearColor (0.0, 0.0, 0.5, 1.0);

		manip.init();
		manip.translate(0,0,-3.6);

		displayVolume.update(volume,volumeSize[0],volumeSize[1],volumeSize[2]);
		displayVolume.updateRamp(10,125);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);

		glMatrixMode(GL_MODELVIEW);

		winw = w;
		winh = h;
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		/* Rotate volume with mouse movement */
		manip.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(2,2,2);

		/* display the volume */
		glEnable(GL_DEPTH_TEST);
		displayMarker();

		pick.enable(); // every object between enable and disable can be picked
		displayVolume.display();
		pick.disable(); // stop defining the selection

		glDisable(GL_DEPTH_TEST);

		glPopMatrix();

		glutSwapBuffers();
	}


	void mouse(int button, int state, int x, int y)
	{
		switch(button)
		{
			case GLUT_LEFT_BUTTON:
			{
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				
				if(state == GLUT_DOWN)
				{
					float xx,yy,zz; // volume coordinate (-0.5..0.5)
					if(pick.get(x,winh-y, xx,yy,zz))
					{
						gPos[0] = xx;
						gPos[1] = yy;
						gPos[2] = zz;
			
						int vx,vy,vz; // voxel coordinate (0..volsize)
						convertVolumeToVoxel(xx,yy,zz, vx,vy,vz);
			
						printf("xyz: %d %d %d\n", vx,vy,vz);
						glutPostRedisplay();
					}
				}
				break;
			}
			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
				manip.rotateYAxis(x-gPrevMouse[0]);
				manip.rotateXAxis(gPrevMouse[1]-y);

				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				glutPostRedisplay();
				break;
			default:
				break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		switch(key)
		{
			case 'q':
				glutDestroyWindow(winid); // close window, terminate glut loop
				exit(0); // on OSX for some reason the program does not terminate
			default:
				break;
		}
	}

	int main_init()
	{
		try
		{
			volumeSize[0] = 128,
			volumeSize[1] = 128,
			volumeSize[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, volumeSize[0]*volumeSize[1]*volumeSize[2]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("3D picking");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
		
			//glutMainLoop();
			
			return 0;
		}
		catch(const Error &e) {
			e.print();
		}

		return 1;
	}
} // namespace
