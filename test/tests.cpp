/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#if WIN32
#include <windows.h>
#include "msvc/glee.h"
#endif


#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <error.h>

#include "test_io.h"
#include "test_display2d.h"
#include "test_display3d.h"
#include "test_display4d.h"
#include "test_mask3d.h"
#include "test_display3d_clip.h"
#include "test_isosurface.h"
#include "test_3dpick.h"
#include "test_transferfunction.h"
#include "test_point3d.h"
#include "test_stereo.h"
#include "test_multi.h"


int main(int argc, char** argv)
{
	try
	{
		printf("*** libVD tests\n");
		printf("*** wait until all windows open\n");
		printf("***\n");
		printf("*** begin tests:\n");

		glutInit(&argc, (char**)argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
		
		TestIO::main_init();
		TestDisplay2D::main_init();
		TestDisplay3D::main_init();
		TestDisplay4D::main_init();
		TestDisplay3DClip::main_init();
		TestDisplayIsoSurface::main_init();
		Test3DPick::main_init();
		TestTransferFunction::main_init();
		TestDisplayMask3D::main_init();
		TestPoint3D::main_init();
		TestStereo::main_init();
		TestMulti3D::main_init();

		printf("*** end tests:\n");

		glutMainLoop();
		return 0;
	}
	catch(const Error &e) { e.print(); }
	catch(...) { printf("unknown error\n"); }

	return 1;
}

