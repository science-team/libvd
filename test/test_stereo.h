/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <io/ioraw.h>
#include <tools/manipworld.h>
#include <tools/stereo.h>
#include <display/display3d.h>

/**
 * @test Testing stereoscopic renderer
 */

namespace TestStereo
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static int gModifiers = 0; // ctrl,alt,shift
	static MouseButton gButton = NONE;
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later
	int winh = 0;


	Tools::Stereo stereo;
	Tools::ManipWorld manipWorld;
	Display::Volume displayVolume;
	unsigned short* volume;
	unsigned int size[3];


	void init()
	{
		glClearColor (0.25, 0.4, 0.6, 1.0);

		manipWorld.init();
		manipWorld.translate(0,0,-3.6);

		displayVolume.update(volume, size[0], size[1], size[2]);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
		glMatrixMode(GL_MODELVIEW);

		winh = h;
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		/* Rotate volume with mouse movement */
		manipWorld.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(2,2,2);

		glEnable(GL_DEPTH_TEST);

		stereo.enable();
		displayVolume.display();
		stereo.disable();

		glDisable(GL_DEPTH_TEST);

		glPopMatrix();
		
		glutSwapBuffers();
	}

	void mouse(int button, int state, int x, int y)
	{
		gModifiers = glutGetModifiers();

		/* degrade the volume when moving the mouse, so the rendering is interactive */
		if(state == GLUT_DOWN) displayVolume.setQuality(0.50);                         // low resolution (interactive)
		else                 { displayVolume.setQuality(1.0f); glutPostRedisplay(); }  // full resolution

		/* pick if shift is pressed */
		if(gModifiers == GLUT_ACTIVE_SHIFT && button == GLUT_LEFT_BUTTON)
		{
		}

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				break;

			case GLUT_RIGHT_BUTTON:
				gButton = RIGHT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				break;

			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
			{
				float dx = (x - gPrevMouse[0]);
				float dy = (gPrevMouse[1] - y);
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				manipWorld.rotateYAxis(dx);
				manipWorld.rotateXAxis(dy);
				glutPostRedisplay();
				break;
			}
			case RIGHT:
			{
				float dy = (gPrevMouse[1] - y);
				gPrevMouse[1] = y;

				float deltaZoom = 1.0f + dy / 100.0f;
				manipWorld.zoom(deltaZoom);
				glutPostRedisplay();
				break;
			}
			default:
				break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		bool refresh = true;

		switch(key)
		{
		case '1': displayVolume.setMode(Display::Volume::VRT); break;
		case '2': displayVolume.setMode(Display::Volume::MIP); break;
		case '3': displayVolume.setMode(Display::Volume::MPR); break;

		case 'q':
			glutDestroyWindow(winid); // close window, terminate glut loop
			exit(0); // on OSX for some reason the program does not terminate
		default:
			break;
		}

		if(refresh) glutPostRedisplay();
	}

	int main_init()
	{
		try
		{
			size[0] = 128,
			size[1] = 128,
			size[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, size[0]*size[1]*size[2]);
			
			glutInitWindowSize(250, 250);
			//glutInitWindowPosition(100, 100);
			winid = glutCreateWindow("display3d");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
		
			//glutMainLoop();
			
			return 0;
		}
		catch(const Error &e) { e.print(); }
		catch(...) { printf("unknown error\n"); }

		return 1;
	}

} // namespace
