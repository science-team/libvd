/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <color.h>
#include <display/display2d.h>
#include <display/display2d_drawpixels.h>
#include <io/iopgm.h>
#include <io/iobmp.h>


/**
 * @test Testing 2D image display
 */

namespace TestDisplay2D
{

	// Image to read
	template <typename T>
	struct Image {
		T* data;
		unsigned int width, height;
	};

	Image<unsigned char>         image_8bits;         // observed image
	Image<color<unsigned char> > image_24bits;        // observed image
	Display::Image disp; // image player
	Display::ImageDrawPixels disp_drawpixels;
	bool use_drawpixels = false;

	GLuint tex;

	int winid = 0; // window id, so we can destroy it later

	void init()
	{
		// Initialize display
		glClearColor(0,0,0,0);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0, 0, image_8bits.width, image_8bits.height);
		glOrtho(-0.5,+0.5,-0.5,+0.5,-1,+1);

		// Deform image
		disp.update(image_24bits.data, image_24bits.width, image_24bits.height);
		disp_drawpixels.update(image_24bits.data, image_24bits.width, image_24bits.height);

	}

	void display()
	{
		// Erase buffer
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(0,0,0,1);

		// Display the image
		if(!use_drawpixels)
			disp.display();
		else
			disp_drawpixels.display();

		glutSwapBuffers();
	}


	void keyboard(unsigned char key, int x, int y)
	{
		bool refresh = true;

		switch(key)
		{
			case '1': disp.update(image_8bits.data, image_8bits.width, image_8bits.height); use_drawpixels=false; break; // Show labels (restored image)
			case '2': disp.update(image_24bits.data, image_24bits.width, image_24bits.height); use_drawpixels=false; break; // Show observed image
			case '3': disp_drawpixels.update(image_8bits.data, image_8bits.width, image_8bits.height); use_drawpixels=true;  break; // Show labels (restored image)
			case '4': disp_drawpixels.update(image_24bits.data, image_24bits.width, image_24bits.height); use_drawpixels=true; break; // Show observed image
			case 'q':
			{
				glutDestroyWindow(winid); // close window, terminate glut loop
				exit(0); // on OSX for some reason the program does not terminate
				break;
			}
			default: refresh = false; break;
		};

		if(refresh) glutPostRedisplay();
	}


	int main_init()
	{
		try
		{
			// Load image
			IO::PGM::read("data/image.pgm", image_8bits.data,  image_8bits.width, image_8bits.height);
			IO::BMP::read("data/image.bmp", image_24bits.data, image_24bits.width, image_24bits.height);
			
			// Initialize window
			glutInitWindowSize(image_24bits.width, image_24bits.height);
			winid = glutCreateWindow("display2d");
			init();
		
			// Ininitialize callbacks
			glutDisplayFunc(display);
			glutKeyboardFunc(keyboard);
		
			// Main loop
			//glutMainLoop();
		
			return 0;
		}
		catch(const Error &e) { e.print(); }
		catch(...) { printf("unknown error\n"); }

		return 1;
	}

};
