/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#include <iostream>
#include <cmath>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <tools/manipworld.h>
#include <tools/transferfunction/editor.h>
#include <tools/transferfunction/ramp.h>
#include <display/display3d.h>

/**
 * @test Testing transfer function manipulation
 */

namespace TestTransferFunction
{
	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static MouseButton gButton = NONE;
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later
	int winh = 0;

	Tools::ManipWorld manipWorld;
	Tools::TransferFunction::Editor tfe;
	Display::Volume displayVolume;
	unsigned short* volume;
	unsigned int size[3];


	void init()
	{
		glClearColor (0.0, 0.0, 0.5, 1.0);

		manipWorld.init();
		manipWorld.translate(0,0,-3.6);

		displayVolume.update(volume, size[0], size[1], size[2]);

		tfe.update(volume, size[0]*size[1]*size[2]);
		tfe.add(new Tools::TransferFunction::Ramp(30,125));
		tfe.clamp(0,125);

		displayVolume.updateLut(tfe);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
		glMatrixMode(GL_MODELVIEW);

		tfe.place(0,0, 200,100, w,h);
		winh = h;
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		/* Rotate volume with mouse movement */
		manipWorld.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(2,2,2);

		glEnable(GL_DEPTH_TEST);
		displayVolume.display();
		glDisable(GL_DEPTH_TEST);

		glPopMatrix();
		
		tfe.display();

		glutSwapBuffers();
	}

	void mouse(int button, int state, int x, int y)
	{
		/* degrade the volume when moving the mouse, so the rendering is interactive */
		if(state == GLUT_DOWN) displayVolume.setQuality(0.50);                         // low resolution (interactive)
		else                 { displayVolume.setQuality(1.0f); glutPostRedisplay(); }  // full resolution

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				tfe.mouse(x,winh-y);
				break;
			case GLUT_RIGHT_BUTTON:
				gButton = RIGHT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				if(tfe.mouse(x,winh-y))
				{
					tfe.colorMode();
					glutPostRedisplay();
				}
				break;
			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
				if(tfe.motion(x,winh-y))
				{
					if(tfe.lutchanged())
						displayVolume.updateLut(tfe);
				}
				else
				{
					int dx = (x - gPrevMouse[0]);
					int dy = (gPrevMouse[1] - y);
					manipWorld.rotateYAxis((float)dx);
					manipWorld.rotateXAxis((float)dy);
				}
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				glutPostRedisplay();
				break;
			case RIGHT:
			{
				int dy = (gPrevMouse[1] - y);
				gPrevMouse[1] = y;

				float deltaZoom = 1.0f + dy / 100.0f;

				manipWorld.zoom(deltaZoom);
				glutPostRedisplay();
				break;
			}
			default:
				break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		switch(key)
		{
			case 'q':
				glutDestroyWindow(winid); // close window, terminate glut loop
				exit(0); // on OSX for some reason the program does not terminate
			default:
				break;
		}
	}

	int main_init()
	{
		try
		{
			size[0] = 128,
			size[1] = 128,
			size[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, size[0]*size[1]*size[2]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("transfer function");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
		
			
			return 0;
		}
		catch(const Error &e) {
			e.print();
		}

		return 1;
	}

} // namespace
