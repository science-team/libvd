/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <io/ioraw.h>
#include <tools/manipworld.h>
#include <tools/transferfunction/editor.h>
#include <tools/transferfunction/ramp.h>
#include <tools/slidebar.h>
#include <tools/clip.h>

#include <display/display4d.h>

/**
 * @test Testing 4D volume rendering
 */

namespace TestDisplay4D
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static int gModifiers = 0; // ctrl,alt,shift
	static MouseButton gButton = NONE;
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later
	int winh = 0;


	Tools::TransferFunction::Editor tfe;
	Tools::ManipWorld manipWorld;
	Tools::SlideBar bar;
	Tools::Clip clip;
	Display::Volume4D displayVolume;
	unsigned char* volume;
	unsigned int size[4];
	float spacing[4];
	unsigned int frame = 0;
	bool pause = 1;


	void init()
	{
		glClearColor (0.25, 0.4, 0.6, 1.0);
		
		manipWorld.init();
		manipWorld.translate(0,0,-3.6);

		displayVolume.update(volume, size[0], size[1], size[2], size[3]);
		tfe.update(&volume[frame*size[0]*size[1]*size[2]], size[0]*size[1]*size[2]);
		tfe.clamp(0,1500);
		tfe.add(new Tools::TransferFunction::Ramp(1000,1600));
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
		glMatrixMode(GL_MODELVIEW);

		tfe.place(0,0, 200,100, w,h);
		bar.place(20,h-50, w-40,20, w,h);
		winh = h;
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		/* Rotate volume with mouse movement */
		manipWorld.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(spacing[0],spacing[1],spacing[2]);

		glEnable(GL_DEPTH_TEST);

		clip.enable();
		clip.display();
		displayVolume.display();
		clip.disable();

		glDisable(GL_DEPTH_TEST);

		glPopMatrix();

		tfe.display();
		bar.display();
		
		glutSwapBuffers();
	}

	void mouse(int button, int state, int x, int y)
	{
		gModifiers = glutGetModifiers();

		/* degrade the volume when moving the mouse, so the rendering is interactive */
		if(state == GLUT_DOWN) displayVolume.setQuality(0.50);                         // low resolution (interactive)
		else                 { displayVolume.setQuality(1.0f); glutPostRedisplay(); }  // full resolution

		/* pick if shift is pressed */
		if(gModifiers == GLUT_ACTIVE_SHIFT && button == GLUT_LEFT_BUTTON)
		{
		}

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				tfe.mouse(x,winh-y);
				bar.mouse(x,winh-y);
				
				break;

			case GLUT_RIGHT_BUTTON:
				gButton = RIGHT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				if(tfe.mouse(x,winh-y))
				{
					tfe.colorMode();
					glutPostRedisplay();
				}
				break;

			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
			{
				float dx = (x - gPrevMouse[0]);
				float dy = (gPrevMouse[1] - y);
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				if(bar.motion(x,winh-y))
				{
					frame = (int)(bar.get() * size[3]);
					frame = min(size[3]-1, frame);
					frame = max(0, frame);

					if(volume) displayVolume.setFrame(frame);
				}
				else if(tfe.motion(x,winh-y))
				{
					if(tfe.lutchanged())
						displayVolume.updateLut(tfe);
				}
				else
				{
					if(gModifiers == GLUT_ACTIVE_SHIFT)
					{
						clip.rotateWithScreen(dx, dy);
						float plane_matrix[16]; clip.getPlaneMatrix(plane_matrix); displayVolume.setMprMatrix(plane_matrix);
					}
					else
					{
						manipWorld.rotateYAxis(dx);
						manipWorld.rotateXAxis(dy);
					}
				}
				glutPostRedisplay();
				break;
			}
			case RIGHT:
			{
				float dy = ( gPrevMouse[1] - y);
				gPrevMouse[1] = y;

				if(gModifiers == GLUT_ACTIVE_SHIFT)
				{
					clip.slide(dy/40);
					float plane_matrix[16]; clip.getPlaneMatrix(plane_matrix); displayVolume.setMprMatrix(plane_matrix);
				}
				else
				{
					float deltaZoom = 1.0f + dy / 100.0f;
					manipWorld.zoom(deltaZoom);
				}
				glutPostRedisplay();
				break;
			}
			default:
				break;
		}
	}

	void idle()
	{
		++frame; if(frame==size[3]) frame = 0;

		if(gButton == NONE)
			tfe.update(&volume[frame*size[0]*size[1]*size[2]], size[0]*size[1]*size[2]);

		bar.set((float)frame/size[3]);

		if(volume) displayVolume.setFrame(frame);

		glutPostRedisplay();
	}

	void keyboard (unsigned char key, int x, int y)
	{
		bool refresh = true;

		switch(key)
		{
		case ' ':
			pause = !pause;
			glutIdleFunc( (pause) ? NULL : idle );
			break;

		case 'q':
			glutDestroyWindow(winid); // close window, terminate glut loop
			exit(0); // on OSX for some reason the program does not terminate
		default:
			break;
		}

		if(refresh) glutPostRedisplay();
	}

	void specialkeyboard(int key, int x, int y)
	{
		bool refresh = true;

		switch(key)
		{
		case GLUT_KEY_LEFT:
			if(frame==0) frame = size[3];
			--frame;
			break;

		case GLUT_KEY_RIGHT:
			++frame; if(frame==size[3]) frame = 0;
			break;

		default: refresh = false; break;
		}

		if(refresh)
		{
			// Change accessories
			if(gButton == NONE)
				if(volume) tfe.update(&volume[frame*size[0]*size[1]*size[2]], size[0]*size[1]*size[2]);

			bar.set((float)frame/size[3]);

			// Change volume
			if(volume) displayVolume.setFrame(frame);

			glutPostRedisplay();
		}
	}


	int main_init()
	{
		try
		{
			size[0] = 256,
			size[1] = 80,
			size[2] = 84,
			size[3] = 32;

			spacing[0] = 2/1.0f,
			spacing[1] = 2/1.0f,
			spacing[2] = 2/1.3f;

			IO::Raw::read("data/uchar-256-80-84-32-baby.raw", volume, size[0]*size[1]*size[2]*size[3]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("display4d");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
			glutSpecialFunc(specialkeyboard);
		
			//glutMainLoop();
			
			return 0;
		}
		catch(const Error &e) { e.print(); }
		catch(...) { printf("unknown error\n"); }

		return 1;
	}

} // namespace
