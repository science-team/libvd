/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <io/ioraw.h>
#include <io/iobmp.h>
#include <io/ioscreenshot.h>
#include <display/display3d.h>

/**
 * @test Testing file IO
 */

namespace TestIO
{

	int winid = 0; // window id, so we can destroy it later

	Display::Volume displayVolume;
	unsigned short* volume;
	unsigned int size[3];


	/** tests:
	 * IO::Raw::read()
	 * IO::glScreenshot()
	 * IO::BMP::write()
	 */
	int test_ioscreenshot()
	{
		try
		{
			/* read a volume and displays it */
			size[0] = 128,
			size[1] = 128,
			size[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, size[0]*size[1]*size[2]);
			displayVolume.update(volume, size[0], size[1], size[2]);

			/* display volume */
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);
			displayVolume.display();
			glDisable(GL_DEPTH_TEST);

			/* now do a screen capture */
			unsigned int w, h;
			color<unsigned char>* image;

			/* do a screenshot */
			IO::glScreenshot(image, w, h);
			glCheckError();

			/* save into an image */
			IO::BMP::write("screenshot.bmp", image, w, h);
			printf("screen capture saved at \"screenshot.bmp\"\n");

			delete [] volume; volume = 0;
			size[0] = size[1] = size[2] = 0;

			return 0; // no error
		}
		catch(const Error &e) { e.print(); }
		catch( ... ) { printf("unknown error\n"); }

		return 1; // error
	}


	void init()
	{
		glClearColor (0.25, 0.4, 0.6, 1.0);
		glViewport(0, 0, 250, 250);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, 1.0, 1.0, 30.0);
		glMatrixMode(GL_MODELVIEW);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0,0,-5);
		glScalef(3,3,3);
	}


	void display(void)
	{
		/* do nothing, all tests should be done by now */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glutSwapBuffers();
	}


	int main_init()
	{
		try
		{
			glutInitWindowSize(250, 250);
			//glutInitWindowPosition(100, 100);
			winid = glutCreateWindow("io");

			init();

			glutDisplayFunc(display); // required by glut

			/* actual test */
			test_ioscreenshot();

			return 0;
		}
		catch(const Error &e) { e.print(); }
		catch(...) { printf("unknown error\n"); }

		return 1;
	}

} // namespace
