/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include <io/ioraw.h>
#include <io/iobmp.h>
#include <io/ioscreenshot.h>
#include <tools/manipworld.h>

#include <display/mask3d.h>

/**
 * @test Testing 3D mask display
 */

namespace TestDisplayMask3D
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static int gModifiers = 0; // ctrl,alt,shift
	static MouseButton gButton = NONE;
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later
	int winh = 0;


	Tools::ManipWorld manipWorld;
	Display::Mask3D displayMask;
	unsigned char* mask;
	unsigned int size[3];


	void init()
	{
		glClearColor (0.25, 0.4, 0.6, 1.0);
		glShadeModel(GL_SMOOTH);
		glEnable(GL_DEPTH_TEST);

		manipWorld.init();
		manipWorld.translate(0,0,-3.6);

		displayMask.update(mask, size[0], size[1], size[2]);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
		glMatrixMode(GL_MODELVIEW);

		winh = h;
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		GLfloat lightPos[4] = {0,0,5,1};
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

		/* Rotate volume with mouse movement */
		manipWorld.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(2,2,2);

		glEnable(GL_DEPTH_TEST);

		glColor3f(0.1,0.02,0);
		displayMask.display();

		glDisable(GL_DEPTH_TEST);

		glPopMatrix();
		
		glutSwapBuffers();
	}

	void mouse(int button, int state, int x, int y)
	{
		gModifiers = glutGetModifiers();

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				break;

			case GLUT_RIGHT_BUTTON:
				gButton = RIGHT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				break;

			default:
				gButton = NONE;
				break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
			{
				float dx = (x - gPrevMouse[0]);
				float dy = (gPrevMouse[1] -y);
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				manipWorld.rotateYAxis(dx);
				manipWorld.rotateXAxis(dy);
				glutPostRedisplay();
				break;
			}
			case RIGHT:
			{
				float dy = (gPrevMouse[1] - y);
				gPrevMouse[1] = y;

				float deltaZoom = 1.0f + dy / 100.0f;
				manipWorld.zoom(deltaZoom);
				glutPostRedisplay();
				break;
			}
			default:
				break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		bool refresh = true;

		switch(key)
		{
		case 'q':
			glutDestroyWindow(winid); // close window, terminate glut loop
			exit(0); // on OSX for some reason the program does not terminate
		default: refresh = false; break;
		}

		if(refresh) glutPostRedisplay();
	}

	int main_init()
	{
		try
		{
			size[0] = 64,
			size[1] = 64,
			size[2] = 64;

			IO::Raw::read("data/uchar-256x256x185-mask.raw", mask, size[0]*size[1]*size[2]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("mask3d");
		
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);

			return 0;
		}
		catch(const Error &e) { e.print(); }
		catch(...) { printf("unknown error\n"); }

		return 1;
	}

} // namespace
