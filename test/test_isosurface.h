/***************************************************************************
 *   Copyright (C) 2006 by Herve Lombaert
 *   herve.lombaert@polymtl.ca
 ***************************************************************************/

#include <iostream>
#include <cmath>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <display/isosurface.h>
#include <tools/manipworld.h>

/**
 * @test Testing Isosurface extraction and visualization
 */

namespace TestDisplayIsoSurface
{

	enum MouseButton { NONE, LEFT, MIDDLE, RIGHT };
	static MouseButton gButton = NONE;
	static int gModifiers = 0; // ctrl,alt,shift
	static int gPrevMouse[2] = { 0,0 };
	int winid = 0; // window id, so we can destroy it later

	Tools::ManipWorld manipWorld;
	unsigned short* volume;
	int size[3];
	Display::IsoSurface displayIsosurface;


	void init()
	{
		glClearColor (0.0, 0.0, 0.5, 1.0);
		glShadeModel(GL_SMOOTH);
		glEnable(GL_DEPTH_TEST);

		manipWorld.init();
		manipWorld.translate(0,0,-3.6);

		displayIsosurface.update(volume, size[0],size[1],size[2], 50);
	}

	void reshape(int w, int h) 
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		GLfloat lightPos[4] = {0,0,5,1};
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

		/* Rotate volume with mouse movement */
		manipWorld.loadModelViewMatrix();

		/* zoom a little bit */
		glScalef(2,2,2);

		glColor3f(0.1,0.02,0);
		displayIsosurface.display();

		glPopMatrix();

		glutSwapBuffers();
	}

	void mouse(int button, int state, int x, int y)
	{
		gModifiers = glutGetModifiers();

		switch(button)
		{
			case GLUT_LEFT_BUTTON:
				gButton = LEFT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				break;
			case GLUT_RIGHT_BUTTON:
				gButton = RIGHT;
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;
				break;
			case GLUT_MIDDLE_BUTTON: gButton = MIDDLE; break;
			default: gButton = NONE; break;
		}
	}

	void motion(int x, int y)
	{
		switch(gButton)
		{
			case LEFT:
			{
				int dx = (x - gPrevMouse[0]);
				int dy = (gPrevMouse[1] - y);
				gPrevMouse[0] = x;
				gPrevMouse[1] = y;

				manipWorld.rotateYAxis((float)dx);
				manipWorld.rotateXAxis((float)dy);
				glutPostRedisplay();
				break;
			}
			case RIGHT:
			{
				int dy = (gPrevMouse[1] - y);
				gPrevMouse[1] = y;

				float deltaZoom = 1.0f + dy / 100.0f;

				manipWorld.zoom(deltaZoom);
				glutPostRedisplay();
				break;
			}
			default: break;
		}
	}

	void keyboard (unsigned char key, int x, int y)
	{
		switch(key)
		{
			case '1':
				displayIsosurface.update(volume,size[0],size[1],size[2],50);
				glutPostRedisplay();
				break;
			case '2':
				displayIsosurface.update(volume,size[0],size[1],size[2],500);
				glutPostRedisplay();
				break;
			case '3':
				displayIsosurface.update(volume,size[0],size[1],size[2],1200);
				glutPostRedisplay();
				break;
			case 'q':
				glutDestroyWindow(winid); // close window, terminate glut loop
				exit(0); // on OSX for some reason the program does not terminate
				break;
			default:
				break;
		}
	}

	int main_init()
	{
		try {
			size[0] = 128,
			size[1] = 128,
			size[2] = 128;

			IO::Raw::read("data/ushort-128x128x128-volume.raw", volume, size[0]*size[1]*size[2]);
			
			glutInitWindowSize(250, 250);
			winid = glutCreateWindow("isosurface");
			
			init();
		
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouse);
			glutMotionFunc(motion);
			glutKeyboardFunc(keyboard);
		
			//glutMainLoop();
			
			return 0;
		}
		catch(const OpenGLError &e) { e.print(); }
		catch(const Error &e) { e.print(); }

		return 1;
	}

} // namespace
